import {Test, TestingModule} from '@nestjs/testing';
import {INestApplication} from '@nestjs/common';
import {RedisService} from "../src/redis/redis.service";
import {RedisModule} from "../src/redis/redis.module";
import {ConfigModule} from "@nestjs/config";
import {configs} from "../src/config";

describe('RedisService (e2e)', () => {

    const TEST_SET_KEY = 'test_set_key';
    let app: INestApplication;
    let redisService: RedisService;

    beforeEach(async () => {

        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot(
                    {
                        isGlobal: true,
                        cache: true,
                        envFilePath: ['.env', '.development.env'], // .env has to come first since existing env vars are not being overwritten
                        load: configs
                    }
                ),
                RedisModule
            ],
        }).compile();

        app = moduleFixture.createNestApplication();
        redisService = moduleFixture.get<RedisService>(RedisService);
        await app.init();
    });

    describe('addValueToSet', () => {
        it('should add the specified value to the redis set', async () => {
            const expected = 'test_value';
            await redisService.addValueToSet(TEST_SET_KEY, expected);
            const actual = (await redisService.getSet(TEST_SET_KEY))[0];
            expect(actual).toEqual(expected);
        });
    });


    describe('doesValueExistInSet', () => {
        it('should return true if the value exists in the set', async () => {

            const expected = 'test_value';
            await redisService.addValueToSet(TEST_SET_KEY, expected);
            expect(await redisService.doesValueExistInSet(TEST_SET_KEY, expected)).toBeTruthy();

        });
    });

    describe('removeSet', () => {
        it('should remove the entire set', async () => {
            await redisService.addValueToSet(TEST_SET_KEY, 'test_value');
            await redisService.removeSet(TEST_SET_KEY);

            const expected = [];
            const actual = await redisService.getSet(TEST_SET_KEY);
            expect(actual).toStrictEqual(expected);
        });
    });


    describe('removeValueFromSet', () => {
        it('should remove the specified value from the set', async () => {

            const valueToRemove = 'test_value';
            const expected = 'test_value2';

            await redisService.addValueToSet(TEST_SET_KEY, valueToRemove);
            await redisService.addValueToSet(TEST_SET_KEY, expected);

            await redisService.removeValueFromSet(TEST_SET_KEY, valueToRemove);

            const actual = await redisService.getSet(TEST_SET_KEY);
            expect(actual[0]).toStrictEqual(expected);

        });
    });

    describe('getSet', () => {
        it('should return an array with all elements in the set', async () => {

            const expected = ['a', 'b', 'c'];

            for (const testValue of expected) {
                await redisService.addValueToSet(TEST_SET_KEY, testValue);
            }
            const actual = (await redisService.getSet(TEST_SET_KEY)).sort();
            expect(actual).toStrictEqual(expected);
        });
    });

    describe('getNumberOfElementsInSet', () => {
        it('should return the number of elements in the set', async () => {

            const testValues = ['a', 'b', 'c'];

            for (const testValue of testValues) {
                await redisService.addValueToSet(TEST_SET_KEY, testValue);
            }
            const actual = await redisService.getNumberOfElementsInSet(TEST_SET_KEY);
            expect(actual).toEqual(testValues.length);

        });
    });

    afterEach(() => {
        redisService.removeSet(TEST_SET_KEY);
        app.close();
    });
});
