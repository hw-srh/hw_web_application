import {Test, TestingModule} from '@nestjs/testing';
import {INestApplication} from '@nestjs/common';
import {AppModule} from '../src/app.module';
import {UsersService} from "../src/users/users.service";
import {User} from "../src/entities";
import {ConfigService} from "@nestjs/config";
import {resetDataBase} from "./shared-scripts/reset-data-base";
import {createTestUser} from "./shared-scripts/create-test-user";
import {PasswordResetTokensService} from "../src/password-reset-tokens/password-reset-tokens.service";

describe('PasswordResetTokensService (e2e)', () => {
    let app: INestApplication;
    let passwordResetTokensService: PasswordResetTokensService;
    let testUser: User;

    const token: string = 'test';
    const tokenHash: string = '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08';


    beforeEach(async () => {

        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        passwordResetTokensService = moduleFixture.get<PasswordResetTokensService>(PasswordResetTokensService);

        await resetDataBase(moduleFixture.get<ConfigService>(ConfigService));

        const usersService = moduleFixture.get<UsersService>(UsersService);
        testUser = await createTestUser(usersService);
        await app.init();
    });

    describe('createNewToken', () => {
        it('should create a new token with the provided data', async () => {

            const expected = {
                id: 1,
                userId: testUser.id,
                tokenHash,
            };

            const {created, ...actual} = await passwordResetTokensService.save({
                userId: testUser.id,
                token
            });

            expect(created).toBeInstanceOf(Date);
            expect(actual).toStrictEqual(expected);
        });
    });

    describe('deleteAllTokensForUser', () => {
        it('should remove all tokens for the current user', async () => {

            const numberOfTokens = 10;
            const newTokens = [];
            for (let i = 0; i < numberOfTokens; i++) {
                newTokens.push(passwordResetTokensService.save({
                    userId: testUser.id,
                    token: `${token}-${i}`
                }));
            }

            await Promise.all(newTokens);

            const result = await passwordResetTokensService.deleteAllTokensForUser(testUser);
            expect(result.affected).toEqual(numberOfTokens);
        });
    });

    describe('findPasswordResetToken', () => {
        it('should return the passwordResetToken entity belonging to the given token', async () => {

            await passwordResetTokensService.save({
                userId: testUser.id,
                token
            });

            const actual = await passwordResetTokensService.findPasswordResetToken(token);

            const {roles, ...expectedUser} = testUser;

            expect(actual.user).toEqual(expectedUser);
        });
    });


    afterEach(async () => {
        // Closing the db and redis connection after each test is an alternative to closing the application after each test
        // but its not really faster (only tested with create and update user)

        // await getConnection().close();
        // await redisService.onApplicationShutdown();
        await app.close();
    });
});
