import {Test, TestingModule} from '@nestjs/testing';
import {INestApplication, UnauthorizedException} from '@nestjs/common';
import * as request from 'supertest';
import {AppModule} from '../src/app.module';
import {UsersService} from "../src/users/users.service";
import {AuthService} from "../src/auth/auth.service";
import {ConfigService} from "@nestjs/config";
import {resetDataBase} from "./shared-scripts/reset-data-base";
import {RedisService} from "../src/redis/redis.service";
import {LoginDto} from "../src/common/shared_interfaces/dto/login/login.dto";
import {basicTestUserData, createTestUser, removeTestUserDataFromRedis} from "./shared-scripts/create-test-user";
import jwt_decode from "jwt-decode";
import {JwtPayload} from "../src/common/shared_interfaces/jwt-payload.interface";
import {JwtService} from "@nestjs/jwt";
import {User} from "../src/entities";

describe('AuthController (e2e)', () => {
    let app: INestApplication;
    let usersService: UsersService;
    let authService: AuthService;
    let redisService: RedisService;

    let jwtService: JwtService;

    let testUser: User;

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        usersService = moduleFixture.get<UsersService>(UsersService);
        authService = moduleFixture.get<AuthService>(AuthService);
        redisService = moduleFixture.get<RedisService>(RedisService);
        jwtService = moduleFixture.get<JwtService>(JwtService);

        await resetDataBase(moduleFixture.get<ConfigService>(ConfigService));
        testUser = await createTestUser(usersService);
        await removeTestUserDataFromRedis(authService, testUser);

        await app.init();
    });

    describe('/auth/login POST', () => {

        it('should return a jwt if the users credentials are valid', async () => {

            const loginDto: LoginDto = {
                userNameOrEmail: testUser.email,
                password: basicTestUserData.password
            };

            await request(app.getHttpServer())
                .post('/auth/login')
                .send(loginDto)
                .expect((response) => {
                    const jwtPayload = jwt_decode<JwtPayload>(response?.body?.access_token);
                    return jwtPayload.sub === testUser.id;
                });
        });

        it('should send code 401 unauthorized if the users credentials are invalid', async () => {

            await request(app.getHttpServer())
                .post('/auth/login')
                .send({})
                .expect(401);
        });

    });

    describe('routes that require a jwt in the Authorization header', () => {
        let jwt: string;
        beforeEach(async () => {
            jwt = (await authService.login(testUser)).access_token;
        });

        describe('/auth/refresh POST', () => {

            it('should return a new jwt token if the request is valid', async () => {
                const decodedJwt = jwt_decode<JwtPayload>(jwt);

                const expiredJwt = await jwtService.signAsync({
                    ...decodedJwt,
                    exp: decodedJwt.exp - (3600 * 24)
                })

                await request(app.getHttpServer())
                    .post('/auth/refresh')
                    .set('Authorization', 'Bearer ' + expiredJwt)
                    .send()
                    .expect((response) => {
                        const oldDecodedJwt = jwt_decode<JwtPayload>(expiredJwt);
                        const newDecodedJwt = jwt_decode<JwtPayload>(response?.body?.access_token);
                        return (oldDecodedJwt.exp > newDecodedJwt.exp) && (oldDecodedJwt.jti !== newDecodedJwt.jti);
                    });
            });

            it('should send code 401 unauthorized if the jwt has expired more than one month ago', async () => {
                const decodedJwt = jwt_decode<JwtPayload>(jwt);

                const moreThanOneMonthOldJwt = await jwtService.signAsync({
                    ...decodedJwt,
                    exp: decodedJwt.exp - (32 * 3600 * 24)
                });

                await request(app.getHttpServer())
                    .post('/auth/refresh')
                    .set('Authorization', 'Bearer ' + moreThanOneMonthOldJwt)
                    .send()
                    .expect(401);
            });

            it('should send code 401 unauthorized if the jwt has not expired yet', async () => {

                await request(app.getHttpServer())
                    .post('/auth/login')
                    .set('Authorization', 'Bearer ' + jwt)
                    .send()
                    .expect(401);
            });
        });
    });


    describe('routes that require a jwt in the Authorization header', () => {
        let jwt: string;
        beforeEach(async () => {
            jwt = (await authService.login(testUser)).access_token;
        });

        describe('/auth/logout DELETE', () => {

            it('should send code 401 unauthorized if the jwt has expired', async () => {
                const decodedJwt = jwt_decode<JwtPayload>(jwt);

                const expiredJwt = await jwtService.signAsync({
                    ...decodedJwt,
                    exp: decodedJwt.exp - (3600 * 24)
                })

                await request(app.getHttpServer())
                    .delete('/auth/logout')
                    .set('Authorization', 'Bearer ' + expiredJwt)
                    .send()
                    .expect(401);
            });

            it('should send code 200 if the jwt was successfully invalided', async () => {

                await request(app.getHttpServer())
                    .delete('/auth/logout')
                    .set('Authorization', 'Bearer ' + jwt)
                    .send()
                    .expect(200);

                await expect(authService.validateJwt(jwt_decode<JwtPayload>(jwt))).rejects.toThrow(UnauthorizedException);
            });
        });
    });

    afterEach(async () => {
        // Closing the db and redis connection after each test is an alternative to closing the application after each test
        // but its not really faster (only tested with create and update user)

        // await getConnection().close();
        // await redisService.onApplicationShutdown();
        await app.close();
    });
});
