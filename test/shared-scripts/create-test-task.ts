import {Task} from "../../src/entities";
import {CreateTaskDto} from "../../src/tasks/dto/create-task.dto";
import {TasksService} from "../../src/tasks/tasks.service";
import {createDateObjWithoutMilliseconds} from "../../src/common/utils/date-factories.util";


function constructDefaultTaskTestData(): CreateTaskDto {
    return {
        customerName: "Best customer",
        customerDescription: "Some description",
        name: "Some name",
        scheduledBreakTime: "01:33:00",
        scheduledStart: createDateObjWithoutMilliseconds(),
        scheduledEnd: createDateObjWithoutMilliseconds(3600 * 1000 * 8), // + 8 hours
        taskDescription: "this is a task",
    }
}

export const createTestTask = async (tasksService: TasksService, customTaskData?: Partial<CreateTaskDto>): Promise<Task> => {

    const createTaskDto: CreateTaskDto = {
        ...constructDefaultTaskTestData(),
        ...customTaskData
    };

    return Object.assign(new Task(), await tasksService.create(createTaskDto));
};