import {CreateUserDto} from "../../src/common/shared_interfaces/dto/user/create-user.dto";
import {User} from "../../src/entities";
import {UsersService} from "../../src/users/users.service";
import {AuthService} from "../../src/auth/auth.service";
import {v1 as uuidv1} from 'uuid';
import {Request} from "express";
import {RandomStringGenerator} from "../../src/common/utils/random-string-generator.util";

export const basicTestUserData = {
    firstname: "test",
    lastname: "test",
    password: "test",
    roles: ["admin"]
};

export const createTestUser = async (usersService: UsersService, otherUserData?: Partial<CreateUserDto> & { password?: string }, removeFields: string[] = []): Promise<User> => {

    const createUserDto: CreateUserDto = {
        ...basicTestUserData,
        email: `${uuidv1()}@test.de`,
        ...otherUserData
    };

    jest.spyOn(RandomStringGenerator, 'hex').mockImplementation(async () => otherUserData?.password ?? basicTestUserData.password);

    const result = await usersService.create(createUserDto, {headers: {host: "localhost"}} as Request);

    for (const fieldName of removeFields) {
        delete result[fieldName];
    }

    return result;
};

export const removeTestUserDataFromRedis = async (authService: AuthService, user: User) => {
    await authService.invalidateJwtsForUser(user);
};