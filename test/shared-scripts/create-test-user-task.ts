import {Task, User, UserTask} from "../../src/entities";
import {CreateUserTaskDto} from "../../src/users-tasks/dto/create-user-task.dto";
import {UsersTasksService} from "../../src/users-tasks/users-tasks.service";
import {createDateObjWithoutMilliseconds} from "../../src/common/utils/date-factories.util";

function constructDefaultUserTaskTestData(): CreateUserTaskDto {
    return {
        task: undefined,
        taskId: undefined,
        user: undefined,
        userId: undefined,
        actualBreakTime: "01:33:00",
        actualStart: createDateObjWithoutMilliseconds(),
        actualEnd: createDateObjWithoutMilliseconds(3600 * 1000 * 8) // + 8 hours
    }
}

export const createTestUserTask = async (
    usersTasksService: UsersTasksService,
    user: User,
    task: Task,
    customUserTaskData?: Partial<CreateUserTaskDto>
): Promise<UserTask> => {

    const createUserTaskDto: CreateUserTaskDto = {
        ...constructDefaultUserTaskTestData(),
        ...customUserTaskData,
        user,
        userId: user.id,
        task,
        taskId: task.id
    };

    return Object.assign(new UserTask(), await usersTasksService.create(createUserTaskDto));
};