import {DatabaseConfig} from "../../src/config/interfaces/database-config.interface";
import {Connection, createConnection} from "typeorm";
import {ConfigService} from "@nestjs/config";


export async function resetDataBase(configService: ConfigService) {
    const dbConfig = configService.get<DatabaseConfig>('database');
    const dbConnection: Connection = await createConnection({...dbConfig, name: 'e2e-tests', multipleStatements: true});

    const tablesInDatabase: { table_name: string }[] = await dbConnection.query(
        'SELECT table_name FROM information_schema.tables WHERE table_schema = \'system\';'
    );

    const setupQueries = [];

    for (const tableData of tablesInDatabase) {
        setupQueries.push(`DELETE FROM ${tableData.table_name}; ALTER TABLE ${tableData.table_name} AUTO_INCREMENT = 1;`)
    }

    setupQueries.push('INSERT INTO role (id) VALUES ("admin")');

    await dbConnection.query(setupQueries.join(''));
    await dbConnection.close();
}

