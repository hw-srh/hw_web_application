import {Test, TestingModule} from '@nestjs/testing';
import {Task, User, UserTask} from "../src/entities";
import {UsersTasksService} from "../src/users-tasks/users-tasks.service";
import {AppModule} from "../src/app.module";
import {resetDataBase} from "./shared-scripts/reset-data-base";
import {ConfigService} from "@nestjs/config";
import {createTestUser} from "./shared-scripts/create-test-user";
import {UsersService} from "../src/users/users.service";
import {TasksService} from "../src/tasks/tasks.service";
import {createTestTask} from "./shared-scripts/create-test-task";
import {createTestUserTask} from "./shared-scripts/create-test-user-task";
import {INestApplication} from "@nestjs/common";
import {createDateObjWithoutMilliseconds} from "../src/common/utils/date-factories.util";
import {sleep} from "../src/common/utils/sleep.util";

describe('UsersTasksService (e2e)', () => {

    let app: INestApplication;

    let usersTasksService: UsersTasksService;
    let tasksService: TasksService;
    let usersService: UsersService

    let user: User;
    let task: Task;
    let userTask: UserTask;

    const setupTestData = async () => {
        user = Object.assign(new User(), await createTestUser(usersService));
        delete (user.roles); // userTask entities will not include the roles relationship

        task = Object.assign(new Task(), await createTestTask(tasksService));
        userTask = Object.assign(new UserTask(), await createTestUserTask(usersTasksService, user, task));
    };

    beforeEach(async () => {

        const module: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = module.createNestApplication();

        await resetDataBase(module.get<ConfigService>(ConfigService));

        usersService = module.get<UsersService>(UsersService);
        tasksService = module.get<TasksService>(TasksService);
        usersTasksService = module.get<UsersTasksService>(UsersTasksService);

        await setupTestData();
    });

    describe('findOne', () => {
        it('should find the userTask entity matching the userId and taskId', async () => {
            const actual = await usersTasksService.findOne({userId: userTask.userId, taskId: userTask.taskId});
            expect(actual).toStrictEqual(userTask);
        });

        it('should return undefined if one key of the composite key is invalid', async () => {
            const actual = await usersTasksService.findOne({userId: userTask.userId, taskId: undefined});
            expect(actual).toBeUndefined();
        });

        it('should return undefined if both keys of the composite key are invalid', async () => {
            const actual = await usersTasksService.findOne({userId: undefined, taskId: undefined});
            expect(actual).toBeUndefined();
        });
    });

    describe('create', () => {
        it('should create a new userTask entity with another task', async () => {
            const anotherTask = await createTestTask(tasksService);

            const expected = Object.assign(new UserTask(), {...userTask, task: anotherTask, taskId: anotherTask.id});
            const actual = await usersTasksService.create(expected);

            expect(actual).toStrictEqual(expected);
        });

        it('should create a new userTask entity with another user', async () => {
            const anotherUser = await createTestUser(usersService);

            const expected = Object.assign(new UserTask(), {...userTask, user: anotherUser, userId: anotherUser.id});
            const actual = await usersTasksService.create(expected);

            expect(actual).toStrictEqual(expected);
        });
    });


    describe('update', () => {
        it('should update an userTask entity', async () => {
            const expected = Object.assign(new UserTask(), {
                ...userTask,
                workProtocol: "updated work protocol",
                task: {
                    ...task,
                    name: "another name",
                    scheduledStart: createDateObjWithoutMilliseconds()
                },
            });

            const actual = await usersTasksService.update(expected);
            expect(actual).toStrictEqual(expected);
        });
    });

    describe('getUserBelongingToUserTask', () => {
        it('should return the user object matching the given composite key ', async () => {

            const actual = await usersTasksService.getUserBelongingToUserTask({
                userId: userTask.userId,
                taskId: userTask.taskId
            });
            expect(actual).toStrictEqual(user);
        });
    });


    describe('findAllInTimeRange', () => {

        const constructListOfUserTasks = async (userToUse: User = user): Promise<UserTask[]> => {
            const newTasks = await Promise.all<Task>([createTestTask(tasksService), createTestTask(tasksService)]);

            return Promise.all<UserTask>([
                createTestUserTask(usersTasksService, userToUse, newTasks[0]),
                createTestUserTask(usersTasksService, userToUse, newTasks[1])
            ]);
        };

        it('should return a list of userTask entities within the specified time range', async () => {

            await sleep(1000);

            const searchRangeStart = new Date(task.scheduledStart.getTime() + 1000);
            const searchRangeEnd = new Date(searchRangeStart.getTime() + 5000);

            const expected = await constructListOfUserTasks();

            const actual = await usersTasksService.findAll({
                timeRange: {start: searchRangeStart, end: searchRangeEnd}
            });

            expect(actual).toEqual(expect.arrayContaining(expected));
        });

        it('should return an empty list if no entity is found within the time range', async () => {

            await constructListOfUserTasks();

            const actual = await usersTasksService.findAll({
                timeRange: {
                    start: new Date(Date.now() + 2000),
                    end: new Date(Date.now() + 2000)
                }
            });

            expect(actual).toStrictEqual([]);
        });

        describe('findAllInTimeRangeForUser', () => {
            describe('should return a list of userTask entities within the specified time range only for the specified userId', () => {

                let searchRangeStart: Date;
                let searchRangeEnd: Date;

                let anotherUser: User;
                let userTasksForAnotherUser: UserTask[];
                let userTasksForDefaultTestUser: UserTask[];

                beforeEach(async () => {

                    await sleep(1000); // bring enough time between the default userTask and the following userTasks

                    searchRangeStart = new Date(task.scheduledStart.getTime() + 1000);
                    searchRangeEnd = new Date(searchRangeStart.getTime() + 5000);

                    userTasksForDefaultTestUser = await constructListOfUserTasks();

                    anotherUser = await createTestUser(usersService);
                    delete (anotherUser.roles); // not included in the user_task queries

                    userTasksForAnotherUser = await constructListOfUserTasks(anotherUser);
                });

                it('(default user)', async () => {
                    const actual = await usersTasksService.findAll({
                        userId: user.id,
                        timeRange: {start: searchRangeStart, end: searchRangeEnd}
                    });

                    expect(actual).toEqual(expect.arrayContaining(userTasksForDefaultTestUser));
                });

                it('(another user)', async () => {
                    const actual = await usersTasksService.findAll({
                        userId: anotherUser.id,
                        timeRange: {start: searchRangeStart, end: searchRangeEnd}
                    });

                    expect(actual).toEqual(expect.arrayContaining(userTasksForAnotherUser));
                });
            });

            it('should return an empty list if the given userId is not found on any entry within the given time range', async () => {
                const actual = await usersTasksService.findAll({
                    userId: user.id + 100_000,
                    timeRange: {
                        start: new Date(Date.now() - 60 * 1000),
                        end: new Date(Date.now() + 60 * 1000)
                    }
                });
                expect(actual).toStrictEqual([]);
            });
        });

    });


    afterEach(async () => {
        // Closing the db and redis connection after each test is an alternative to closing the application after each test
        // but its not really faster (only tested with create and update user)

        // await getConnection().close();
        // await redisService.onApplicationShutdown();
        await app.close();
    });
});