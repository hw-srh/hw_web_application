import {Test, TestingModule} from '@nestjs/testing';
import {INestApplication} from '@nestjs/common';
import * as request from 'supertest';
import {AppModule} from '../src/app.module';
import {UsersService} from "../src/users/users.service";
import {AuthService} from "../src/auth/auth.service";
import {Task, User, UserTask} from "../src/entities";
import {ConfigService} from "@nestjs/config";
import {resetDataBase} from "./shared-scripts/reset-data-base";
import {createTestUser, removeTestUserDataFromRedis} from "./shared-scripts/create-test-user";
import {UsersTasksService} from "../src/users-tasks/users-tasks.service";
import {TasksService} from "../src/tasks/tasks.service";
import {createTestTask} from "./shared-scripts/create-test-task";
import {createTestUserTask} from "./shared-scripts/create-test-user-task";
import {
    constructCompositeKey,
    constructTimeTrackingResponseDto
} from "../src/time-tracking/time-tracking-response.builder";
import {RawResponseTimeTrackingDto} from "../src/common/shared_interfaces/dto/time-tracking/raw-response-time-tracking.dto";
import {createDateObjWithoutMilliseconds} from "../src/common/utils/date-factories.util";
import {ResponseTimeTrackingDto} from "../src/common/shared_interfaces/dto/time-tracking/response-time-tracking.dto";
import {sleep} from "../src/common/utils/sleep.util";

describe('TimeTrackingController (e2e)', () => {
    let app: INestApplication;

    let usersTasksService: UsersTasksService;
    let tasksService: TasksService;
    let usersService: UsersService
    let authService: AuthService;

    let user: User;
    let task: Task;
    let userTask: UserTask;

    let jwt: string;

    const setupTestData = async () => {
        user = Object.assign(new User(), await createTestUser(usersService));
        task = Object.assign(new Task(), await createTestTask(tasksService));
        userTask = Object.assign(new UserTask(), await createTestUserTask(usersTasksService, user, task));
    };

    const convertToRawResponse = (responseTimeTrackingDTOs: ResponseTimeTrackingDto | ResponseTimeTrackingDto[]): RawResponseTimeTrackingDto[] => {
        if (!Array.isArray(responseTimeTrackingDTOs)) {
            responseTimeTrackingDTOs = [responseTimeTrackingDTOs]
        }

        return responseTimeTrackingDTOs.map((value: ResponseTimeTrackingDto): RawResponseTimeTrackingDto => {
            return JSON.parse(JSON.stringify(value));
        });
    };

    const compositeKeyFactory = (userParam: User = user, taskParam: Task = task): string => {
        return constructCompositeKey(userParam, taskParam);
    };

    beforeEach(async () => {

        const module: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = module.createNestApplication();
        usersService = module.get<UsersService>(UsersService);
        authService = module.get<AuthService>(AuthService);
        tasksService = module.get<TasksService>(TasksService);
        usersTasksService = module.get<UsersTasksService>(UsersTasksService);

        await resetDataBase(module.get<ConfigService>(ConfigService));
        await removeTestUserDataFromRedis(authService, user);

        await setupTestData();

        jwt = (await authService.login(user)).access_token;

        await app.init();
    });

    describe('/time-tracking POST', () => {
        it('should create new time tracking datasets and send the data back with ids', async () => {

            const {id, ...createTimeTrackingDto} = constructTimeTrackingResponseDto(userTask);

            const response = await request(app.getHttpServer())
                .post('/time-tracking')
                .set('Authorization', 'Bearer ' + jwt)
                .send([createTimeTrackingDto])
                .expect(201);

            const actual: RawResponseTimeTrackingDto[] = response.body;

            const expected: RawResponseTimeTrackingDto[] = convertToRawResponse({
                id: compositeKeyFactory(user, {...task, id: task.id + 1}),
                ...createTimeTrackingDto
            });

            expect(actual).toStrictEqual(expected);
        });

        it('should send 403 Forbidden if the user is not an admin', async () => {

            user = Object.assign(new User(), await createTestUser(usersService, {roles: []}));
            jwt = (await authService.login(user)).access_token;

            await request(app.getHttpServer())
                .post('/time-tracking')
                .set('Authorization', 'Bearer ' + jwt)
                .send([])
                .expect(403);
        });
    });


    describe('/time-tracking PATCH', () => {
        it('should update existing datasets and return the new data', async () => {

            const updateTimeTrackingDto = {
                ...constructTimeTrackingResponseDto(userTask),
                actualStart: createDateObjWithoutMilliseconds(2000)
            };

            const response = await request(app.getHttpServer())
                .patch('/time-tracking')
                .set('Authorization', 'Bearer ' + jwt)
                .send([updateTimeTrackingDto])
                .expect(200);

            const actual: RawResponseTimeTrackingDto[] = response.body;

            const expected: RawResponseTimeTrackingDto[] = convertToRawResponse({
                id: compositeKeyFactory(),
                ...updateTimeTrackingDto
            });

            expect(actual).toStrictEqual(expected);
        });

        describe(`access control is limited to admins and specific data for the current users data`, () => {

            beforeEach(async () => {
                user = await createTestUser(usersService, {roles: []});
                userTask = await createTestUserTask(usersTasksService, user, task);
                jwt = (await authService.login(user)).access_token;
            });


            it('should send 200 if only allowed field are used and the data belongs to the current user', async () => {

                const updateTimeTrackingDto = {
                    id: compositeKeyFactory(),
                    actualStart: createDateObjWithoutMilliseconds(2000),
                    actualEnd: createDateObjWithoutMilliseconds(2000 * 60),
                    actualBreakTime: "02:33",
                    workProtocol: "Whatever has been done to complete the job..."
                };

                await request(app.getHttpServer())
                    .patch('/time-tracking')
                    .set('Authorization', 'Bearer ' + jwt)
                    .send([updateTimeTrackingDto])
                    .expect(200);
            });

            it('should send 403 Forbidden if the user tries to update a field not on the allowed list', async () => {

                const updateTimeTrackingDto = {
                    id: compositeKeyFactory(),
                    scheduledStart: new Date()
                };

                await request(app.getHttpServer())
                    .patch('/time-tracking')
                    .set('Authorization', 'Bearer ' + jwt)
                    .send([updateTimeTrackingDto])
                    .expect(403);
            });

            it('should send 403 Forbidden if the user tries to update data from another user', async () => {

                const updateTimeTrackingDto = {
                    id: compositeKeyFactory({...user, id: user.id + 1}),
                    scheduledStart: new Date()
                };

                await request(app.getHttpServer())
                    .patch('/time-tracking')
                    .set('Authorization', 'Bearer ' + jwt)
                    .send([updateTimeTrackingDto])
                    .expect(403);
            });
        });
    });


    describe('/time-tracking:composite-key DELETE', () => {
        it('should remove the time-tracking data (but not the user) on delete', async () => {
            await request(app.getHttpServer())
                .delete(`/time-tracking/${compositeKeyFactory()}`)
                .set('Authorization', 'Bearer ' + jwt)
                .send()
                .expect(200);
        });

        it('should send 403 Forbidden because the user does not have the admin role', async () => {

            user = await createTestUser(usersService, {roles: []});
            jwt = (await authService.login(user)).access_token;

            await request(app.getHttpServer())
                .delete(`/time-tracking/${compositeKeyFactory()}`)
                .set('Authorization', 'Bearer ' + jwt)
                .send()
                .expect(403);
        });
    });

    describe('/time-tracking GET', () => {
        it('should return a list of timeTracking data within the given time range', async () => {

            await sleep(1000);

            const secondUserTask = await createTestUserTask(usersTasksService, user, await createTestTask(tasksService));

            await sleep(1000);
            await createTestUserTask(usersTasksService, user, await createTestTask(tasksService));

            const response = await request(app.getHttpServer())
                .get(`/time-tracking`)
                .query(
                    {
                        'time-range-start': secondUserTask.task.scheduledStart,
                        'time-range-end': secondUserTask.task.scheduledEnd
                    }
                )
                .set('Authorization', 'Bearer ' + jwt)
                .send()
                .expect(200);

            const expected = convertToRawResponse(constructTimeTrackingResponseDto(secondUserTask));
            const actual = response.body;

            expect(actual).toEqual(expect.arrayContaining<RawResponseTimeTrackingDto>(expected));
        });


        it('should return a list of timeTracking data within the given time range', async () => {

            await sleep(1000);

            const secondUserTask = await createTestUserTask(usersTasksService, user, await createTestTask(tasksService));
            const thirdUserTask = await createTestUserTask(usersTasksService, user, await createTestTask(tasksService));

            const response = await request(app.getHttpServer())
                .get(`/time-tracking`)
                .query(
                    {
                        'time-range-start': new Date(Date.now() - 5000),
                        'time-range-end': new Date(Date.now() + 5000)
                    }
                ).set('Authorization', 'Bearer ' + jwt)
                .send()
                .expect(200);

            const expected = convertToRawResponse([
                constructTimeTrackingResponseDto(userTask),
                constructTimeTrackingResponseDto(secondUserTask),
                constructTimeTrackingResponseDto(thirdUserTask),
            ]);

            const actual = response.body;

            expect(actual).toEqual(expect.arrayContaining<RawResponseTimeTrackingDto>(expected));
        });

        it('should send 403 Forbidden for a user without the admin role', async () => {

            user = await createTestUser(usersService, {roles: []});
            jwt = (await authService.login(user)).access_token;

            await request(app.getHttpServer())
                .get(`/time-tracking`)
                .query(
                    {
                        'time-range-start': new Date(Date.now() - 5000),
                        'time-range-end': new Date(Date.now() + 5000)
                    }
                )
                .set('Authorization', 'Bearer ' + jwt)
                .send()
                .expect(403);
        });

        it('should return a list of timeTracking data for a specific user within the given time range', async () => {

            const secondUserTask = await createTestUserTask(usersTasksService, user, await createTestTask(tasksService));
            const thirdUserTask = await createTestUserTask(usersTasksService, await createTestUser(usersService), await createTestTask(tasksService));

            const response = await request(app.getHttpServer())
                .get(`/time-tracking`)
                .query(
                    {
                        'user-id': thirdUserTask.user.id,
                        'time-range-start': new Date(Date.now() - 5000),
                        'time-range-end': new Date(Date.now() + 5000)
                    }
                )
                .set('Authorization', 'Bearer ' + jwt)
                .send()
                .expect(200);

            const expected = convertToRawResponse(constructTimeTrackingResponseDto(thirdUserTask));
            const actual = response.body;

            expect(actual).toEqual(expect.arrayContaining<RawResponseTimeTrackingDto>(expected));
        });

        it(
            `should send 403 Forbidden if the current user has no admin role
            and the requested user id does not match the current users id`, async () => {

                user = await createTestUser(usersService, {roles: []});
                jwt = (await authService.login(user)).access_token;


                await request(app.getHttpServer())
                    .get('/time-tracking')
                    .query(
                        {
                            'user-id': user.id + 1,
                            'time-range-start': new Date(Date.now() - 5000),
                            'time-range-end': new Date(Date.now() + 5000)
                        }
                    )
                    .set('Authorization', 'Bearer ' + jwt)
                    .send()
                    .expect(403);
            });


        it('should return a list of timeTracking data for a specific customer name', async () => {

            const secondUserTask = await createTestUserTask(usersTasksService, user, await createTestTask(tasksService));
            const thirdUserTask = await createTestUserTask(usersTasksService, await createTestUser(usersService), await createTestTask(tasksService));

            const response = await request(app.getHttpServer())
                .get(`/time-tracking`)
                .query(
                    {
                        'customer-name': thirdUserTask.task.customerName
                    }
                )
                .set('Authorization', 'Bearer ' + jwt)
                .send()
                .expect(200);

            const expected = convertToRawResponse(
                [
                    constructTimeTrackingResponseDto(userTask),
                    constructTimeTrackingResponseDto(secondUserTask),
                    constructTimeTrackingResponseDto(thirdUserTask),
                ]
            );
            const actual = response.body;

            expect(actual).toEqual(expect.arrayContaining<RawResponseTimeTrackingDto>(expected));
        });


        it('should return a list of timeTracking data for a specific task name', async () => {

            const secondUserTask = await createTestUserTask(usersTasksService, user, await createTestTask(tasksService));
            const thirdUserTask = await createTestUserTask(usersTasksService, await createTestUser(usersService), await createTestTask(tasksService));

            const response = await request(app.getHttpServer())
                .get(`/time-tracking`)
                .query(
                    {
                        'task-name': thirdUserTask.task.name
                    }
                )
                .set('Authorization', 'Bearer ' + jwt)
                .send()
                .expect(200);

            const expected = convertToRawResponse(
                [
                    constructTimeTrackingResponseDto(userTask),
                    constructTimeTrackingResponseDto(secondUserTask),
                    constructTimeTrackingResponseDto(thirdUserTask),
                ]
            );
            const actual = response.body;

            expect(actual).toEqual(expect.arrayContaining<RawResponseTimeTrackingDto>(expected));
        });
    });

    afterEach(async () => {
        // Closing the db and redis connection after each test is an alternative to closing the application after each test
        // but its not really faster (only tested with create and update user)

        // await getConnection().close();
        // await redisService.onApplicationShutdown();
        await app.close();
    });
});
