import {Test, TestingModule} from '@nestjs/testing';
import {BadRequestException, INestApplication} from '@nestjs/common';
import {AppModule} from '../src/app.module';
import {TasksService} from "../src/tasks/tasks.service";
import {CreateTaskDto} from "../src/tasks/dto/create-task.dto";
import {Task} from "../src/entities";
import {createTestTask} from "./shared-scripts/create-test-task";
import {resetDataBase} from "./shared-scripts/reset-data-base";
import {ConfigService} from "@nestjs/config";
import {SubjectWithoutIdentifierError} from "typeorm/error/SubjectWithoutIdentifierError";

describe('TasksService (e2e)', () => {
    let app: INestApplication;
    let tasksService: TasksService;

    let task: Task;

    beforeEach(async () => {

        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        tasksService = moduleFixture.get<TasksService>(TasksService);

        await resetDataBase(moduleFixture.get<ConfigService>(ConfigService));
        task = await createTestTask(tasksService);

        await app.init();
    });

    describe('findOne', () => {
        it('should return the task entity belonging to the given primary key', async () => {
            const actual = await tasksService.findOne(task.id);
            expect(actual).toStrictEqual(task);
        });

        it('should return undefined if no entity matches the given id', async () => {
            const actual = await tasksService.findOne(undefined);
            expect(actual).toBeUndefined();
        });
    });

    describe('create', () => {
        it('should create a new task', async () => {
            const actual = await tasksService.create(task as CreateTaskDto)
            expect(actual).toStrictEqual(task);
        });
    });

    describe('update', () => {
        it('should update a task', async () => {
            const expected = {...task, name: 'another name'};
            const actual = await tasksService.update({id: task.id, name: expected.name})
            expect(actual).toStrictEqual(expected);
        });

        it(`should throw an ${BadRequestException.name} when no task with the given id can be found`, async () => {
            await expect(tasksService.update({...task, id: undefined})).rejects.toThrow(BadRequestException);
        });
    });

    describe('delete', () => {
        it('should remove the task without throwing an exception', async () => {
            await expect(tasksService.delete(task.id)).resolves.not.toThrow();
        });

        it('should reject when the id does not exist', async () => {
            await expect(tasksService.delete(undefined)).rejects.toThrow(SubjectWithoutIdentifierError);
        });
    });


    afterEach(async () => {
        // Closing the db and redis connection after each test is an alternative to closing the application after each test
        // but its not really faster (only tested with create and update user)

        // await getConnection().close();
        // await redisService.onApplicationShutdown();
        await app.close();
    });
});
