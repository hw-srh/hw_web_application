import {Test, TestingModule} from '@nestjs/testing';
import {INestApplication} from '@nestjs/common';
import * as request from 'supertest';
import {AppModule} from '../src/app.module';
import {CreateUserDto} from "../src/common/shared_interfaces/dto/user/create-user.dto";
import {UsersService} from "../src/users/users.service";
import {AuthService} from "../src/auth/auth.service";
import {Role, User} from "../src/entities";
import {ConfigService} from "@nestjs/config";
import {resetDataBase} from "./shared-scripts/reset-data-base";
import {RedisService} from "../src/redis/redis.service";
import {basicTestUserData, createTestUser, removeTestUserDataFromRedis} from "./shared-scripts/create-test-user";
import {v1 as uuidv1} from 'uuid';
import {ResponseUserDto} from "../src/common/shared_interfaces/dto/user/response-user.dto";
import {UpdateUserDto} from "../src/users/dto/update-user.dto";

describe('UsersController (e2e)', () => {
    let app: INestApplication;
    let usersService: UsersService;
    let authService: AuthService;
    let redisService: RedisService;
    let jwt: string;
    let testUser: User;


    beforeEach(async () => {

        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        usersService = moduleFixture.get<UsersService>(UsersService);
        authService = moduleFixture.get<AuthService>(AuthService);
        redisService = moduleFixture.get<RedisService>(RedisService);

        await resetDataBase(moduleFixture.get<ConfigService>(ConfigService));
        testUser = await createTestUser(usersService);
        await removeTestUserDataFromRedis(authService, testUser);

        jwt = (await authService.login(testUser)).access_token;

        await app.init();
    });

    describe('/users POST create a new user', () => {
        it('should successfully create a new user', async () => {

            const email = `${uuidv1()}@test.de`;

            const createUserDto: CreateUserDto = {
                ...basicTestUserData,
                email,
            };

            const response = await request(app.getHttpServer())
                .post('/users')
                .set('Authorization', 'Bearer ' + jwt)
                .send([createUserDto])
                .expect(201);

            const actual = response.body;

            const expected: ResponseUserDto[] = [{
                id: actual[0].id,
                email,
                username: email, // default value
                workHoursPerWeek: 40, // default value
                roles: basicTestUserData.roles,
                firstname: basicTestUserData.firstname,
                lastname: basicTestUserData.lastname,
            }];

            expect(actual).toStrictEqual(expected);
        });

        it('should fail to create a new user because of a malformed email', async () => {

            const email = 'wrong_email@';

            const createUserDto: CreateUserDto = {
                ...basicTestUserData,
                email,
            };

            const response = await request(app.getHttpServer())
                .post('/users')
                .set('Authorization', 'Bearer ' + jwt)
                .send([createUserDto])
                .expect(400);

            const actual = response?.body;

            const expected = {
                "statusCode": 400,
                "error": "Bad Request",
                "message": ["email must be an email"]
            };

            expect(actual).toStrictEqual(expected);
        });

    });

    describe('/users PATCH', () => {

        it('should update an existing users data (expect password)', async () => {

            delete testUser.password;

            const newData: Partial<UpdateUserDto> = {
                firstname: 'another',
                lastname: 'another',
                workHoursPerWeek: 60,
                roles: ['admin'],
            }

            const updateUserDto: UpdateUserDto = {
                id: testUser.id,
                ...newData
            }

            const response = await request(app.getHttpServer())
                .patch('/users')
                .set('Authorization', 'Bearer ' + jwt)
                .send(updateUserDto)
                .expect(200);

            const actual = response.body;

            const expected = {
                ...testUser,
                ...newData
            };

            expect(actual).toStrictEqual(expected);
        });


        it('should update an existing users password', async () => {

            delete testUser.password;

            const updateUserDto: UpdateUserDto = {
                id: testUser.id,
                password: basicTestUserData.password,
                newPassword: basicTestUserData.password + 'new'
            };

            const response = await request(app.getHttpServer())
                .patch('/users')
                .set('Authorization', 'Bearer ' + jwt)
                .send(updateUserDto)
                .expect(200);

            const actual = response.body;

            const expected = {
                ...testUser,
                roles: testUser.roles.map((role: Role) => role.id)
            };

            expect(actual).toStrictEqual(expected);
        });

        it('should send code 400 bad request if the old password is incorrect', async () => {

            const updateUserDto: UpdateUserDto = {
                id: testUser.id,
                password: basicTestUserData.password + 'wrong',
                newPassword: basicTestUserData.password + 'new'
            };

            await request(app.getHttpServer())
                .patch('/users')
                .set('Authorization', 'Bearer ' + jwt)
                .send(updateUserDto)
                .expect(400);
        });

        it('should send code 401 unauthorized if the user to update does not exist', async () => {

            const updateUserDto: UpdateUserDto = {
                id: 10_000_000,
            };

            await request(app.getHttpServer())
                .patch('/users')
                .set('Authorization', 'Bearer ' + jwt)
                .send(updateUserDto)
                .expect(401);
        });
    });

    describe('get user(s) /users/* GET', () => {
        it('/users/:id GET should get the user with the id', async () => {

            delete testUser.password;

            const response = await request(app.getHttpServer())
                .get(`/users/${testUser.id}`)
                .set('Authorization', 'Bearer ' + jwt)
                .expect(200);

            const actual = response.body;

            const expected = {
                ...testUser,
                roles: testUser.roles.map((role: Role) => role.id),
            };

            expect(actual).toStrictEqual(expected);
        });

        describe('get multiple users', () => {
            it('/users GET should return all users', async () => {

                delete testUser.password;

                const anotherUser = await createTestUser(usersService, {}, ["password"]);

                const response = await request(app.getHttpServer())
                    .get('/users')
                    .set('Authorization', 'Bearer ' + jwt)
                    .expect(200);

                const actual = response.body;

                const expected = [
                    {
                        ...testUser,
                        roles: testUser.roles.map(role => role.id),
                    },
                    {
                        ...anotherUser,
                        roles: anotherUser.roles.map(role => role.id),
                    },
                ];

                expect(actual).toStrictEqual(expected);
            });

            it('/users/others GET should return all users except for the current user', async () => {

                const anotherUser = await createTestUser(usersService, {}, ["password"]);

                const response = await request(app.getHttpServer())
                    .get('/users/others')
                    .set('Authorization', 'Bearer ' + jwt)
                    .expect(200);

                const actual = response.body;

                const expected = [
                    {
                        ...anotherUser,
                        roles: anotherUser.roles.map(role => role.id),
                    },
                ];

                expect(actual).toStrictEqual(expected);
            });
        });
    });


    it('/users/:id DELETE should delete the user using the specified id', async () => {

        await request(app.getHttpServer())
            .delete(`/users/${testUser.id}`)
            .set('Authorization', 'Bearer ' + jwt)
            .expect(200);

        const expected = undefined;
        const actual = await usersService.findOne(testUser.id);

        expect(actual).toStrictEqual(expected);
    });

    afterEach(async () => {
        // Closing the db and redis connection after each test is an alternative to closing the application after each test
        // but its not really faster (only tested with create and update user)

        // await getConnection().close();
        // await redisService.onApplicationShutdown();
        await app.close();
    });
});
