import {Test, TestingModule} from '@nestjs/testing';
import {INestApplication} from '@nestjs/common';
import * as request from 'supertest';
import {AppModule} from '../src/app.module';
import {UsersService} from "../src/users/users.service";
import {AuthService} from "../src/auth/auth.service";
import {ConfigService} from "@nestjs/config";
import {resetDataBase} from "./shared-scripts/reset-data-base";
import {createTestUser, removeTestUserDataFromRedis} from "./shared-scripts/create-test-user";
import {PasswordResetToken, User} from "../src/entities";
import {getRepositoryToken} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {SmtpConfig} from "../src/config/interfaces/smtp-config.interface";
import axios from "axios";
import {PasswordResetTokensService} from "../src/password-reset-tokens/password-reset-tokens.service";
import {randomBytes} from "crypto";
import {PasswordResetDto} from "../src/common/shared_interfaces/dto/password-reset/password-reset.dto";
import {sleep} from "../src/common/utils/sleep.util";

describe('PasswordResetController (e2e)', () => {
    let app: INestApplication;
    let usersService: UsersService;
    let authService: AuthService;
    let configService: ConfigService;
    let passwordResetTokensService: PasswordResetTokensService;

    let passwordResetTokenRepository: Repository<PasswordResetToken>;


    let testUser: User;


    beforeEach(async () => {

        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = moduleFixture.createNestApplication();

        usersService = moduleFixture.get<UsersService>(UsersService);
        authService = moduleFixture.get<AuthService>(AuthService);
        configService = moduleFixture.get(ConfigService);
        passwordResetTokensService = moduleFixture.get(PasswordResetTokensService);

        passwordResetTokenRepository = moduleFixture.get(getRepositoryToken(PasswordResetToken));

        await resetDataBase(moduleFixture.get<ConfigService>(ConfigService));
        testUser = await createTestUser(usersService, {email: configService.get<SmtpConfig>('smtp').auth.user});
        await removeTestUserDataFromRedis(authService, testUser);

        // not all api v1 methods have a api v2 equivalent
        await axios.delete('http://127.0.0.1:8025/api/v1/messages');
        await app.init();
    });

    describe('/password-reset/:email POST', () => {

        let host = 'localhost';

        const requestPasswordResetLink = async (email: string) => {
            await request(app.getHttpServer())
                .post(`/password-reset/${encodeURIComponent(email)}`)
                .send()
                .expect((res: any) => {
                    host = res.request.host;
                    return res.status === 201;
                });
        };

        const fetchEmailBodyFromServer = async (email: string) => {
            const emailOnServer = await axios.get(`http://127.0.0.1:8025/api/v2/search?kind=from&query=${email}`);

            const body = emailOnServer?.data?.items[0]?.Content?.Body;

            if (!body) {
                return null;
            }

            return Buffer.from(body, 'base64')?.toString('utf-8');
        };

        it('should send a password reset link', async () => {

            await requestPasswordResetLink(testUser.email);

            // the server uses a random offset between 0-200 ms to prevent sniffing for existing email addresses
            await sleep(300);
            const actualEmailBody = await fetchEmailBodyFromServer(testUser.email);
            const expectedEmailBodyPattern = new RegExp(`^<a href="https:\/\/${host}\/app\/password-reset\/[0-f]{64}">Click to reset your password<\/a>$`);
            expect(actualEmailBody).toMatch(expectedEmailBodyPattern);
        });

        it('should not send a password reset link because no user with the given email exists', async () => {

            await requestPasswordResetLink(testUser.email + 'does-not-exist');

            // the server uses a random offset between 0-200 ms to prevent sniffing for existing email addresses
            await sleep(300);
            const actualEmailBody = await fetchEmailBodyFromServer(testUser.email + 'does-not-exist');
            expect(actualEmailBody).toBeNull();
        });
    });

    describe('/password-reset POST', () => {

        let jwt;
        let passwordResetToken;
        const sendPasswordResetRequest = async (passwordResetDto: PasswordResetDto, expectedStatusCode: number = 201) => {
            await request(app.getHttpServer())
                .post('/password-reset')
                .set('Authorization', 'Bearer ' + jwt)
                .send(passwordResetDto)
                .expect(expectedStatusCode);
        };


        beforeEach(async () => {
            jwt = (await authService.login(testUser)).access_token;
            passwordResetToken = randomBytes(32).toString('hex');
            await passwordResetTokensService.save({userId: testUser.id, token: passwordResetToken});
        });

        it('should set the new password', async () => {

            await sendPasswordResetRequest({
                email: testUser.email,
                password: 'another_password',
                token: passwordResetToken
            });

            const userWithNewPasswordHash = await usersService.findOne(testUser.id);
            expect(userWithNewPasswordHash.password).not.toEqual(testUser.password);
        });

        it('should not set the new password because the email does not match the user belonging to the token', async () => {

            await sendPasswordResetRequest({
                email: testUser.email + 'wrong-email',
                password: 'another_password',
                token: passwordResetToken
            }, 401);


            const user = await usersService.findOne(testUser.id);
            expect(user.password).toEqual(testUser.password);
        });

        it('should not set the new password because the token does not exist', async () => {

            await sendPasswordResetRequest({
                email: testUser.email,
                password: 'another_password',
                token: passwordResetToken + 'wrong-token'
            }, 401);

            const user = await usersService.findOne(testUser.id);
            expect(user.password).toEqual(testUser.password);
        });

        it('should not set the new password because the token has expired ( > 30 minutes)', async () => {

            await passwordResetTokenRepository.update(testUser.id, {
                created: new Date(Date.now() - 31 * 60000)
            });

            await sendPasswordResetRequest({
                email: testUser.email,
                password: 'another_password',
                token: passwordResetToken
            }, 400);

            const user = await usersService.findOne(testUser.id);
            expect(user.password).toEqual(testUser.password);
        });
    });


    afterEach(async () => {
        // Closing the db and redis connection after each test is an alternative to closing the application after each test
        // but its not really faster (only tested with create and update user)

        // await getConnection().close();
        // await redisService.onApplicationShutdown();
        await app.close();
    });
});
