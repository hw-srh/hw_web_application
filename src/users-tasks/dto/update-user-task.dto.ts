import {CreateUserTaskDto} from "./create-user-task.dto";

export type UpdateUserTaskDto = Partial<CreateUserTaskDto>;