import {TimeRangeDto} from "../../common/shared_interfaces/dto/util/time-range.util.dto";

export interface FilterUserTaskDto {
    userId?: number;
    timeRange?: TimeRangeDto;
    taskName?: string;
    customerName?: string;
}