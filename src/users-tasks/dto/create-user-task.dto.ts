import {Task, User} from "../../entities";

export interface CreateUserTaskDto {
    actualBreakTime: string,
    actualEnd: Date,
    actualStart: Date,
    task: Task,
    taskId: number,
    user: User,
    userId: number,
    workProtocol?: string
}