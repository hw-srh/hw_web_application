import {Test, TestingModule} from '@nestjs/testing';
import {UsersTasksService} from './users-tasks.service';
import {Repository} from "typeorm";
import {Task, User, UserTask} from "../entities";
import {getRepositoryToken} from "@nestjs/typeorm";
import {SelectQueryBuilder} from "typeorm/query-builder/SelectQueryBuilder";

describe('UsersTasksService', () => {
    let usersTasksService: UsersTasksService;

    let userTaskRepositoryMock: Repository<UserTask>;

    let user: User;
    let task: Task;
    let userTask: UserTask;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                UsersTasksService,
                {
                    provide: SelectQueryBuilder,
                    useFactory: () => (new class {
                        leftJoinAndSelect() {
                            return this;
                        }

                        where() {
                            return this;
                        }

                        andWhere() {
                            return this;
                        }

                        getMany() {
                            return [userTask];
                        }
                    }())
                },
                {
                    provide: getRepositoryToken(UserTask),
                    useFactory: (selectQueryBuilder: SelectQueryBuilder<UserTask>) => ({
                        save: jest.fn(async (updateData) => Object.assign(new UserTask(), {...userTask, ...updateData})),
                        findOne: jest.fn(async () => userTask),
                        createQueryBuilder: jest.fn(() => selectQueryBuilder)
                    }),
                    inject: [SelectQueryBuilder]
                },
            ],
        }).compile();

        user = Object.assign(new User(), {
            email: "",
            firstname: "",
            id: 0,
            lastname: "",
            password: "",
            passwordResetTokens: [],
            roles: [],
            userTasks: [],
            username: "",
            workHoursPerWeek: 0
        });

        task = Object.assign(new Task, {
            customerDescription: "",
            id: 0,
            name: "",
            scheduledBreakTime: "",
            scheduledEnd: undefined,
            scheduledStart: undefined,
            taskDescription: ""
        });

        userTask = Object.assign(new UserTask(), {
            actualBreakTime: undefined,
            actualEnd: undefined,
            actualStart: undefined,
            task,
            taskId: task.id,
            user,
            userId: user.id,
            workProtocol: undefined
        });


        userTaskRepositoryMock = module.get(getRepositoryToken(UserTask));

        usersTasksService = module.get<UsersTasksService>(UsersTasksService);
    });

    it('should be defined', () => {
        expect(usersTasksService).toBeDefined();
    });

    describe('findOne', () => {
        it('should find the userTask entity matching the userId and taskId', async () => {
            const actual = await usersTasksService.findOne({userId: userTask.userId, taskId: userTask.taskId});
            expect(actual).toStrictEqual(userTask);
        });
    });

    describe('create', () => {
        it('should create a new userTask entity', async () => {
            const actual = await usersTasksService.create(userTask);
            expect(actual).toStrictEqual(userTask);
        });
    });

    describe('update', () => {
        it('should update an userTask entity', async () => {
            const workProtocol = "some protocol";
            const expected = Object.assign(new UserTask(), {...userTask, workProtocol});
            const actual = await usersTasksService.update(expected);
            expect(actual).toStrictEqual(expected);
        });
    });


    describe('findAll', () => {
        it('should return a list of userTask entities filtered according to the provided filter', async () => {
            const expected = [userTask];
            const actual = await usersTasksService.findAll({timeRange: {start: new Date(), end: new Date()}});

            expect(actual).toStrictEqual(expected);
        })
    });


    describe('getUserBelongingToUserTask', () => {
        it('should return the user object matching the given composite key ', async () => {
            const expected = Object.assign(new User(), user);

            jest.spyOn(userTaskRepositoryMock, 'findOne').mockImplementation(async () => userTask);

            const actual = await usersTasksService.getUserBelongingToUserTask({
                userId: userTask.userId,
                taskId: userTask.taskId
            });
            expect(actual).toStrictEqual(expected);
        });
    });
});
