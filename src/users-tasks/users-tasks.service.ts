import {BadRequestException, Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {User, UserTask} from "../entities";
import {Repository} from "typeorm";
import {UpdateUserTaskDto} from "./dto/update-user-task.dto";
import {CreateUserTaskDto} from "./dto/create-user-task.dto";
import {FilterUserTaskDto} from "./dto/filter-user-task.dto";
import {UsersTasksFilterQueryBuilder} from "./users-tasks-filter-query.builder";

@Injectable()
export class UsersTasksService {
    constructor(
        @InjectRepository(UserTask)
        private readonly userTaskRepository: Repository<UserTask>,
    ) {
    }

    async findOne(ids: { userId: number, taskId: number }): Promise<UserTask> {
        return this.userTaskRepository.findOne({
            where: {...ids},
            join: {
                alias: "user_task",
                leftJoinAndSelect: {
                    "user": "user_task.user",
                    "task": "user_task.task"
                }
            }
        });
    }

    async update(updateUserTaskDto: UpdateUserTaskDto): Promise<UserTask | undefined> {
        const existingUserTask = await this.findOne({
            userId: updateUserTaskDto.userId,
            taskId: updateUserTaskDto.taskId
        });

        if (!existingUserTask) {
            throw new BadRequestException(
                `No user_task mapping found for the specified keys: 
                userId ${updateUserTaskDto.userId}, 
                taskId ${updateUserTaskDto.taskId}`
            );
        }

        const result = await this.userTaskRepository.save({
            ...existingUserTask,
            ...updateUserTaskDto
        });

        return result ? Object.assign(new UserTask(), result) : undefined;
    }

    async getUserBelongingToUserTask(ids: { userId: number, taskId: number }): Promise<User | null> {
        const userTask = await this.userTaskRepository.findOne({
            where: {...ids},
            join: {
                alias: "user_task",
                leftJoinAndSelect: {
                    "user": "user_task.user"
                }
            }
        });

        return userTask?.user;
    }

    async create(createUserTaskDto: CreateUserTaskDto): Promise<UserTask> {
        return this.userTaskRepository.save(createUserTaskDto);
    }

    async findAll(filter: FilterUserTaskDto): Promise<UserTask[]> {
        const queryBuilder = this.userTaskRepository.createQueryBuilder('user_task')
            .leftJoinAndSelect('user_task.user', 'user')
            .leftJoinAndSelect('user_task.task', 'task');

        UsersTasksFilterQueryBuilder.appendWhereStatements(queryBuilder, filter);

        return queryBuilder.getMany();
    }
}
