import {Module} from '@nestjs/common';
import {UsersTasksService} from './users-tasks.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Task, User, UserTask} from "../entities";

@Module({
  imports: [
    TypeOrmModule.forFeature([Task, UserTask, User]),
  ],
  providers: [UsersTasksService],
  exports: [UsersTasksService]
})
export class UsersTasksModule {
}
