import {FilterUserTaskDto} from "./dto/filter-user-task.dto";
import {SelectQueryBuilder} from "typeorm/query-builder/SelectQueryBuilder";
import {UserTask} from "../entities";
import {TimeRangeDto} from "../common/shared_interfaces/dto/util/time-range.util.dto";

interface Filter {
    query: string;
    paramsFactory: (paramValue: any) => object;
}

export class UsersTasksFilterQueryBuilder {

    private static filters: { [key: string]: Filter } = {
        taskName: {
            query: 'task.name = :taskName',
            paramsFactory: (taskName: string): object => ({taskName})
        },
        customerName: {
            query: 'task.customerName = :customerName',
            paramsFactory: (customerName: string): object => ({customerName})
        },
        timeRange: {
            query: 'task.scheduledStart BETWEEN :fromDate AND :toDate',
            paramsFactory: (timeRangeDto: TimeRangeDto): object => ({
                fromDate: timeRangeDto.start,
                toDate: timeRangeDto.end
            })
        },
        userId: {
            query: 'user.id = :userId',
            paramsFactory: (userId: number) => ({userId})
        }
    }

    public static appendWhereStatements(queryBuilder: SelectQueryBuilder<UserTask>, filter: FilterUserTaskDto): SelectQueryBuilder<UserTask> {
        for (const [filterPropertyName, value] of Object.entries(filter)) {
            const filter: Filter = UsersTasksFilterQueryBuilder.filters[filterPropertyName];
            queryBuilder.andWhere(filter.query, filter.paramsFactory(value))
        }
        return queryBuilder;
    }
}