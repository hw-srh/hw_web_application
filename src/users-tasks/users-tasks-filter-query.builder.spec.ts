import {Test, TestingModule} from '@nestjs/testing';
import {UsersTasksService} from './users-tasks.service';
import {Repository} from "typeorm";
import {UserTask} from "../entities";
import {getRepositoryToken} from "@nestjs/typeorm";
import {SelectQueryBuilder} from "typeorm/query-builder/SelectQueryBuilder";
import {UsersTasksFilterQueryBuilder} from "./users-tasks-filter-query.builder";
import {FilterUserTaskDto} from "./dto/filter-user-task.dto";

describe('UsersTasksFilterQueryBuilder', () => {

    let userTaskRepositoryMock: Repository<UserTask>;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                UsersTasksService,
                {
                    provide: SelectQueryBuilder,
                    useFactory: () => (new class {
                        leftJoinAndSelect() {
                            return this;
                        }

                        where() {
                            return this;
                        }

                        andWhere() {
                            return this;
                        }

                        getMany() {
                            return [];
                        }
                    }())
                },
                {
                    provide: getRepositoryToken(UserTask),
                    useFactory: (selectQueryBuilder: SelectQueryBuilder<UserTask>) => ({
                        createQueryBuilder: jest.fn(() => selectQueryBuilder)
                    }),
                    inject: [SelectQueryBuilder]
                },
            ],
        }).compile();

        userTaskRepositoryMock = module.get(getRepositoryToken(UserTask));

    });


    describe('appendWhereStatements', () => {
        it('should append statements for each filter property in the specified filter object', () => {

            const queryBuilder = userTaskRepositoryMock.createQueryBuilder('user_task');

            const filter: FilterUserTaskDto = {
                customerName: "some customer name",
                taskName: "some task name",
                timeRange: {
                    start: new Date(),
                    end: new Date()
                },
                userId: 7
            };

            expect(() => UsersTasksFilterQueryBuilder.appendWhereStatements(queryBuilder, filter)).not.toThrow()
        });
    });

});
