import {BadRequestException, forwardRef, Inject, Injectable, Logger, UnauthorizedException} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {In, Not, QueryFailedError, Repository} from 'typeorm';
import {Role, User} from "../entities";
import * as bcrypt from "bcrypt";
import {CreateUserDto} from "../common/shared_interfaces/dto/user/create-user.dto";
import {ResponseUserDto} from "../common/shared_interfaces/dto/user/response-user.dto";
import {InvalidChangePasswordRequestException} from "./exceptions/invalid-change-password-request.exception";
import {ConfigService} from "@nestjs/config";
import {BcryptConfig} from "../config/interfaces/bcrypt-config.interface";
import {PasswordResetService} from "../password-reset/password-reset.service";
import {Request} from "express";
import {UpdateUserDto} from "./dto/update-user.dto";
import {RandomStringGenerator} from "../common/utils/random-string-generator.util";

@Injectable()
export class UsersService {
    // 2^12 = 4096 salt calculations,
    // should be as high as is acceptable for the application in order to make brute force attacks harder
    private readonly defaultSaltRounds;

    constructor(
        @InjectRepository(User)
        private readonly usersRepository: Repository<User>,
        @InjectRepository(Role)
        private readonly rolesRepository: Repository<Role>,
        private readonly configService: ConfigService,
        private readonly logger: Logger,
        @Inject(forwardRef(() => PasswordResetService))
        private readonly passwordResetService: PasswordResetService
    ) {
        this.defaultSaltRounds = configService.get<BcryptConfig>('bcrypt').defaultSaltRounds;
    }

    async setNewPassword(user: User, newPassword: string): Promise<User | null> {
        return await this.usersRepository.save({
            ...user,
            password: await this.hashPassword(newPassword)
        });
    }

    async update(updateUserDto: UpdateUserDto): Promise<User> {

        const {newPassword, password, ...userUpdateData} = updateUserDto;
        const existingUser = await this.findOne(updateUserDto.id);

        if (!existingUser) {
            throw new UnauthorizedException('User not found');
        }


        const entityToSave = {
            ...existingUser,
            ...{
                ...userUpdateData,
                password: await this.getPasswordHashForUpdatedUser(updateUserDto, existingUser),
                roles: await this.constructRolesForUserUpdate(updateUserDto, existingUser)
            },
        };

        return await this.usersRepository.save(entityToSave);
    }

    async findAll(exceptions: number[] = []): Promise<User[]> {
        return this.usersRepository.find({
            where: {
                id: Not(In(exceptions))
            },
            join: {
                alias: "user",
                leftJoinAndSelect: {
                    roles: "user.roles"
                }
            }
        });
    }

    async findOne(id: number): Promise<User | undefined> {
        return await this.usersRepository.findOne({
            where: {id}, // using findOne(id) returns a value even if the id is undefined
            // see this github issue: https://github.com/typeorm/typeorm/issues/2500
            join: {
                alias: "user",
                leftJoinAndSelect: {
                    roles: "user.roles"
                }
            }
        });
    }

    async findOneByUserNameOrEmail(userNameOrEmail: string): Promise<User | undefined> {
        return this.usersRepository.findOne({
            where: [
                {email: userNameOrEmail},
                {username: userNameOrEmail}
            ],
            join: {
                alias: "user",
                leftJoinAndSelect: {
                    roles: "user.roles"
                }
            }
        });
    }

    async remove(id: number): Promise<void> {
        await this.usersRepository.remove(Object.assign(new User(), {id}));
    }

    async convertToOutgoingUserDto(user: User): Promise<ResponseUserDto> {
        const {password, roles, userTasks, passwordResetTokens, ...filteredUser} = user;
        return {
            ...filteredUser,
            roles: roles ? roles.map((role: Role) => role.id) : []
        }
    }

    private async constructRolesForUserUpdate(updateUserDto: UpdateUserDto, existingUser: User): Promise<Role[]> {

        if (!Array.isArray(updateUserDto?.roles)) {
            return existingUser.roles;
        }

        if (updateUserDto.roles.length === 0) {
            return [];
        }

        return await this.rolesRepository.find({
            where: {
                id: In(updateUserDto.roles)
            }
        });
    }

    private async getPasswordHashForUpdatedUser(updateUserDto: UpdateUserDto, existingUser: User): Promise<string> {

        const {newPassword, password} = updateUserDto;

        let passwordHash = existingUser.password;

        if (newPassword) {
            if (!await bcrypt.compare(password, existingUser.password)) {
                throw new InvalidChangePasswordRequestException("Invalid old password provided !");
            }
            passwordHash = await this.hashPassword(newPassword)
        }

        return passwordHash;
    }

    private async hashPassword(password: string): Promise<string> {
        try {
            return await bcrypt.hash(password, this.defaultSaltRounds);
        } catch (e) {
            this.logger.log(e.message);
            throw new UnauthorizedException();
        }
    }

    async create(createUserDto: CreateUserDto, request: Request): Promise<User> {
        const user: User = Object.assign(new User(), createUserDto);

        user.password = await this.hashPassword(await RandomStringGenerator.hex());
        user.username ??= user.email;

        if (user.roles && user.roles.length > 0) {
            user.roles = await this.rolesRepository.find({
                where: {
                    id: In([...user.roles ?? 'user'])
                }
            })
        }

        try {
            const result = await this.usersRepository.save(user);
            await this.passwordResetService.sendLink(result.email, request.headers.host);
            return result;
        } catch (e) {
            if (e instanceof QueryFailedError && e.message.includes('Duplicate entry')) {
                throw new BadRequestException(`User with email: ${createUserDto.email} or username: ${createUserDto.username ?? "NOT_DEFINED"} already exists`);
            }

            throw e;
        }
    }
}