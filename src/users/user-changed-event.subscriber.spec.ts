import {Test} from '@nestjs/testing';
import {UsersService} from "./users.service";
import {User} from "../entities";
import {Connection, UpdateEvent} from "typeorm";
import {AuthService} from "../auth/auth.service";
import {UserChangedEventSubscriber} from "./user-changed-event.subscriber";


describe('UsersService', () => {

    let userChangedEventSubscriber: UserChangedEventSubscriber;
    let authService: AuthService;

    const user: User = {
        id: 42,
        email: "test",
        password: "test",
        firstname: "test",
        lastname: "test",
        username: "test",
        workHoursPerWeek: 40,
        roles: [{id: "admin",}, {id: "something_else"}]
    } as User;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: Connection,
                    useValue: {
                        subscribers: []
                    }
                },
                {
                    provide: AuthService,
                    useValue: {
                        invalidateJwtsForUser: jest.fn()
                    }
                },
                UserChangedEventSubscriber
            ],
        }).compile();

        userChangedEventSubscriber = moduleRef.get<UserChangedEventSubscriber>(UserChangedEventSubscriber);
        authService = moduleRef.get<AuthService>(AuthService);
    });

    it('should be defined', () => {
        expect(userChangedEventSubscriber).toBeInstanceOf(UserChangedEventSubscriber);
    })

    it('should invalidate the jwts if the users roles changed', async () => {

        const event = {
            entity: user,
            databaseEntity: {
                ...user,
                roles: [
                    {id: "another_role", users: null},
                    {id: "yet_another_role", users: null}]
            }
        } as UpdateEvent<User>;

        await userChangedEventSubscriber.afterUpdate(event);

        expect(authService.invalidateJwtsForUser).toBeCalledWith(user);
    });

    it('should invalidate the jwts if the users password hash changed', async () => {

        const expected = {
            ...user,
            password: "another"
        };

        const event = {
            entity: expected,
            databaseEntity: user
        } as UpdateEvent<User>;

        await userChangedEventSubscriber.afterUpdate(event);

        expect(authService.invalidateJwtsForUser).toBeCalledWith(expected);
    });

    it('should invalidate the jwts if the users email changed', async () => {

        const expected = {
            ...user,
            email: "another"
        };

        const event = {
            entity: expected,
            databaseEntity: user
        } as UpdateEvent<User>;

        await userChangedEventSubscriber.afterUpdate(event);

        expect(authService.invalidateJwtsForUser).toBeCalledWith(expected);
    });

    it('should invalidate the jwts if the users username changed', async () => {

        const expected = {
            ...user,
            username: "another"
        };

        const event = {
            entity: expected,
            databaseEntity: user
        } as UpdateEvent<User>;

        await userChangedEventSubscriber.afterUpdate(event);

        expect(authService.invalidateJwtsForUser).toBeCalledWith(expected);
    });

    it('should not invalidate the jwts if the users firstname changed', async () => {

        const expected = {
            ...user,
            firstname: "another"
        };

        const event = {
            entity: expected,
            databaseEntity: user
        } as UpdateEvent<User>;

        await userChangedEventSubscriber.afterUpdate(event);

        expect(authService.invalidateJwtsForUser).not.toBeCalled();
    });

    it('should not invalidate the jwts if the users lastname changed', async () => {

        const expected = {
            ...user,
            lastname: "another"
        };

        const event = {
            entity: expected,
            databaseEntity: user
        } as UpdateEvent<User>;

        await userChangedEventSubscriber.afterUpdate(event);

        expect(authService.invalidateJwtsForUser).not.toBeCalled();
    });

    it('should not invalidate the jwts if the users work hours changed changed', async () => {

        const expected = {
            ...user,
            workHoursPerWeek: 80
        };

        const event = {
            entity: expected,
            databaseEntity: user
        } as UpdateEvent<User>;

        await userChangedEventSubscriber.afterUpdate(event);

        expect(authService.invalidateJwtsForUser).not.toBeCalled();
    });
});