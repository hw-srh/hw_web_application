import {Test} from '@nestjs/testing';
import {UsersService} from "./users.service";
import {Role, User} from "../entities";
import {getRepositoryToken} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {CreateUserDto} from "../common/shared_interfaces/dto/user/create-user.dto";
import * as bcrypt from "bcrypt";
import {InvalidChangePasswordRequestException} from "./exceptions/invalid-change-password-request.exception";
import {Logger, UnauthorizedException} from "@nestjs/common";
import {ConfigService} from "@nestjs/config";
import {Request} from "express";
import {UpdateUserDto} from "./dto/update-user.dto";
import {UpdateUserExceptPasswordDto} from "../common/shared_interfaces/dto/user/update-user-except-password.dto";
import {PasswordResetService} from "../password-reset/password-reset.service";
import {RandomStringGenerator} from "../common/utils/random-string-generator.util";

describe('UsersService', () => {
    let usersService: UsersService;
    let userRepositoryMock: Repository<User>;
    let roleRepositoryMock: Repository<Role>;

    const user: User = {
        id: 42,
        email: "test",
        firstname: "test",
        lastname: "test",
        password: "test",
        username: "test",
        roles: [{id: "admin",}, {id: "something_else"}]
    } as User;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: ConfigService,
                    useValue: {
                        get: () => ({
                            defaultSaltRounds: 12,
                        }),
                    }
                },
                {
                    provide: getRepositoryToken(User),
                    useValue: {
                        save: jest.fn(async (user) => user as User),
                        remove: jest.fn(async () => user),
                        findOne: jest.fn(async () => user),
                        find: jest.fn(async () => [user, user])
                    }
                },
                {
                    provide: getRepositoryToken(Role),
                    useValue: {
                        find: jest.fn(async () => user.roles)
                    }
                },
                {
                    provide: PasswordResetService,
                    useValue: {
                        sendLink: () => {
                        }
                    }
                },
                Logger,
                UsersService,
            ],
        }).compile();

        userRepositoryMock = moduleRef.get(getRepositoryToken(User));
        roleRepositoryMock = moduleRef.get(getRepositoryToken(Role));

        usersService = moduleRef.get<UsersService>(UsersService);
    });

    describe('create', () => {
        it('should create a new user', async () => {
            const userToBeCreated: CreateUserDto = {...user, roles: user.roles.map(role => role.id)};

            jest.spyOn(RandomStringGenerator, 'hex').mockImplementation(async () => user.password);

            const {
                passwordResetTokens,
                userTasks,
                ...actual
            } = await usersService.create(Object.assign({}, userToBeCreated), {headers: {host: "localhost"}} as Request);

            const passwordSalt = actual.password.substr(0, 29);

            const expected = {
                ...userToBeCreated,
                password: await bcrypt.hash(user.password, passwordSalt),
                roles: user.roles,
            };

            expect(actual).toStrictEqual(expected);
        });
    });


    describe('update', () => {

        let existingUserWithPasswordHash: User;
        const basicUpdateDto: UpdateUserExceptPasswordDto = {
            ...user,
            roles: user.roles.map(role => role.id),
        }

        beforeEach(async () => {
            const userToBeCreated: CreateUserDto = {...user, roles: user.roles.map(role => role.id)};
            existingUserWithPasswordHash = await usersService.create(Object.assign({}, userToBeCreated), {headers: {host: "localhost"}} as Request);
            jest.spyOn(userRepositoryMock, "findOne").mockImplementation(async () => existingUserWithPasswordHash);
        });

        it('should update an existing user', async () => {

            const updateData: UpdateUserExceptPasswordDto = {
                ...basicUpdateDto,
                firstname: 'another'
            };

            const {passwordResetTokens, userTasks, ...actual} = await usersService.update(updateData);

            const expected = {
                ...updateData,
                password: existingUserWithPasswordHash.password,
                roles: user.roles,
            };

            expect(actual).toStrictEqual(expected);
        });

        it('should update an existing users password', async () => {

            const newPassword = 'newPassword';

            const updateData = {
                ...basicUpdateDto,
                firstname: 'another'
            };

            const {passwordResetTokens, userTasks, ...actual} = await usersService.update({
                ...updateData,
                password: user.password,
                newPassword: newPassword,
            });

            const passwordSalt = actual.password.substr(0, 29);

            const expected = {
                ...updateData,
                roles: user.roles,
                password: await bcrypt.hash(newPassword, passwordSalt)
            };

            expect(actual).toStrictEqual(expected);
        });

        it('should throw an exception because the old password is incorrect', async () => {

            const updateData: UpdateUserDto = {
                ...basicUpdateDto,
                password: 'wrongPassword',
                newPassword: 'newPassword',
                firstname: 'another'
            };

            await expect(usersService.update(updateData)).rejects.toThrow(InvalidChangePasswordRequestException);
        });

        it('should throw an exception because the user does not exist', async () => {

            jest.spyOn(userRepositoryMock, 'findOne').mockImplementation(async () => undefined);
            await expect(usersService.update(basicUpdateDto)).rejects.toThrow(UnauthorizedException);
        });
    });

    describe('findOne', () => {
        it('should return a single user', async () => {

            const expected = user;
            const actual = await usersService.findOne(user.id);

            expect(actual).toStrictEqual(expected);
        });
    });

    describe('findAll', () => {
        it('should return an array of users', async () => {

            const users: User[] = [user, user];

            const expected = await Promise.all(users);
            const actual = await usersService.findAll();

            expect(actual).toStrictEqual(expected);
        });
    });

    describe('setNewPassword', () => {
        it('should hash the new password and update the users password hash', async () => {

            jest.spyOn(userRepositoryMock, 'save').mockImplementation(async (userToUpdate: User) => userToUpdate);

            const actual = await usersService.setNewPassword(Object.assign(new User, user), user.password + 'new');

            const passwordSalt = actual.password.substr(0, 29);

            const expected = {
                ...user,
                password: await bcrypt.hash(user.password + 'new', passwordSalt)
            };

            expect(actual).toStrictEqual(expected);
        });
    });
});