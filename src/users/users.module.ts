import {forwardRef, Logger, Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {UsersController} from './users.controller';
import {UsersService} from './users.service';
import {Role, User} from "../entities";
import {RedisModule} from "../redis/redis.module";
import {UserChangedEventSubscriber} from "./user-changed-event.subscriber";
import {AuthModule} from "../auth/auth.module";
import {AdminScope} from "../auth/scopes/admin.scope";
import {PasswordResetModule} from "../password-reset/password-reset.module";

@Module({
    imports: [
        TypeOrmModule.forFeature([User, Role]),
        RedisModule,
        forwardRef(() => AuthModule),
        AdminScope,
        forwardRef(() => PasswordResetModule)
    ],
    providers: [
        UsersService,
        UserChangedEventSubscriber,
        Logger
    ],
    exports: [UsersService],
    controllers: [UsersController],
})
export class UsersModule {}