import {BadRequestException} from "@nestjs/common";

export class InvalidChangePasswordRequestException extends BadRequestException {
}