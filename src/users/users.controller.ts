import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseArrayPipe,
    ParseIntPipe,
    Patch,
    Post,
    Request,
    UseGuards,
    UsePipes,
    ValidationPipe
} from '@nestjs/common';
import {UsersService} from './users.service';
import {RestrictToScopes} from "../auth/decorators/scopes.decorator";
import {AdminScope} from "../auth/scopes/admin.scope";
import {ScopeGuard} from "../auth/guards/scope.guard";
import {CurrentUserScope} from "../auth/scopes/current-user.scope";
import {User} from "../entities";
import {ResponseUserDto} from "../common/shared_interfaces/dto/user/response-user.dto";
import {UsersUserIdLocator} from "./users-user-id.locator";
import {CreateUserDto} from "./dto/create-user.dto";
import {UpdateUserDto} from "./dto/update-user.dto";
import {TrimPipe} from "../common/pipes/trim.pipe";

@Controller('users')
@UseGuards(ScopeGuard)
@RestrictToScopes(new AdminScope())
export class UsersController {
    constructor(private readonly usersService: UsersService) {
    }

    @Post()
    @UsePipes(ValidationPipe, TrimPipe)
    async create(
        @Request() req,
        @Body(new ParseArrayPipe({items: CreateUserDto})) createUserDTOs: CreateUserDto[]
    ): Promise<ResponseUserDto[]> {

        const createUserJobs = [];

        for (const createUserDto of createUserDTOs) {
            createUserJobs.push(this.usersService.create(createUserDto, req));
        }

        const resultJobs = createUserJobs.map(async (createUserPromise: Promise<User>): Promise<ResponseUserDto> => {
            return await this.usersService.convertToOutgoingUserDto(await createUserPromise);
        })

        return Promise.all(resultJobs);
    }

    @RestrictToScopes(new CurrentUserScope(new UsersUserIdLocator()))
    @Patch()
    @UsePipes(ValidationPipe, TrimPipe)
    async update(@Body() updateUserDto: UpdateUserDto): Promise<ResponseUserDto> {
        const user = await this.usersService.update(updateUserDto);
        return this.usersService.convertToOutgoingUserDto(user);
    }

    @Get()
    async findAll(): Promise<ResponseUserDto[]> {
        const users = await this.usersService.findAll();
        const outgoingUsers: Promise<ResponseUserDto>[] = users.map((user: User) => this.usersService.convertToOutgoingUserDto(user));

        return await Promise.all(outgoingUsers);
    }

    @Get('/others')
    async findOtherUsers(@Request() req): Promise<ResponseUserDto[]> {
        const users = await this.usersService.findAll([req.user.id]);
        const outgoingUsers: Promise<ResponseUserDto>[] = users.map((user: User) => this.usersService.convertToOutgoingUserDto(user));
        return await Promise.all(outgoingUsers);
    }

    @RestrictToScopes(new CurrentUserScope())
    @Get(':id')
    @UsePipes(ValidationPipe)
    async findOne(@Param('id', new ParseIntPipe()) id: number): Promise<ResponseUserDto> {
        return this.usersService.convertToOutgoingUserDto(await this.usersService.findOne(id));
    }

    @Delete(':id')
    @UsePipes(ValidationPipe)
    remove(@Param('id', new ParseIntPipe()) id: number): Promise<void> {
        return this.usersService.remove(id);
    }
}
