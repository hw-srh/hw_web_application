import {Role, User} from "../entities";
import {Connection, EntitySubscriberInterface, RemoveEvent, UpdateEvent} from "typeorm";
import {Injectable} from "@nestjs/common";
import {InjectConnection} from "@nestjs/typeorm";
import {AuthService} from "../auth/auth.service";


@Injectable()
export class UserChangedEventSubscriber implements EntitySubscriberInterface {


    constructor(
        @InjectConnection()
        private readonly connection: Connection,
        private readonly authService: AuthService
    ) {
        connection.subscribers.push(this);
    }

    listenTo() {
        return User;
    }

    // this method currently only works as expected if the repository method remove is used and not the delete method
    // also note that using afterRemove results in an empty user object e.g. {id: undefined} and thus this method has to be used
    async beforeRemove(event: RemoveEvent<User>) {
        await this.authService.invalidateJwtsForUser(event.entity);
    }

    async afterUpdate(event: UpdateEvent<User>) {

        const oldEntity = event.databaseEntity;
        const newEntity = event.entity;

        if (this.didUsersAuthenticationDataChange(oldEntity, newEntity)) {
            await this.authService.invalidateJwtsForUser(newEntity);
        }
    }

    private didUsersAuthenticationDataChange(oldEntity: User, newEntity: User): boolean {
        const didRolesChange = (): boolean => {
            if (oldEntity.roles.length !== newEntity.roles.length) {
                return true;
            }

            const changedRoles = newEntity.roles.filter((newRole: Role) => {
                for (const oldRole of oldEntity.roles) {
                    if (newRole.id === oldRole.id) {
                        return false;
                    }
                }

                return true;
            });

            return changedRoles.length > 0;
        }

        return (
            (oldEntity.password !== newEntity.password) ||
            (oldEntity.email !== newEntity.email) ||
            (oldEntity.username !== newEntity.username) ||
            (didRolesChange())
        );
    }

}