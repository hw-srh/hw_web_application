import {IsArray, IsEmail, IsNumber, IsOptional, IsString, Max, MaxLength, Min} from "class-validator";
import {UpdateUserIncludingPasswordDto} from "../../common/shared_interfaces/dto/user/update-user-including-password.dto";

export class UpdateUserDto implements Partial<UpdateUserIncludingPasswordDto> {

    @IsNumber()
    @Min(1)
    @Max(4294967295)
    id: number;

    @IsOptional()
    @IsString()
    newPassword?: string;

    @IsOptional()
    @IsString()
    password?: string;

    @IsOptional()
    @IsString()
    firstname?: string;

    @IsOptional()
    @IsString()
    lastname?: string;

    @IsOptional()
    @IsEmail()
    email?: string;

    @IsOptional()
    @IsString()
    username?: string;

    @IsOptional()
    @Min(1)
    @Max(80)
    workHoursPerWeek?: number;

    @IsOptional()
    @IsArray()
    @IsString({each: true})
    @MaxLength(50, {each: true})
    roles?: string[];
}