import {IsArray, IsEmail, IsOptional, IsString, Max, MaxLength, Min} from 'class-validator';

import {CreateUserDto as CreateUserDtoInterface} from "../../common/shared_interfaces/dto/user/create-user.dto";

export class CreateUserDto implements CreateUserDtoInterface {

    @IsString()
    firstname: string;

    @IsString()
    lastname: string;

    @IsEmail()
    email: string;

    @IsOptional()
    @IsString()
    username?: string;

    @IsOptional()
    @Min(1)
    @Max(80)
    workHoursPerWeek?: number;

    @IsOptional()
    @IsArray()
    @IsString({each: true})
    @MaxLength(50, {each: true})
    roles?: string[];
}