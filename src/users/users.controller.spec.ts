import {Test} from '@nestjs/testing';
import {UsersService} from "./users.service";
import {UsersController} from "./users.controller";
import {Role, User} from "../entities";
import {CreateUserDto} from "../common/shared_interfaces/dto/user/create-user.dto";
import {ResponseUserDto} from "../common/shared_interfaces/dto/user/response-user.dto";
import {Repository} from "typeorm";
import {getRepositoryToken} from "@nestjs/typeorm";
import {repositoryProviderValueMock} from "../common/mocks/repository-provider-value.mock";
import {ConfigService} from "@nestjs/config";
import {Logger} from "@nestjs/common";
import {Request} from "express";
import {PasswordResetService} from "../password-reset/password-reset.service";


describe('UsersController', () => {
    let usersController: UsersController;
    let usersService: UsersService;
    let userRepositoryMock: Repository<User>;
    let roleRepositoryMock: Repository<Role>;

    const user: User = {
        id: 42,
        email: "test",
        password: "test",
        firstname: "test",
        lastname: "test",
        username: "test",
        roles: [{id: "admin",}, {id: "something_else"}]
    } as User;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [UsersController],
            providers: [
                UsersService,
                {
                    provide: PasswordResetService,
                    useValue: {
                        sendLink: () => {
                        }
                    }
                },
                {
                    provide: ConfigService,
                    useValue: {
                        get: () => ({
                            defaultSaltRounds: 12,
                        }),
                    }
                },
                {
                    provide: getRepositoryToken(User),
                    useValue: repositoryProviderValueMock
                },
                {
                    provide: getRepositoryToken(Role),
                    useValue: repositoryProviderValueMock
                },
                Logger
            ],
        }).compile();

        userRepositoryMock = moduleRef.get(getRepositoryToken(User));
        roleRepositoryMock = moduleRef.get(getRepositoryToken(Role));

        usersService = moduleRef.get<UsersService>(UsersService);
        usersController = moduleRef.get<UsersController>(UsersController);
    });

    describe('create', () => {
        it('should return an array of users', async () => {

            const userToBeCreated: CreateUserDto = {...user, roles: user.roles.map(role => role.id)};

            jest.spyOn(usersService, 'create').mockImplementation(async () => user);

            const expectedResult = [await usersService.convertToOutgoingUserDto(user)];

            const actualResult = await usersController.create(
                {headers: {host: "localhost"}} as Request,
                [userToBeCreated]
            );

            expect(actualResult).toStrictEqual(expectedResult);
        });
    });

    describe('findOne with the user id', () => {
        it('should return an a single user entity', async () => {
            jest.spyOn(usersService, 'findOne').mockImplementation(async () => user);

            const expectedResult = await usersService.convertToOutgoingUserDto(user);
            const actualResult = await usersController.findOne(user.id);
            expect(actualResult).toStrictEqual(expectedResult);
        });
    });

    describe('findAll', () => {
        it('should return an array of users (entities)', async () => {

            const users: User[] = [user, user];

            jest.spyOn(usersService, 'findAll').mockImplementation(async () => users);

            const expectedResult: Promise<ResponseUserDto>[] = users.map(async (user: User) => {
                return usersService.convertToOutgoingUserDto(user)
            });

            const actualResult = await usersController.findAll();

            expect(actualResult).toStrictEqual(await Promise.all(expectedResult));
        });
    });
});