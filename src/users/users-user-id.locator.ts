import {CurrentUserIdLocator} from "../auth/interfaces/current-user-id-locator.interface";
import {Request} from "express";

export class UsersUserIdLocator implements CurrentUserIdLocator {
    async findIds(req: Request): Promise<number[]> {
        return [req.body?.id];
    }
}