import {ParseTimestampPipe} from "./parse-timestamp-pipe.service";

describe('ParseTimestampStringPipe', () => {
  it('should be defined', () => {
    expect(new ParseTimestampPipe()).toBeDefined();
  });

  it('should return a date object for a valid js timestamp', () => {
    const expectedDate = new Date();
    const actualDate = (new ParseTimestampPipe()).transform(expectedDate.getTime(), undefined)
    expect(actualDate).toEqual(expectedDate);
  });
});
