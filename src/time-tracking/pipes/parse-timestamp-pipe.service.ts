import {ArgumentMetadata, Injectable, PipeTransform} from '@nestjs/common';

@Injectable()
export class ParseTimestampPipe implements PipeTransform<number, Date> {
    transform(timestamp: number, metadata: ArgumentMetadata): Date {
        return new Date(timestamp);
    }
}