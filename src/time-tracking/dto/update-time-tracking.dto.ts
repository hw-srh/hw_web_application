import {UpdateTimeTrackingDto as UpdateTimeTrackingDtoInterface} from "../../common/shared_interfaces/dto/time-tracking/update-time-tracking.dto";
import {IsDate, IsEmail, IsOptional, IsString, Length, Matches, MaxLength, MinLength} from "class-validator";
import {Type} from "class-transformer";

export class UpdateTimeTrackingDto implements UpdateTimeTrackingDtoInterface {

    @IsString()
    @MinLength(3)
    @MaxLength(21)
    @Matches('^[1-9][0-9]{0,9}-[1-9][0-9]{0,9}$') // actual maximum is 4294967295-4294967295 (unsigned 32 bit integer) but not validated here
    id: string;

    @IsOptional()
    @IsString()
    @Length(5, 8)
    @Matches('^[0-9]{2}:[0-9]{2}(:[0-9]{2}){0,1}$')
    actualBreakTime: string;

    @IsOptional()
    @Type(() => Date)
    @IsDate()
    actualEnd: Date;

    @IsOptional()
    @Type(() => Date)
    @IsDate()
    actualStart: Date;

    @IsOptional()
    @IsString()
    customerName: string;

    @IsOptional()
    @IsString()
    customerDescription: string;

    @IsOptional()
    @IsEmail()
    email: string;

    @IsOptional()
    @IsString()
    @Length(5, 8)
    @Matches('^[0-9]{2}:[0-9]{2}(:[0-9]{2}){0,1}$')
    scheduledBreakTime: string;

    @IsOptional()
    @Type(() => Date)
    @IsDate()
    scheduledEnd: Date;

    @IsOptional()
    @Type(() => Date)
    @IsDate()
    scheduledStart: Date;

    @IsOptional()
    @IsString()
    taskDescription: string;

    @IsOptional()
    @IsString()
    taskName: string;

    @IsOptional()
    @IsString()
    workProtocol: string;
}