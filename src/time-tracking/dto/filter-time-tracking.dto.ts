import {IsDate, IsNumberString, IsOptional, IsString} from "class-validator";
import {Expose, Type} from "class-transformer";
import {FilterTimeTrackingDtoWithTimeRange} from "../../common/shared_interfaces/dto/time-tracking/FilterTimeTrackingDtoWithTimeRange";

export class FilterTimeTrackingDto implements FilterTimeTrackingDtoWithTimeRange {

    @Expose({name: 'time-range-start', toClassOnly: true})
    @IsOptional()
    @Type(() => Date)
    @IsDate()
    timeRangeStart: Date;

    @Expose({name: 'time-range-end', toClassOnly: true})
    @IsOptional()
    @Type(() => Date)
    @IsDate()
    timeRangeEnd: Date;

    @Expose({name: 'task-name', toClassOnly: true})
    @IsOptional()
    @IsString()
    taskName: string;

    @Expose({name: 'customer-name', toClassOnly: true})
    @IsOptional()
    @IsString()
    customerName: string;

    @Expose({name: 'user-id', toClassOnly: true})
    @IsOptional()
    @IsNumberString()
    userId: number;
}