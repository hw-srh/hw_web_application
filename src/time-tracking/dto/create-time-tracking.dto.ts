import {CreateTimeTrackingDto as CreateTimeTrackingDtoInterface} from "../../common/shared_interfaces/dto/time-tracking/create-time-tracking.dto";
import {IsDate, IsEmail, IsOptional, IsString, Length, Matches} from "class-validator";
import {Type} from "class-transformer";

export class CreateTimeTrackingDto implements CreateTimeTrackingDtoInterface {

    @IsOptional()
    @IsString()
    @Length(5, 8)
    @Matches('^[0-9]{2}:[0-9]{2}(:[0-9]{2}){0,1}$')
    actualBreakTime: string;

    @IsOptional()
    @Type(() => Date)
    @IsDate()
    actualEnd: Date;

    @IsOptional()
    @Type(() => Date)
    @IsDate()
    actualStart: Date;

    @IsString()
    customerName: string;

    @IsString()
    customerDescription: string;

    @IsEmail()
    email: string;

    @IsOptional()
    @IsString()
    @Length(5, 8)
    @Matches('^[0-9]{2}:[0-9]{2}(:[0-9]{2}){0,1}$')
    scheduledBreakTime: string;

    @Type(() => Date)
    @IsDate()
    scheduledEnd: Date;

    @IsOptional()
    @Type(() => Date)
    @IsDate()
    scheduledStart: Date;

    @IsString()
    taskDescription: string;

    @IsString()
    taskName: string;

    @IsOptional()
    @IsString()
    workProtocol: string;

}