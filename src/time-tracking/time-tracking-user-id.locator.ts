import {CurrentUserIdLocator} from "../auth/interfaces/current-user-id-locator.interface";
import {Request} from "express";
import {User} from "../entities";
import {TimeTrackingService} from "./time-tracking.service";

export class TimeTrackingUserIdLocator implements CurrentUserIdLocator {

    async findIds(req: Request, otherDependencies: Map<{ new(...params: any): TimeTrackingService }, TimeTrackingService>): Promise<number[]> {

        const ids = [];

        for (const userTaskDTOs of req.body) {
            const timeTrackingService = otherDependencies.get(TimeTrackingService);
            const user: User | null = await timeTrackingService.getUserBelongingToUserTask(userTaskDTOs?.id);
            ids.push(user?.id);
        }

        return ids;
    }
}