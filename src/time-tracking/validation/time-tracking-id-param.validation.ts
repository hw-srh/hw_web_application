import {IsString, Matches, MaxLength, MinLength} from "class-validator";

export class TimeTrackingIdParamValidation {
    @IsString()
    @MinLength(3)
    @MaxLength(21)
    @Matches('[1-9][0-9]{0,9}-[1-9][0-9]{0,9}') // actual maximum is 4294967295-4294967295 (unsigned 32 bit integer)
    id: string;
}