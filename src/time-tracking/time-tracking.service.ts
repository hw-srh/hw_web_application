import {BadRequestException, Injectable} from '@nestjs/common';
import {UsersService} from "../users/users.service";
import {TasksService} from "../tasks/tasks.service";
import {UsersTasksService} from "../users-tasks/users-tasks.service";
import {Task, User, UserTask} from "../entities";
import {ResponseTimeTrackingDto} from "../common/shared_interfaces/dto/time-tracking/response-time-tracking.dto";
import {CreateTimeTrackingDto} from "../common/shared_interfaces/dto/time-tracking/create-time-tracking.dto";
import {UpdateTimeTrackingDto} from "../common/shared_interfaces/dto/time-tracking/update-time-tracking.dto";
import {constructTimeTrackingResponseDto} from "./time-tracking-response.builder";
import {UpdateUserTaskDto} from "../users-tasks/dto/update-user-task.dto";
import {FilterUserTaskDto} from "../users-tasks/dto/filter-user-task.dto";
import {FilterTimeTrackingDtoWithTimeRange} from "../common/shared_interfaces/dto/time-tracking/FilterTimeTrackingDtoWithTimeRange";

export interface UserIdAndTaskId {
    userId: string,
    taskId: string
}

@Injectable()
export class TimeTrackingService {

    constructor(
        private readonly userService: UsersService,
        private readonly taskService: TasksService,
        private readonly usersTasksService: UsersTasksService
    ) {
    }

    private static getUserIdAndTaskIdFromUserTaskId(userTaskId: string): UserIdAndTaskId {
        const [userId, taskId] = userTaskId.split('-');
        return {userId, taskId};
    }

    async create(createUserTaskDto: CreateTimeTrackingDto): Promise<ResponseTimeTrackingDto> {
        const user = await this.userService.findOneByUserNameOrEmail(createUserTaskDto.email);

        if (!user) {
            throw new BadRequestException(`No user found with email: ${createUserTaskDto.email}`)
        }

        const task = await this.taskService.create({
            customerName: createUserTaskDto.customerName,
            customerDescription: createUserTaskDto.customerDescription,
            name: createUserTaskDto.taskName,
            scheduledBreakTime: createUserTaskDto.scheduledBreakTime,
            scheduledEnd: createUserTaskDto.scheduledEnd,
            scheduledStart: createUserTaskDto.scheduledStart,
            taskDescription: createUserTaskDto.taskDescription,
        });

        const userTask = await this.usersTasksService.create({
            user,
            userId: user.id,
            task,
            taskId: task.id,
            actualStart: createUserTaskDto.actualStart,
            actualEnd: createUserTaskDto.actualEnd,
            actualBreakTime: createUserTaskDto.actualBreakTime,
            workProtocol: createUserTaskDto.workProtocol,
        });

        return constructTimeTrackingResponseDto(userTask);
    }

    async update(updateTimeTrackingDto: UpdateTimeTrackingDto): Promise<ResponseTimeTrackingDto> {

        const {userId, taskId} = TimeTrackingService.getUserIdAndTaskIdFromUserTaskId(updateTimeTrackingDto.id);

        const task = Object.assign(new Task(),
            {id: parseInt(taskId)},
            updateTimeTrackingDto.customerDescription === null ? null : {customerDescription: updateTimeTrackingDto.customerDescription},
            updateTimeTrackingDto.customerName === null ? null : {customerName: updateTimeTrackingDto.customerName},
            updateTimeTrackingDto.taskName === null ? null : {name: updateTimeTrackingDto.taskName},
            updateTimeTrackingDto.scheduledBreakTime === null ? null : {scheduledBreakTime: updateTimeTrackingDto.scheduledBreakTime},
            updateTimeTrackingDto.scheduledEnd === null ? null : {scheduledEnd: updateTimeTrackingDto.scheduledEnd},
            updateTimeTrackingDto.scheduledStart === null ? null : {scheduledStart: updateTimeTrackingDto.scheduledStart},
            updateTimeTrackingDto.taskDescription === null ? null : {taskDescription: updateTimeTrackingDto.taskDescription},
        );

        const user = await this.userService.findOne(parseInt(userId));

        if (!user) {
            throw new BadRequestException(`A user with id ${userId} does not exist!`)
        }

        const updateUserTaskDto: UpdateUserTaskDto = Object.assign({},
            {
                user,
                userId: user.id
            },
            {
                task,
                taskId: task.id
            },
            updateTimeTrackingDto.actualBreakTime === null ? null : {actualBreakTime: updateTimeTrackingDto.actualBreakTime},
            updateTimeTrackingDto.actualEnd === null ? null : {actualEnd: updateTimeTrackingDto.actualEnd},
            updateTimeTrackingDto.actualStart === null ? null : {actualStart: updateTimeTrackingDto.actualStart},
            updateTimeTrackingDto.workProtocol === null ? null : {workProtocol: updateTimeTrackingDto.workProtocol}
        );

        // currently does not update deep
        const userTask = await this.usersTasksService.update(updateUserTaskDto);
        // workaround because onUpdate CASCADE option does not work with the userTask entity
        userTask.task = await this.taskService.update(task);

        return constructTimeTrackingResponseDto(userTask);
    }

    async findAll(filter: Partial<FilterTimeTrackingDtoWithTimeRange>): Promise<ResponseTimeTrackingDto[]> {

        const {timeRangeStart, timeRangeEnd, ...others} = filter;

        const filterUserTaskDto: FilterUserTaskDto = others;

        if (timeRangeStart && timeRangeEnd) {
            filterUserTaskDto.timeRange = {
                start: timeRangeStart,
                end: timeRangeEnd
            }
        }

        const userTasks = await this.usersTasksService.findAll(filterUserTaskDto);
        return userTasks.map((userTask: UserTask) => {
            return constructTimeTrackingResponseDto(userTask)
        });
    }

    async delete(userTaskId: string): Promise<any> {
        const {userId, taskId} = TimeTrackingService.getUserIdAndTaskIdFromUserTaskId(userTaskId);
        return this.taskService.delete(parseInt(taskId));
    }

    async getUserBelongingToUserTask(userTaskId: string): Promise<User | null> {
        const {userId, taskId} = TimeTrackingService.getUserIdAndTaskIdFromUserTaskId(userTaskId);
        return this.usersTasksService.getUserBelongingToUserTask({userId: parseInt(userId), taskId: parseInt(taskId)});
    }
}
