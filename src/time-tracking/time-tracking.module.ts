import {Module} from '@nestjs/common';
import {TimeTrackingController} from './time-tracking.controller';
import {TimeTrackingService} from './time-tracking.service';
import {UsersModule} from "../users/users.module";
import {TasksModule} from "../tasks/tasks.module";
import {UsersTasksModule} from "../users-tasks/users-tasks.module";

@Module({
    imports: [
        UsersModule,
        TasksModule,
        UsersTasksModule
    ],
    providers: [
        {
            provide: 'SCOPES_DEPENDENCIES',
            useFactory: async (timeTrackingService: TimeTrackingService) => {
                return new Map([[TimeTrackingService, timeTrackingService]]);
            },
            inject: [TimeTrackingService]
        },
        TimeTrackingService
    ],
    controllers: [TimeTrackingController],
})
export class TimeTrackingModule {
}
