import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseArrayPipe,
    Patch,
    Post,
    Query,
    UseGuards,
    UsePipes,
    ValidationPipe
} from '@nestjs/common';
import {ScopeGuard} from "../auth/guards/scope.guard";
import {RestrictToScopes} from "../auth/decorators/scopes.decorator";
import {AdminScope} from "../auth/scopes/admin.scope";
import {AndConnectedScopesScope} from "../auth/scopes/and-connected-scopes.scope";
import {CurrentUserScope} from "../auth/scopes/current-user.scope";
import {TimeTrackingUserIdLocator} from "./time-tracking-user-id.locator";
import {RestrictedFieldsScope} from "../auth/scopes/restricted-fields.scope";
import {TimeTrackingService} from "./time-tracking.service";
import {ResponseTimeTrackingDto} from "../common/shared_interfaces/dto/time-tracking/response-time-tracking.dto";
import {CreateTimeTrackingDto} from "./dto/create-time-tracking.dto";
import {UpdateTimeTrackingDto} from "./dto/update-time-tracking.dto";
import {TimeTrackingIdParamValidation} from "./validation/time-tracking-id-param.validation";
import {FilterTimeTrackingDto} from "./dto/filter-time-tracking.dto";

@UsePipes(ValidationPipe)
@Controller('time-tracking')
export class TimeTrackingController {

    private static ALLOWED_KEYS: (keyof UpdateTimeTrackingDto)[] = [
        "id",
        "actualStart",
        "actualEnd",
        "actualBreakTime",
        "workProtocol",
    ];

    constructor(
        private readonly timeTrackingService: TimeTrackingService
    ) {
    }

    @UseGuards(ScopeGuard)
    @RestrictToScopes(new AdminScope())
    @Post()
    async create(
        @Body(new ParseArrayPipe({items: CreateTimeTrackingDto})) createUserTaskDTOs: CreateTimeTrackingDto[]
    ): Promise<ResponseTimeTrackingDto[]> {
        const createPromises = [];

        for (const createUserTaskDto of createUserTaskDTOs) {
            createPromises.push(this.timeTrackingService.create(createUserTaskDto));
        }

        return Promise.all(createPromises);
    }

    @UseGuards(ScopeGuard)
    @RestrictToScopes(
        new AdminScope(),
        new AndConnectedScopesScope(
            [
                new CurrentUserScope(new TimeTrackingUserIdLocator()),
                new RestrictedFieldsScope(TimeTrackingController.ALLOWED_KEYS),
            ]
        ))
    @Patch()
    async update(
        @Body(new ParseArrayPipe({items: UpdateTimeTrackingDto}))
            updateUserTaskDTOs: UpdateTimeTrackingDto[]
    ): Promise<ResponseTimeTrackingDto[]> {
        const updatePromises = [];

        for (const updateUserTaskDto of updateUserTaskDTOs) {
            updatePromises.push(this.timeTrackingService.update(updateUserTaskDto));
        }

        return Promise.all(updatePromises);
    }

    @UseGuards(ScopeGuard)
    @RestrictToScopes(new AdminScope())
    @Delete(':id')
    async delete(@Param() params: TimeTrackingIdParamValidation): Promise<any> {
        return this.timeTrackingService.delete(params.id);
    }

    @UseGuards(ScopeGuard)
    @RestrictToScopes(new AdminScope(), new CurrentUserScope('user-id'))
    @Get()
    @UsePipes(new ValidationPipe({forbidUnknownValues: true, transform: true, whitelist: true}))
    async findAll(
        @Query() filter: FilterTimeTrackingDto
    ): Promise<ResponseTimeTrackingDto[]> {

        // workaround because the @Expose decorator in FilterTimeTrackingDto
        // always adds the property even if it does not exist, but its necessary to map
        // the hyphen separated url params into camel case properties
        const filterWithoutUndefinedFields = {};
        for (const [filterPropertyName, filterValue] of Object.entries(filter)) {
            if (filterValue !== undefined) {
                filterWithoutUndefinedFields[filterPropertyName] = filterValue;
            }
        }
        return this.timeTrackingService.findAll(filterWithoutUndefinedFields);
    }
}
