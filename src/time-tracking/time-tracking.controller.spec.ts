import {Test, TestingModule} from '@nestjs/testing';
import {TimeTrackingController} from './time-tracking.controller';
import {TimeTrackingService} from "./time-tracking.service";

describe('TimeTrackingController', () => {
  let controller: TimeTrackingController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: TimeTrackingService,
          useValue: {}
        }
      ],
      controllers: [TimeTrackingController],
    }).compile();

    controller = module.get<TimeTrackingController>(TimeTrackingController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
