import {Test, TestingModule} from '@nestjs/testing';
import {TimeTrackingService} from './time-tracking.service';
import {UsersService} from "../users/users.service";
import {TasksService} from "../tasks/tasks.service";
import {UsersTasksService} from "../users-tasks/users-tasks.service";
import {Task, User, UserTask} from "../entities";
import {constructCompositeKey, constructTimeTrackingResponseDto} from "./time-tracking-response.builder";
import {ResponseTimeTrackingDto} from "../common/shared_interfaces/dto/time-tracking/response-time-tracking.dto";
import {FilterUserTaskDto} from "../users-tasks/dto/filter-user-task.dto";

describe('TimeTrackingService', () => {
    let timeTrackingService: TimeTrackingService;
    let usersService: UsersService;
    let tasksService: TasksService;
    let usersTasksService: UsersTasksService;

    let user: User;
    let task: Task;
    let userTask: UserTask;

    const constructResponseDto = (userTaskParam?: UserTask, overwrites?: { [key: string]: any }) => {

        userTaskParam ??= userTask;

        const responseTimeTrackingDto: ResponseTimeTrackingDto = constructTimeTrackingResponseDto(userTaskParam);

        return {
            ...responseTimeTrackingDto,
            ...overwrites
        };
    };


        beforeEach(async () => {
            const module: TestingModule = await Test.createTestingModule({
                providers: [
                    {
                        provide: UsersService,
                        useValue: {
                            findOneByUserNameOrEmail: jest.fn(async () => user),
                            findOne: jest.fn(async () => user),
                        }
                    },
                    {
                        provide: TasksService,
                        useValue: {
                            create: jest.fn(async () => task),
                            update: jest.fn(async (updateData) => ({...task, ...updateData})),
                            delete: jest.fn(async () => {
                            }),
                        }
                    },
                    {
                        provide: UsersTasksService,
                        useValue: {
                            create: jest.fn(async () => userTask),
                            update: jest.fn(async (updateData) => ({...userTask, ...updateData})),
                            findAll: jest.fn(async () => [userTask, userTask]),
                            getUserBelongingToUserTask: jest.fn(async () => user)
                        }
                    },
                    TimeTrackingService
                ],
            }).compile();

            timeTrackingService = module.get<TimeTrackingService>(TimeTrackingService);

            usersService = module.get<UsersService>(UsersService);
            tasksService = module.get<TasksService>(TasksService);
            usersTasksService = module.get<UsersTasksService>(UsersTasksService);

            user = Object.assign(new User(), {
                email: "",
                firstname: "",
                id: 0,
                lastname: "",
                password: "",
                passwordResetTokens: [],
                roles: [],
                userTasks: [],
                username: "",
                workHoursPerWeek: 0
            });

            task = Object.assign(new Task, {
                customerDescription: "",
                id: 0,
                name: "",
                scheduledBreakTime: "",
                scheduledEnd: undefined,
                scheduledStart: undefined,
                taskDescription: ""
            });

            userTask = Object.assign(new UserTask(), {
                actualBreakTime: undefined,
                actualDate: undefined,
                actualEnd: undefined,
                actualStart: undefined,
                task,
                taskId: task.id,
                user,
                userId: user.id,
                workProtocol: undefined
            });
        });

        it('should be defined', () => {
            expect(timeTrackingService).toBeDefined();
        });

        describe('create', () => {
            it('should create a new task with user_task connection', async () => {

                const expected = constructResponseDto(userTask);

                const actual = await timeTrackingService.create({
                    customerName: "",
                    customerDescription: "",
                    email: "",
                    scheduledEnd: new Date(),
                    taskDescription: "",
                    taskName: ""
                });

                expect(actual).toStrictEqual(expected);
            });
        });

        describe('update', () => {
            it('should update the provided data', async () => {

                const workProtocol = 'some protocol';

                const expected = constructResponseDto(userTask, {workProtocol});

                const actual = await timeTrackingService.update(expected);

                expect(actual).toStrictEqual(expected);
            });
        });

    describe('findAll', () => {
        it('should convert the time tracking filter to a user task filter', async () => {
            const expected: FilterUserTaskDto = {
                timeRange: {
                    start: new Date(),
                    end: new Date(),
                },
            };

            jest.spyOn(usersTasksService, 'findAll').mockImplementation(async (filter) => {
                expect(filter).toStrictEqual(expected);
                return [];
            })

            await expect(timeTrackingService.findAll({
                timeRangeStart: expected.timeRange.start,
                timeRangeEnd: expected.timeRange.end
            })).resolves.not.toThrow();
        });

        it('should omit the time range filter when only one of the time range attributes was specified', async () => {
            const expected: FilterUserTaskDto = {
                userId: 5,
                customerName: 'whatever',
                taskName: 'not important'
            };

            jest.spyOn(usersTasksService, 'findAll').mockImplementation(async (filter) => {
                expect(filter).toStrictEqual(expected);
                return [];
            })

            await expect(timeTrackingService.findAll({timeRangeStart: new Date(), ...expected})).resolves.not.toThrow();
        });
    });

        describe('delete', () => {
            it('should not fail when calling delete', async () => {
                await expect(timeTrackingService.delete(constructResponseDto().id)).resolves.not.toThrow();
            });
        });


        describe('getUserBelongingToUserTask', () => {
            it('should return the user object matching the given composite key ', async () => {
                const expected = Object.assign(new User(), user);
                const actual = await timeTrackingService.getUserBelongingToUserTask(constructCompositeKey(user, task));
                expect(actual).toStrictEqual(expected);
            });
        });
    }
);
