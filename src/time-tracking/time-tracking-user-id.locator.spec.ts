import {Test, TestingModule} from '@nestjs/testing';
import {TimeTrackingUserIdLocator} from "./time-tracking-user-id.locator";
import {createMock} from "@golevelup/ts-jest";
import {ExecutionContext} from "@nestjs/common";
import {DeepMocked} from "@golevelup/ts-jest/lib/mocks";
import {Request} from "express";
import {User} from "../entities";
import {TimeTrackingService} from "./time-tracking.service";

describe('TimeTrackingUserIdLocator', () => {
    let timeTrackingUserIdLocator: TimeTrackingUserIdLocator;

    let timeTrackingService: TimeTrackingService;
    let executionContext: DeepMocked<ExecutionContext>;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                {
                    provide: TimeTrackingService,
                    useValue: {
                        getUserBelongingToUserTask: jest.fn(async (id: number) => Object.assign(new User(), {id})),
                    }
                }
            ],
        }).compile();


        executionContext = createMock<ExecutionContext>();
        executionContext.switchToHttp().getRequest.mockReturnValue({
            body: [
                {
                    id: 1
                },
                {
                    id: 2
                }
            ]
        });

        timeTrackingService = module.get(TimeTrackingService);
        timeTrackingUserIdLocator = new TimeTrackingUserIdLocator();

    });

    it('should be defined', () => {
        expect(timeTrackingUserIdLocator).toBeDefined();
    });

    describe('findIds', () => {
        it('should return all ids from the objects in the request body', async () => {
            const request = executionContext.switchToHttp().getRequest() as Request;
            const otherDependencies = new Map([[TimeTrackingService, timeTrackingService]]);
            await expect(timeTrackingUserIdLocator.findIds(request, otherDependencies)).resolves.toStrictEqual([1, 2]);
        });
    });

});
