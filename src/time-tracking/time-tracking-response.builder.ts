import {Task, User, UserTask} from "../entities";
import {ResponseTimeTrackingDto} from "../common/shared_interfaces/dto/time-tracking/response-time-tracking.dto";

export function constructTimeTrackingResponseDto(userTask: UserTask): ResponseTimeTrackingDto {
    return {
        actualBreakTime: userTask.actualBreakTime,
        actualEnd: userTask.actualEnd,
        actualStart: userTask.actualStart,
        customerDescription: userTask.task.customerDescription,
        customerName: userTask.task.customerName,
        email: userTask.user.email,
        id: constructCompositeKey(userTask.user, userTask.task),
        scheduledBreakTime: userTask.task.scheduledBreakTime,
        scheduledEnd: userTask.task.scheduledEnd,
        scheduledStart: userTask.task.scheduledStart,
        taskDescription: userTask.task.taskDescription,
        taskName: userTask.task.name,
        workProtocol: userTask.workProtocol
    };
}

export function constructCompositeKey(user: User, task: Task): string {
    return `${user.id}-${task.id}`;
}