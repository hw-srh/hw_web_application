import {Injectable, OnModuleDestroy} from '@nestjs/common';
import {ConfigService} from "@nestjs/config";
import {AppEnvironmentType} from "./config/app-environment.config";

@Injectable()
export class AppService implements OnModuleDestroy {
    constructor(private configService: ConfigService) {
    }

    onModuleDestroy(): any {
        const currentAppEnv = this.configService.get<AppEnvironmentType>('appEnv');
        if (['test', 'testing'].includes(currentAppEnv)) {
            return;
        }
        console.log('SIGTERM received application shutting down ...');
    }
}
