export interface CreateTaskDto {
    customerDescription: string;
    customerName: string;
    name: string,
    scheduledBreakTime: string,
    scheduledStart: Date,
    scheduledEnd: Date,
    taskDescription: string,
}