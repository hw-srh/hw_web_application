import {BadRequestException, Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Task} from "../entities";
import {Repository} from "typeorm";
import {CreateTaskDto} from "./dto/create-task.dto";
import {UpdateTaskDto} from "./dto/update-task.dto";

@Injectable()
export class TasksService {
    constructor(
        @InjectRepository(Task)
        private readonly taskRepository: Repository<Task>,
    ) {
    }

    async findOne(id: number): Promise<Task | undefined> {
        return this.taskRepository.findOne({
            where: {id} // using findOne(id) returns a value even if the id is undefined
            // see this github issue: https://github.com/typeorm/typeorm/issues/2500
        });
    }

    async delete(id: number): Promise<any> {
        return this.taskRepository.remove(Object.assign(new Task(), {id}));
    }

    async create(createTaskDto: CreateTaskDto): Promise<Task> {
        return this.taskRepository.save(Object.assign(new Task(), {
            ...createTaskDto
        }));
    }

    async update(updateTaskDto: UpdateTaskDto): Promise<Task> {
        const existingTask = await this.findOne(updateTaskDto.id);

        if (!existingTask) {
            throw new BadRequestException('task update requires a valid task id!');
        }

        return this.taskRepository.save({
            ...existingTask,
            ...updateTaskDto
        });
    }
}
