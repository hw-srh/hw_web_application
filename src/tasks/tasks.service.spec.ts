import {Test, TestingModule} from '@nestjs/testing';
import {TasksService} from './tasks.service';
import {getRepositoryToken} from "@nestjs/typeorm";
import {Task} from "../entities";
import {Repository} from "typeorm";
import {CreateTaskDto} from "./dto/create-task.dto";
import {BadRequestException} from "@nestjs/common";

describe('TasksService', () => {
    let tasksService: TasksService;

    let taskRepositoryMock: Repository<Task>;

    let task: Task;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                TasksService,
                {
                    provide: getRepositoryToken(Task),
                    useValue: {
                        save: jest.fn(async (task) => task as Task),
                        remove: jest.fn(async () => {
                        }),
                        findOne: jest.fn(async () => task),
                    }
                },
            ],
        }).compile();

        task = Object.assign(new Task, {
            customerDescription: "",
            id: 0,
            name: "",
            scheduledBreakTime: "",
            scheduledEnd: undefined,
            scheduledStart: undefined,
            taskDescription: ""
        });

        taskRepositoryMock = module.get(getRepositoryToken(Task));

        tasksService = module.get<TasksService>(TasksService);
    });

    it('should be defined', () => {
        expect(tasksService).toBeDefined();
    });

    describe('findOne', () => {
        it('should return the task entity belonging to the given primary key', async () => {
            const actual = await tasksService.findOne(task.id);
            expect(actual).toStrictEqual(task);
        });
    });

    describe('create', () => {
        it('should create a new task', async () => {
            const actual = await tasksService.create(task as CreateTaskDto)
            expect(actual).toStrictEqual(task);
        });
    });

    describe('update', () => {
        it('should update a task', async () => {
            const expected = {...task, name: 'another name'};
            const actual = await tasksService.update({id: task.id, name: expected.name})
            expect(actual).toStrictEqual(expected);
        });

        it(`should throw an ${BadRequestException.name} when no task with the given id can be found`, async () => {
            jest.spyOn(taskRepositoryMock, 'findOne').mockImplementation(async () => null);
            await expect(tasksService.update(task)).rejects.toThrow(BadRequestException);
        });
    });

    describe('delete', () => {
        it('should remove the task without throwing an exception', async () => {
            await expect(tasksService.delete(task.id)).resolves.not.toThrow();
        });
    });
});
