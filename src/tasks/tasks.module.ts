import {Module} from '@nestjs/common';
import {TasksService} from "./tasks.service";
import {TypeOrmModule} from "@nestjs/typeorm";
import {Task, UserTask} from "../entities";

@Module({
    imports: [
        TypeOrmModule.forFeature([Task, UserTask]),
    ],
    providers: [TasksService],
    exports: [TasksService]
})
export class TasksModule {
}
