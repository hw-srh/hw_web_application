export interface CreatePasswordResetTokenDto {
    userId: number;
    token: string;
}