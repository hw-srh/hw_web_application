import {Module} from '@nestjs/common';
import {PasswordResetTokensService} from './password-reset-tokens.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {PasswordResetToken, User} from "../entities";

@Module({
  imports: [
    TypeOrmModule.forFeature([PasswordResetToken, User]),
  ],
  providers: [PasswordResetTokensService],
  exports: [PasswordResetTokensService]
})
export class PasswordResetTokensModule {
}
