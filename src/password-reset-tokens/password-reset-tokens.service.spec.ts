import {Test, TestingModule} from '@nestjs/testing';
import {PasswordResetTokensService} from './password-reset-tokens.service';
import {getRepositoryToken} from "@nestjs/typeorm";
import {PasswordResetToken, User} from "../entities";
import {Repository} from "typeorm";

describe('PasswordResetTokensService', () => {
    let service: PasswordResetTokensService;
    let passwordResetTokenRepositoryMock: Repository<PasswordResetToken>;


    const token: string = 'test';
    const tokenHash: string = '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08';

    const user: User = {
        id: 42,
        email: "test",
        password: "test",
        firstname: "test",
        lastname: "test",
        username: "test",
        roles: [{id: "admin", users: null}]
    } as User;

    beforeEach(async () => {
        const moduleRef: TestingModule = await Test.createTestingModule({
            providers: [
                {
                    provide: getRepositoryToken(PasswordResetToken),
                    useValue: {
                        save: jest.fn(),
                        delete: jest.fn(async () => {
                        }),
                        findOne: jest.fn(),
                    }
                },
                PasswordResetTokensService
            ],
        }).compile();

        service = moduleRef.get<PasswordResetTokensService>(PasswordResetTokensService);
        passwordResetTokenRepositoryMock = moduleRef.get(getRepositoryToken(PasswordResetToken));

    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    describe('findUserByTokenAndEmail', () => {
        it('it should return the user belonging to the given token', async () => {

            jest.spyOn(passwordResetTokenRepositoryMock, 'findOne').mockImplementation(async (conditions: any) => {
                expect(conditions?.where?.tokenHash).toEqual(tokenHash);
                return {
                    user
                } as PasswordResetToken;
            });

            const actual = await service.findPasswordResetToken(token);

            expect(actual).toStrictEqual({user} as PasswordResetToken);
        });
    });

    describe('deleteAllTokensForUser', () => {
        it('should not throw when deleting all tokens for a user', async () => {
            await expect(service.deleteAllTokensForUser(user)).resolves.not.toThrow();
        });
    });

    describe('createNewToken', () => {
        it('should create a new token with the provided data', async () => {

            const expected = Object.assign(new PasswordResetToken(), {
                userId: user.id,
                tokenHash
            });

            jest.spyOn(passwordResetTokenRepositoryMock, 'save').mockImplementation(async () => {
                return expected;
            });

            const actual = await service.save({
                userId: user.id,
                token
            });

            expect(actual).toStrictEqual(expected);
        });
    });
});
