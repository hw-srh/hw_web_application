import {Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {PasswordResetToken, User} from "../entities";
import {Repository} from "typeorm";
import {CreatePasswordResetTokenDto} from "./dto/create-password-reset-token.dto";
import {createHash} from "crypto";
import {DeleteResult} from "typeorm/query-builder/result/DeleteResult";

@Injectable()
export class PasswordResetTokensService {

    constructor(
        @InjectRepository(PasswordResetToken)
        private readonly passwordResetTokenRepository: Repository<PasswordResetToken>
    ) {
    }

    private static constructHexEncodedHashFromToken(token: string): string {
        return createHash('sha256').update(token).digest('hex');
    }

    async findPasswordResetToken(passwordResetToken: string): Promise<PasswordResetToken | null> {
        return await this.passwordResetTokenRepository.findOne({
            where: {
                tokenHash: PasswordResetTokensService.constructHexEncodedHashFromToken(passwordResetToken)
            },
            join: {
                alias: "password_reset_token",
                leftJoinAndSelect: {
                    user: "password_reset_token.user"
                }
            },
        });
    }

    async deleteAllTokensForUser(user: User): Promise<DeleteResult> {
        return this.passwordResetTokenRepository.delete({
            userId: user.id,
        });
    }

    async save(createPasswordResetTokenDto: CreatePasswordResetTokenDto): Promise<PasswordResetToken> {
        const passwordResetToken = Object.assign(new PasswordResetToken(), {
            userId: createPasswordResetTokenDto.userId,
            tokenHash: PasswordResetTokensService.constructHexEncodedHashFromToken(createPasswordResetTokenDto.token)
        });
        return this.passwordResetTokenRepository.save(passwordResetToken);
    }
}
