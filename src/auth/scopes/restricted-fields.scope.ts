import {Scope} from "../interfaces/scope.interface";
import {ExecutionContext} from "@nestjs/common";
import {Request} from "express";

export class RestrictedFieldsScope implements Scope {
    alias: string = 'restricted-fields';

    constructor(private readonly allowedKeys: string[]) {
    }

    async hasAccessToScope(context: ExecutionContext): Promise<boolean> {
        const request: Request = context.switchToHttp().getRequest();

        let body: any = request.body;

        if (!Array.isArray(body)) {
            body = [body];
        }

        for (const obj of body) {
            for (const key of Object.keys(obj)) {
                if (!this.allowedKeys.includes(key)) {
                    return false;
                }
            }
        }

        return true;
    }

}