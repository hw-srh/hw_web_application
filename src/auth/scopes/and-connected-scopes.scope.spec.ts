import {DeepMocked} from "@golevelup/ts-jest/lib/mocks";
import {ExecutionContext} from "@nestjs/common";
import {createMock} from "@golevelup/ts-jest";
import {AndConnectedScopesScope} from "./and-connected-scopes.scope";
import {CurrentUserScope} from "./current-user.scope";
import {RestrictedFieldsScope} from "./restricted-fields.scope";
import {User} from "../../entities";


describe('AndConnectedScopesScope', () => {

    const restrictedFields = [
        'test',
        'another_test',
        'jet_another_test'
    ];

    let executionContext: DeepMocked<ExecutionContext>;

    beforeEach(async () => {
        executionContext = createMock<ExecutionContext>();
    });

    describe('hasAccessToScope', () => {

        const validObject = {
            test: null,
            another_test: null,
            jet_another_test: null
        };

        it('should resolve to true if all scopes resolve to true', async () => {

            const andConnectedScopesScope = new AndConnectedScopesScope([
                new CurrentUserScope(), new RestrictedFieldsScope(restrictedFields)
            ]);

            executionContext.switchToHttp().getRequest.mockReturnValue({
                body: validObject,
                params: {
                    id: 1
                }
            });
            await expect(andConnectedScopesScope.hasAccessToScope(
                executionContext, Object.assign(new User(), {id: 1}), null
            )).resolves.toBeTruthy();
        });

        it('should resolve to false if one of the scopes resolve to false', async () => {

            const andConnectedScopesScope = new AndConnectedScopesScope([
                new CurrentUserScope(), new RestrictedFieldsScope(restrictedFields)
            ]);

            executionContext.switchToHttp().getRequest.mockReturnValue({
                body: {validObject, unexpectedField: null},
                params: {
                    id: 1
                }
            });
            await expect(andConnectedScopesScope.hasAccessToScope(
                executionContext, Object.assign(new User(), {id: 1}), null
            )).resolves.toBeFalsy();
        });
    });
});