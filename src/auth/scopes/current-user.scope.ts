import {Scope} from "../interfaces/scope.interface";
import {ExecutionContext} from "@nestjs/common";
import {User} from "../../entities";
import {Request} from "express";
import {CurrentUserIdLocator} from "../interfaces/current-user-id-locator.interface";

export class CurrentUserScope implements Scope {
    alias = 'current-user';

    constructor(private readonly currentUserIdLocator: CurrentUserIdLocator | string = 'id') {
    }

    async hasAccessToScope(context: ExecutionContext, user: User, otherDependencies?: any): Promise<boolean> {
        return this.requestedUserMatchesCurrentUser(context, user, otherDependencies);
    }

    private async requestedUserMatchesCurrentUser(context: ExecutionContext, user: User, otherDependencies?: any): Promise<boolean> {
        const request: Request = context.switchToHttp().getRequest();

        if (typeof this.currentUserIdLocator === 'string') {
            return this.validateAgainstIdInURIParams(request, user) || this.validateAgainstIdInUrlPrams(request, user);
        }

        return this.validateAgainstIdsFromIdLocator(request, user, otherDependencies);
    }

    private validateAgainstIdInURIParams(request: Request, user: User): boolean {
        const id = parseInt(request.params[this.currentUserIdLocator as string]);
        return id === user.id;
    }

    private validateAgainstIdInUrlPrams(request: Request, user: User): boolean {
        const id = parseInt(request.query[this.currentUserIdLocator as string] as string);
        return id === user.id;
    }

    private async validateAgainstIdsFromIdLocator(request: Request, user: User, otherDependencies: any): Promise<boolean> {
        const ids = await (this.currentUserIdLocator as CurrentUserIdLocator).findIds(request, otherDependencies);

        if (ids.length === 0) {
            return false;
        }

        for (const id of ids) {
            if (id !== user.id) {
                return false;
            }
        }

        return true;
    }
}