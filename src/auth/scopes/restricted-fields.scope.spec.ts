import {DeepMocked} from "@golevelup/ts-jest/lib/mocks";
import {ExecutionContext} from "@nestjs/common";
import {createMock} from "@golevelup/ts-jest";
import {RestrictedFieldsScope} from "./restricted-fields.scope";

describe('RestrictedFieldsScope', () => {

    const restrictedFields = [
        'test',
        'another_test',
        'jet_another_test'
    ];

    let restrictedFieldsScope: RestrictedFieldsScope;
    let executionContext: DeepMocked<ExecutionContext>;

    beforeEach(async () => {
        restrictedFieldsScope = new RestrictedFieldsScope(restrictedFields);
        executionContext = createMock<ExecutionContext>();
    });

    it('should be defined', () => {
        expect(RestrictedFieldsScope).toBeDefined();
    });

    describe('hasAccessToScope', () => {

        const validObject = {
            test: null,
            another_test: null,
            jet_another_test: null
        };

        it('should resolve to true if the object only contains properties on the allowed list', async () => {
            executionContext.switchToHttp().getRequest.mockReturnValue({
                body: validObject
            });
            await expect(restrictedFieldsScope.hasAccessToScope(executionContext)).resolves.toBeTruthy();
        });

        it('should resolve to true if all objects only contain properties on the allowed list', async () => {
            executionContext.switchToHttp().getRequest.mockReturnValue({
                body: [validObject, validObject, validObject]
            });
            await expect(restrictedFieldsScope.hasAccessToScope(executionContext)).resolves.toBeTruthy();
        });

        it('should resolve to true if the object is empty', async () => {
            executionContext.switchToHttp().getRequest.mockReturnValue({
                body: {}
            });
            await expect(restrictedFieldsScope.hasAccessToScope(executionContext)).resolves.toBeTruthy();
        });

        it('should resolve to false if one of the objects contains a unexpected property name', async () => {
            executionContext.switchToHttp().getRequest.mockReturnValue({
                body: [validObject, {...validObject, unexpected: null}]
            });
            await expect(restrictedFieldsScope.hasAccessToScope(executionContext)).resolves.toBeFalsy();
        });
    });
});