import {User} from "../../entities";
import {CurrentUserScope} from "./current-user.scope";
import {DeepMocked} from "@golevelup/ts-jest/lib/mocks";
import {ExecutionContext} from "@nestjs/common";
import {createMock} from "@golevelup/ts-jest";

describe('CurrentUserScope', () => {

    let currentUserScope: CurrentUserScope;
    let executionContext: DeepMocked<ExecutionContext>;

    beforeEach(async () => {
        currentUserScope = new CurrentUserScope();
        executionContext = createMock<ExecutionContext>();
    });

    it('should be defined', () => {
        expect(CurrentUserScope).toBeDefined();
    });

    describe('hasAccessToScope', () => {
        it('should return true if the id param in the request matches the current users id', async () => {
            const userWithAdminGroup = Object.assign(new User(), {id: 10});
            executionContext.switchToHttp().getRequest.mockReturnValue({
                params: {
                    id: 10
                },
                query: {}
            });
            await expect(currentUserScope.hasAccessToScope(executionContext, userWithAdminGroup)).resolves.toBeTruthy();
        });

        it('should return false if the id param in the request does not match the current users id', async () => {
            const userWithAdminGroup = Object.assign(new User(), {id: 10});
            executionContext.switchToHttp().getRequest.mockReturnValue({
                params: {
                    id: 11
                },
                query: {}
            });
            await expect(currentUserScope.hasAccessToScope(executionContext, userWithAdminGroup)).resolves.toBeFalsy();
        });

        it('should return true if the id url param matches the current users id', async () => {
            const userWithAdminGroup = Object.assign(new User(), {id: 10});
            executionContext.switchToHttp().getRequest.mockReturnValue({
                params: {},
                query: {
                    id: 10
                }
            });
            await expect(currentUserScope.hasAccessToScope(executionContext, userWithAdminGroup)).resolves.toBeTruthy();
        });

        it('should return false if the id url param does not match the current users id', async () => {
            const userWithAdminGroup = Object.assign(new User(), {id: 10});
            executionContext.switchToHttp().getRequest.mockReturnValue({
                params: {},
                query: {
                    id: 11
                }
            });
            await expect(currentUserScope.hasAccessToScope(executionContext, userWithAdminGroup)).resolves.toBeFalsy();
        });

        it('should return false if the id param is missing in the uri and query string', async () => {
            const userWithAdminGroup = Object.assign(new User(), {id: 10});
            executionContext.switchToHttp().getRequest.mockReturnValue({
                params: {},
                query: {}
            });
            await expect(currentUserScope.hasAccessToScope(executionContext, userWithAdminGroup)).resolves.toBeFalsy();
        });
    });
});