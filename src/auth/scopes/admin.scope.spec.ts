import {User} from "../../entities";
import {AdminScope} from "./admin.scope";
import {createMock} from "@golevelup/ts-jest";
import {ExecutionContext} from "@nestjs/common";
import {DeepMocked} from "@golevelup/ts-jest/lib/mocks";

describe('AdminScope', () => {

    let adminScope: AdminScope;
    let executionContext: DeepMocked<ExecutionContext>;

    beforeEach(async () => {
        adminScope = new AdminScope();
        executionContext = createMock<ExecutionContext>();
    });

    it('should be defined', () => {
        expect(adminScope).toBeDefined();
    });

    describe('hasAccessToScope', () => {
        it('should return true if the user has the admin group', async () => {
            const userWithAdminGroup = Object.assign(new User(), {roles: [{id: 'admin'}]});
            await expect(adminScope.hasAccessToScope(executionContext, userWithAdminGroup)).resolves.toBeTruthy();
        });

        it('should return false if the user is not part of the admin group', async () => {
            const userWithAdminGroup = Object.assign(new User(), {roles: []});
            await expect(adminScope.hasAccessToScope(executionContext, userWithAdminGroup)).resolves.toBeFalsy();
        });

        it('should return false if the roles attribute is undefined', async () => {
            await expect(adminScope.hasAccessToScope(executionContext, new User())).resolves.toBeFalsy();
        });
    });
});