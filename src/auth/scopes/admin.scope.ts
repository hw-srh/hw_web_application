import {Scope} from "../interfaces/scope.interface";
import {User} from "../../entities";
import {ExecutionContext} from "@nestjs/common";

export class AdminScope implements Scope {
    alias = 'admin';

    async hasAccessToScope(context: ExecutionContext, user: User): Promise<boolean> {
        for (const role of user?.roles ?? []) {
            if (role.id === 'admin') {
                return true;
            }
        }
        return false;
    }
}