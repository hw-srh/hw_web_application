import {Scope} from "../interfaces/scope.interface";
import {User} from "../../entities";
import {ExecutionContext} from "@nestjs/common";

export class AndConnectedScopesScope implements Scope {
    alias: 'and-connected-scopes';

    constructor(private readonly scopes: Scope[]) {
    }

    async hasAccessToScope(context: ExecutionContext, user: User, otherDependencies: any): Promise<boolean> {
        for (const scope of this.scopes) {
            if (!await scope.hasAccessToScope(context, user, otherDependencies)) {
                return false;
            }
        }

        return true;
    }

}