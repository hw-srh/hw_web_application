import {Test} from '@nestjs/testing';
import {AuthService} from "../auth.service";
import {User} from "../../entities";
import {JwtRefreshGuard} from "./jwt-refresh.guard";
import {PassportModule} from "@nestjs/passport";
import {JwtModule, JwtService} from "@nestjs/jwt";
import {ConfigService} from "@nestjs/config";
import {DeepMocked} from "@golevelup/ts-jest/lib/mocks";
import {ExecutionContext, UnauthorizedException} from "@nestjs/common";
import {createMock} from "@golevelup/ts-jest";
import {JwtInvalidRefreshException} from "../exceptions/jwt-invalid-refresh.exception";
import {JwtRefreshStrategy} from "../strategies/jwt-refresh.strategy";


describe('JwtRefreshGuard', () => {

    let jwtRefreshGuard: JwtRefreshGuard;
    let executionContext: DeepMocked<ExecutionContext>;
    let authServiceMock: AuthService;
    let jwtService: JwtService;
    let jwt: string;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: AuthService,
                    useValue: {
                        hasJwtExpired: jest.fn(() => true),
                        removeJti: jest.fn(() => {
                        }),
                        validateJwt: jest.fn(() => true),
                        constructUserFromJwtPayload: jest.fn(() => new User()),
                    }
                },
                {
                    provide: ConfigService,
                    useValue: {
                        get: jest.fn(() => ({secret: "some_random_key"})),
                    }
                },
                JwtRefreshStrategy,
                JwtRefreshGuard,
            ],
            imports: [
                PassportModule.register({defaultStrategy: 'jwt-refresh'}),
                JwtModule.register({secret: "some_random_key"}),
            ]
        }).compile();


        jwtService = moduleRef.get<JwtService>(JwtService);
        jwt = await jwtService.signAsync({
            sub: 10,
            exp: Math.floor((Date.now() + 30 * 60000) / 1000),
            jti: "some_uuid",
            context: {
                user: {},
                roles: []
            }
        });

        executionContext = createMock<ExecutionContext>();
        executionContext.switchToHttp().getRequest.mockReturnValue({
            headers: {
                authorization: `Bearer ${jwt}`
            }
        });

        jwtRefreshGuard = moduleRef.get<JwtRefreshGuard>(JwtRefreshGuard);
        authServiceMock = moduleRef.get<AuthService>(AuthService);
    });

    describe('canActivate', () => {
        it('should return true for a request with a valid jwt', async (done) => {
            expect(await jwtRefreshGuard.canActivate(executionContext)).toBeTruthy();
            done();
        });


        it('should throw an Unauthorized exception if the jwt token is invalid', async (done) => {

            executionContext.switchToHttp().getRequest.mockReturnValue({
                headers: {
                    authorization: `Bearer ${jwt}INVALID`
                }
            });

            try {
                await jwtRefreshGuard.canActivate(executionContext);
                fail(`should have thrown ${UnauthorizedException.name}`);
            } catch (e) {
                expect(e).toBeInstanceOf(UnauthorizedException);
            }
            done();
        });

        it('should throw invalid refresh exception if the token has not expired yet', async (done) => {

            jest.spyOn(authServiceMock, 'hasJwtExpired').mockImplementation(() => false);

            try {
                await jwtRefreshGuard.canActivate(executionContext);
                fail(`should have thrown ${JwtInvalidRefreshException.name}`);
            } catch (e) {
                expect(e).toBeInstanceOf(JwtInvalidRefreshException);
            }
            done();
        });

        it('should throw invalid refresh exception if the token has not expired yet', async (done) => {


            const oneMonthOldJwt = await jwtService.signAsync({
                sub: 10,
                // 32 days old so definitely older than one month
                exp: Math.floor((Date.now() - 60000 * 60 * 24 * 32) / 1000),
                jti: "some_uuid",
                context: {
                    user: {},
                    roles: []
                }
            });

            executionContext.switchToHttp().getRequest.mockReturnValue({
                headers: {
                    authorization: `Bearer ${oneMonthOldJwt}`
                }
            });

            try {
                await jwtRefreshGuard.canActivate(executionContext);
                fail(`should have thrown ${UnauthorizedException.name}`);
            } catch (e) {
                expect(e).toBeInstanceOf(UnauthorizedException);
            }
            done();
        });
    });
});