import {Test} from '@nestjs/testing';
import {PublicAuthGuard} from "./public.guard";
import {ExecutionContext} from "@nestjs/common";
import {Reflector} from "@nestjs/core";
import {DeepMocked} from "@golevelup/ts-jest/lib/mocks";
import {createMock} from "@golevelup/ts-jest";


describe('PublicAuthGuard', () => {

    let publicAuthGuard: PublicAuthGuard;
    let reflector: Reflector;
    let executionContext: DeepMocked<ExecutionContext>;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: Reflector,
                    useValue: {
                        get: jest.fn(() => []),
                    }
                },
                PublicAuthGuard
            ],
        }).compile();

        reflector = moduleRef.get<Reflector>(Reflector);
        publicAuthGuard = moduleRef.get<PublicAuthGuard>(PublicAuthGuard);
        executionContext = createMock<ExecutionContext>();
    });

    it('should be defined', () => {
        expect(publicAuthGuard).toBeDefined();
    });

    describe('canActivate', () => {
        it('should return true when the current controller class or method has another guard(s)', async (done) => {

            jest.spyOn(reflector, 'get').mockReturnValue(['another_guard']);
            expect(await publicAuthGuard.canActivate(executionContext)).toBeTruthy();
            done();
        });

        it('should return false when the current controller class or method has no guard(s) and is also not public', async (done) => {

            jest.spyOn(reflector, 'get').mockImplementation((metadataKey: string) => {
                if (metadataKey === 'isPublic') {
                    return undefined;
                }
                return [];
            });
            expect(await publicAuthGuard.canActivate(executionContext)).toBeFalsy();
            done();
        });

        it('should return true when the current controller class or method has no guard(s) but is public', async (done) => {

            jest.spyOn(reflector, 'get').mockImplementation((metadataKey: string) => {
                if (metadataKey === 'isPublic') {
                    return true;
                }
                return [];
            });
            expect(await publicAuthGuard.canActivate(executionContext)).toBeTruthy();
            done();
        });
    });
});