import {Test} from '@nestjs/testing';
import {AuthService} from "../auth.service";
import {LogoutGuard} from "./logout.guard";
import {PassportModule} from "@nestjs/passport";
import {JwtModule, JwtService} from "@nestjs/jwt";
import {ConfigService} from "@nestjs/config";
import {DeepMocked} from "@golevelup/ts-jest/lib/mocks";
import {ExecutionContext, UnauthorizedException} from "@nestjs/common";
import {createMock} from "@golevelup/ts-jest";
import {JwtInvalidLogoutRequestException} from "../exceptions/jwt-invalid-logout-request.exception";
import {JwtLogoutStrategy} from "../strategies/jwt-logout.strategy";


describe('LogoutGuard', () => {

    let logoutGuard: LogoutGuard;
    let executionContext: DeepMocked<ExecutionContext>;
    let authServiceMock: AuthService;
    let jwt: string;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: AuthService,
                    useValue: {
                        hasJwtExpired: jest.fn(() => false),
                        removeJti: jest.fn(() => {
                        }),
                    }
                },
                {
                    provide: ConfigService,
                    useValue: {
                        get: jest.fn(() => ({secret: "some_random_key"})),
                    }
                },
                JwtLogoutStrategy,
                LogoutGuard,
            ],
            imports: [
                PassportModule.register({defaultStrategy: 'jwt-logout'}),
                JwtModule.register({secret: "some_random_key"}),
            ]
        }).compile();


        const jwtService = moduleRef.get<JwtService>(JwtService);
        jwt = await jwtService.signAsync({
            sub: 10,
            exp: Math.floor((Date.now() + 30 * 60000) / 1000),
            jti: "some_uuid",
            context: {
                user: {},
                roles: []
            }
        });

        executionContext = createMock<ExecutionContext>();
        executionContext.switchToHttp().getRequest.mockReturnValue({
            headers: {
                authorization: `Bearer ${jwt}`
            }
        });

        logoutGuard = moduleRef.get<LogoutGuard>(LogoutGuard);
        authServiceMock = moduleRef.get<AuthService>(AuthService);
    });

    describe('canActivate', () => {
        it('should return true for a request with a valid jwt', async (done) => {
            expect(await logoutGuard.canActivate(executionContext)).toBeTruthy();
            done();
        });


        it('should throw an Unauthorized exception if the jwt token is invalid', async (done) => {

            executionContext.switchToHttp().getRequest.mockReturnValue({
                headers: {
                    authorization: `Bearer ${jwt}INVALID`
                }
            });

            try {
                await logoutGuard.canActivate(executionContext);
                fail(`should have thrown ${UnauthorizedException.name}`);
            } catch (e) {
                expect(e).toBeInstanceOf(UnauthorizedException);
            }
            done();
        });

        it('should throw an invalid logout request exception when the jwt token has expired', async (done) => {

            jest.spyOn(authServiceMock, 'hasJwtExpired').mockImplementation(() => true);

            try {
                await logoutGuard.canActivate(executionContext);
                fail(`should have thrown ${JwtInvalidLogoutRequestException.name}`);
            } catch (e) {
                expect(e).toBeInstanceOf(JwtInvalidLogoutRequestException);
            }
            done();
        });
    });

});