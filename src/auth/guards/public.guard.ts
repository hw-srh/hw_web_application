import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';
import {Reflector} from "@nestjs/core";
import {GUARDS_METADATA} from "@nestjs/common/constants";

@Injectable()
export class PublicAuthGuard implements CanActivate {

    constructor(private reflector: Reflector) {
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {

        if (this.hasCurrentControllerAnotherGuard(context) || this.hasCurrentControllerMethodAnotherGuard(context)) {
            return true;
        }

        return this.isCurrentControllerPublic(context) || this.isCurrentControllerMethodPublic(context);
    }

    private hasCurrentControllerAnotherGuard(context: ExecutionContext): boolean {
        return (this.reflector.get<string[]>(GUARDS_METADATA, context.getClass()) ?? []).length > 0;
    }

    private hasCurrentControllerMethodAnotherGuard(context: ExecutionContext): boolean {
        return (this.reflector.get<string[]>(GUARDS_METADATA, context.getHandler()) ?? []).length > 0;
    }

    private isCurrentControllerPublic(context: ExecutionContext): boolean {
        return this.reflector.get<boolean>('isPublic', context.getClass()) ?? false;
    }

    private isCurrentControllerMethodPublic(context: ExecutionContext): boolean {
        return this.reflector.get<boolean>('isPublic', context.getHandler()) ?? false;
    }
}