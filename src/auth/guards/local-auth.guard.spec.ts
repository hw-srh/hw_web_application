import {Test} from '@nestjs/testing';
import {AuthService} from "../auth.service";
import {User} from "../../entities";
import {LoginDto} from "../../common/shared_interfaces/dto/login/login.dto";
import {LocalStrategy} from "../strategies/local.strategy";
import {LocalAuthGuard} from "./local-auth.guard";
import {ExecutionContext, UnauthorizedException} from "@nestjs/common";
import {createMock} from "@golevelup/ts-jest";
import {PassportModule} from "@nestjs/passport";
import {DeepMocked} from "@golevelup/ts-jest/lib/mocks";


describe('LocalAuthGuard', () => {

    let localAuthGuard: LocalAuthGuard;
    let executionContext: DeepMocked<ExecutionContext>;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: AuthService,
                    useValue: {
                        validateUser: jest.fn(async (loginDto: LoginDto) => {
                            if (!loginDto.userNameOrEmail || !loginDto.password) return undefined;
                            return Object.assign(new User(), {email: loginDto.userNameOrEmail})
                        })
                    }
                },
                LocalStrategy,
                LocalAuthGuard
            ],
            imports: [
                PassportModule.register({defaultStrategy: 'local'}),
            ]
        }).compile();

        localAuthGuard = moduleRef.get<LocalAuthGuard>(LocalAuthGuard);
        executionContext = createMock<ExecutionContext>();

    });

    it('should be defined', () => {
        expect(localAuthGuard).toBeDefined();
    });

    describe('canActivate', () => {
        it('should return true with existing credentials', async (done) => {

            const request = executionContext.switchToHttp().getRequest;
            request.mockReturnValue({
                    body: {
                        userNameOrEmail: 'test@test.de',
                        password: 'test_password'
                    }
            });
            
            expect(await localAuthGuard.canActivate(executionContext)).toBeTruthy();

            done();
        });

        it('should throw unauthorized exception if some credentials are missing', async (done) => {

            const request = executionContext.switchToHttp().getRequest;
            request.mockReturnValue({
                body: {
                    userNameOrEmail: 'test@test.de',
                }
            });

            try {
                await localAuthGuard.canActivate(executionContext);
                fail(`should have thrown ${UnauthorizedException.name}`);
            } catch (e) {
                expect(e).toBeInstanceOf(UnauthorizedException);
            }
            done();
        });

        it('should throw unauthorized exception if the property names have the wrong form', async (done) => {

            const request = executionContext.switchToHttp().getRequest;
            request.mockReturnValue({
                body: {
                    user_name_or_email: 'test@test.de',
                }
            });

            try {
                await localAuthGuard.canActivate(executionContext);
                fail(`should have thrown ${UnauthorizedException.name}`);
            } catch (e) {
                expect(e).toBeInstanceOf(UnauthorizedException);
            }
            done();
        });
    });
});