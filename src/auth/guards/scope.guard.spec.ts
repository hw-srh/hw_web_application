import {Test} from '@nestjs/testing';
import {User} from "../../entities";
import {ScopeGuard} from "./scope.guard";
import {DeepMocked} from "@golevelup/ts-jest/lib/mocks";
import {ExecutionContext} from "@nestjs/common";
import {JwtStrategy} from "../strategies/jwt.strategy";
import {createMock} from "@golevelup/ts-jest";
import {Reflector} from "@nestjs/core";
import {PassportModule} from "@nestjs/passport";
import {JwtModule, JwtService} from "@nestjs/jwt";
import {AuthService} from "../auth.service";
import {ConfigService} from "@nestjs/config";
import {Scope} from "../interfaces/scope.interface";

describe('ScopeGuard', () => {

    let executionContext: DeepMocked<ExecutionContext>;

    let reflector: Reflector;
    let scopeGuard: ScopeGuard;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: AuthService,
                    useValue: {
                        hasJwtExpired: jest.fn(() => false),
                        validateJwt: jest.fn(() => true),
                        constructUserFromJwtPayload: jest.fn(() => new User()),
                    }
                },
                {
                    provide: ConfigService,
                    useValue: {
                        get: jest.fn(() => ({secret: "some_random_key"})),
                    }
                },
                {
                    provide: Reflector,
                    useValue: {
                        get: jest.fn(() => []),
                    }
                },
                JwtStrategy,
                ScopeGuard,
            ],
            imports: [
                PassportModule.register({defaultStrategy: 'jwt'}),
                JwtModule.register({secret: "some_random_key"}),
            ]
        }).compile();

        const jwtService = moduleRef.get<JwtService>(JwtService);
        const jwt = await jwtService.signAsync({
            sub: 10,
            exp: Math.floor((Date.now() + 30 * 60000) / 1000),
            jti: "some_uuid",
            context: {
                user: {},
                roles: []
            }
        });

        executionContext = createMock<ExecutionContext>();
        executionContext.switchToHttp().getRequest.mockReturnValue({
            headers: {
                authorization: `Bearer ${jwt}`
            }
        });

        const jwtStrategy = moduleRef.get<JwtStrategy>(JwtStrategy);
        jest.spyOn(jwtStrategy, 'validate').mockImplementation(async () => new User());

        reflector = moduleRef.get<Reflector>(Reflector);
        scopeGuard = moduleRef.get<ScopeGuard>(ScopeGuard);
    });

    describe('canActivate', () => {

        const scopeReturningFalse =
            new class implements Scope {
                alias = 'test';

                async hasAccessToScope(context: ExecutionContext, user: User): Promise<boolean> {
                    return false;
                }
            };

        const scopeReturningTrue =
            new class implements Scope {
                alias = 'test2';

                async hasAccessToScope(context: ExecutionContext, user: User): Promise<boolean> {
                    return true;
                }
            };


        it('should return false if no scope has been defined', async () => {
            await expect(scopeGuard.canActivate(executionContext)).resolves.toBeFalsy();
        });

        it('should return true if at least one scope validation method returns true', async () => {

            jest.spyOn(reflector, 'get').mockReturnValue([scopeReturningFalse, scopeReturningTrue]);
            await expect(scopeGuard.canActivate(executionContext)).resolves.toBeTruthy();
        });

        it('should return false if all scopes validation methods return false', async () => {

            jest.spyOn(reflector, 'get').mockReturnValue([scopeReturningFalse, scopeReturningFalse]);
            await expect(scopeGuard.canActivate(executionContext)).resolves.toBeFalsy();
        });

    });

});