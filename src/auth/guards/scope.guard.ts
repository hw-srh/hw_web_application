import {ExecutionContext, Inject, Injectable, Optional} from '@nestjs/common';
import {Reflector} from "@nestjs/core";
import {Scope} from "../interfaces/scope.interface";
import {JwtAuthGuard} from "./jwt-auth.guard";
import {User} from "../../entities";

@Injectable()
export class ScopeGuard extends JwtAuthGuard {

    constructor(
        private reflector: Reflector,
        @Optional()
        @Inject('SCOPES_DEPENDENCIES')
        private scopeDependencies: Map<{ new(...params: any): Scope }, any>
    ) {
        super();
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {

        const user: User = await this.getCurrentUser(context);

        const controllerScopes = this.getControllerScopes(context);
        if (await this.hasAccessToAtLeastOneScope(context, user, controllerScopes)) {
            return true;
        }

        const controllerMethodScopes = this.getControllerMethodScopes(context);
        return this.hasAccessToAtLeastOneScope(context, user, controllerMethodScopes);
    }

    private async hasAccessToAtLeastOneScope(context: ExecutionContext, user: User, scopes: Scope[]) {
        if (Array.isArray(scopes) && (scopes.length > 0)) {
            for (const scope of scopes) {
                if (await scope.hasAccessToScope(context, user, this.scopeDependencies)) {
                    return true;
                }
            }
        }

        return false;
    }

    private getControllerScopes(context: ExecutionContext): Scope[] {
        return this.reflector.get<Scope[]>('scopes', context.getClass()) ?? [];
    }

    private getControllerMethodScopes(context: ExecutionContext): Scope[] {
        return this.reflector.get<Scope[]>('scopes', context.getHandler()) ?? [];
    }

    private async getCurrentUser(context: ExecutionContext): Promise<User> {
        await super.canActivate(context);
        // injected by the handleRequest method from the parent class after calling super.canActivate
        return context.switchToHttp().getRequest().user;
    }
}