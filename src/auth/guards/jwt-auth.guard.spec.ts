import {Test} from '@nestjs/testing';
import {AuthService} from "../auth.service";
import {User} from "../../entities";
import {JwtAuthGuard} from "./jwt-auth.guard";
import {PassportModule} from "@nestjs/passport";
import {JwtModule, JwtService} from "@nestjs/jwt";
import {ConfigService} from "@nestjs/config";
import {DeepMocked} from "@golevelup/ts-jest/lib/mocks";
import {ExecutionContext, UnauthorizedException} from "@nestjs/common";
import {JwtStrategy} from "../strategies/jwt.strategy";
import {createMock} from "@golevelup/ts-jest";
import {JwtExpiredException} from "../exceptions/jwt-expired.exception";


describe('JwtAuthGuard', () => {

    let jwtAuthGuard: JwtAuthGuard;
    let executionContext: DeepMocked<ExecutionContext>;
    let authServiceMock: AuthService;
    let jwt: string;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: AuthService,
                    useValue: {
                        hasJwtExpired: jest.fn(() => false),
                        validateJwt: jest.fn(() => true),
                        constructUserFromJwtPayload: jest.fn(() => new User()),
                    }
                },
                {
                    provide: ConfigService,
                    useValue: {
                        get: jest.fn(() => ({secret: "some_random_key"})),
                    }
                },
                JwtStrategy,
                JwtAuthGuard,
            ],
            imports: [
                PassportModule.register({defaultStrategy: 'jwt'}),
                JwtModule.register({secret: "some_random_key"}),
            ]
        }).compile();


        const jwtService = moduleRef.get<JwtService>(JwtService);
        jwt = await jwtService.signAsync({
            sub: 10,
            exp: Math.floor((Date.now() + 30 * 60000) / 1000),
            jti: "some_uuid",
            context: {
                user: {},
                roles: []
            }
        });

        executionContext = createMock<ExecutionContext>();
        executionContext.switchToHttp().getRequest.mockReturnValue({
            headers: {
                authorization: `Bearer ${jwt}`
            }
        });

        jwtAuthGuard = moduleRef.get<JwtAuthGuard>(JwtAuthGuard);
        authServiceMock = moduleRef.get<AuthService>(AuthService);
    });

    describe('canActivate', () => {
        it('should return true for a request with a valid jwt', async (done) => {
            expect(await jwtAuthGuard.canActivate(executionContext)).toBeTruthy();
            done();
        });


        it('should throw an Unauthorized exception if the jwt token is invalid', async (done) => {

            executionContext.switchToHttp().getRequest.mockReturnValue({
                headers: {
                    authorization: `Bearer ${jwt}INVALID`
                }
            });

            try {
                await jwtAuthGuard.canActivate(executionContext);
                fail(`should have thrown ${UnauthorizedException.name}`);
            } catch (e) {
                expect(e).toBeInstanceOf(UnauthorizedException);
            }
            done();
        });

        it('should throw an jwt expired exception when the jwt token has expired', async (done) => {

            jest.spyOn(authServiceMock, 'hasJwtExpired').mockImplementation(() => true);

            try {
                await jwtAuthGuard.canActivate(executionContext);
                fail(`should have thrown ${JwtExpiredException.name}`);
            } catch (e) {
                expect(e).toBeInstanceOf(JwtExpiredException);
            }
            done();
        });
    });
    
});