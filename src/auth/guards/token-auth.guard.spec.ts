import {Test} from '@nestjs/testing';
import {AuthService} from "../auth.service";
import {User} from "../../entities";
import {TokenStrategy} from "../strategies/token.strategy";
import {TokenAuthGuard} from "./token-auth.guard";
import {ExecutionContext, UnauthorizedException} from "@nestjs/common";
import {createMock} from "@golevelup/ts-jest";
import {PassportModule} from "@nestjs/passport";
import {DeepMocked} from "@golevelup/ts-jest/lib/mocks";


describe('TokenAuthGuard', () => {

    let authServiceMock: AuthService;
    let tokenAuthGuard: TokenAuthGuard;
    let executionContext: DeepMocked<ExecutionContext>;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: AuthService,
                    useValue: {
                        validateUserByEmailAndPasswordResetToken: jest.fn(async (token?: string) => {
                            if (typeof token !== "string") return undefined;
                            return new User();
                        }),
                    }
                },
                TokenStrategy,
                TokenAuthGuard
            ],
            imports: [
                PassportModule.register({defaultStrategy: 'token'}),
            ]
        }).compile();

        authServiceMock = moduleRef.get<AuthService>(AuthService);
        tokenAuthGuard = moduleRef.get<TokenAuthGuard>(TokenAuthGuard);
        executionContext = createMock<ExecutionContext>();

    });

    it('should be defined', () => {
        expect(TokenAuthGuard).toBeDefined();
    });

    describe('canActivate', () => {
        it('should return true when token and email address have been proved', async (done) => {

            const request = executionContext.switchToHttp().getRequest;
            request.mockReturnValue({
                body: {
                    token: "some_random_token_id",
                    email: 'some_users_email@test.test'
                }
            });

            expect(await tokenAuthGuard.canActivate(executionContext)).toBeTruthy();

            done();
        });

        it('should throw unauthorized exception if the token is missing from the request body', async (done) => {

            const request = executionContext.switchToHttp().getRequest;
            request.mockReturnValue({
                body: {
                    email: 'some_users_email@test.test'
                }
            });

            try {
                await tokenAuthGuard.canActivate(executionContext);
                fail(`should have thrown ${UnauthorizedException.name}`);
            } catch (e) {
                expect(e).toBeInstanceOf(UnauthorizedException);
            }
            done();
        });

        it('should throw unauthorized exception if the email is missing from the request body', async (done) => {

            const request = executionContext.switchToHttp().getRequest;
            request.mockReturnValue({
                body: {
                    token: "some_random_token_id"
                }
            });

            try {
                await tokenAuthGuard.canActivate(executionContext);
                fail(`should have thrown ${UnauthorizedException.name}`);
            } catch (e) {
                expect(e).toBeInstanceOf(UnauthorizedException);
            }
            done();
        });
    });
});