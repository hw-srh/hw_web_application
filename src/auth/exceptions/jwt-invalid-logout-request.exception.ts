import {JwtExpiredException} from "./jwt-expired.exception";

export class JwtInvalidLogoutRequestException extends JwtExpiredException {
}