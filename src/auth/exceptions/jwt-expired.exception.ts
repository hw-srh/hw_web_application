import {HttpException, HttpStatus} from "@nestjs/common";

export class JwtExpiredException extends HttpException {
    constructor() {
        super('jwt expired', HttpStatus.UNAUTHORIZED);
    }
}