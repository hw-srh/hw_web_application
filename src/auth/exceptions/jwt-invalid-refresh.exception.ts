import {HttpException, HttpStatus} from "@nestjs/common";

export class JwtInvalidRefreshException extends HttpException {
    constructor() {
        super('jwt token is still valid', HttpStatus.UNAUTHORIZED);
    }
}