import {BadRequestException} from "@nestjs/common";

export class PasswordResetTokenExpiredException extends BadRequestException {

}