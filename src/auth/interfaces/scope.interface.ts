import {ExecutionContext} from "@nestjs/common";
import {User} from "../../entities";

export interface Scope {
    alias: string;

    hasAccessToScope(context: ExecutionContext, user: User, otherDependencies?: any): Promise<boolean>;
}