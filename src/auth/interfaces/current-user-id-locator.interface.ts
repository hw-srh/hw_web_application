import {Request} from "express";

export interface CurrentUserIdLocator {
    findIds(req: Request, otherDependencies?: any): Promise<number[]>;
}