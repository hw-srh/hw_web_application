import {Test} from '@nestjs/testing';
import {AuthService} from "../auth.service";
import {JwtRefreshStrategy} from "./jwt-refresh.strategy";
import {createMock} from "@golevelup/ts-jest";
import {Request, Response} from 'express';
import * as passport from "passport";
import {PassportModule} from "@nestjs/passport";
import {JwtModule, JwtService} from "@nestjs/jwt";
import {ConfigService} from "@nestjs/config";
import {User} from "../../entities";
import {JwtInvalidRefreshException} from "../exceptions/jwt-invalid-refresh.exception";
import {UnauthorizedException} from "@nestjs/common";


describe('JwtRefreshStrategy', () => {

    let authServiceMock: AuthService;
    let strategy: JwtRefreshStrategy;
    let jwtService: JwtService;
    let jwt: string;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: AuthService,
                    useValue: {
                        hasJwtExpired: jest.fn(() => true),
                        removeJti: jest.fn(() => {
                            return;
                        }),
                        validateJwt: jest.fn(() => true),
                        constructUserFromJwtPayload: jest.fn(() => new User()),
                    }
                },
                {
                    provide: ConfigService,
                    useValue: {
                        get: jest.fn(() => ({secret: "some_random_key"})),
                    }
                },
                JwtRefreshStrategy,
            ],
            imports: [
                PassportModule.register({defaultStrategy: 'jwt-refresh'}),
                JwtModule.register({secret: "some_random_key"}),
            ]
        }).compile();

        authServiceMock = moduleRef.get<AuthService>(AuthService);
        strategy = moduleRef.get<JwtRefreshStrategy>(JwtRefreshStrategy);

        jwtService = moduleRef.get<JwtService>(JwtService);

        jwt = await jwtService.signAsync({
            sub: 10,
            exp: Math.floor((Date.now() + 30 * 60000) / 1000),
            jti: "some_uuid",
            context: {
                user: {},
                roles: []
            }
        });
    });

    it('should be defined', () => {
        expect(strategy).toBeDefined();
    });

    describe('validate', () => {
        it('should return status code 200 if the jwt token is valid', async (done) => {

            const request = createMock<Request>();
            request.headers = {
                authorization: `Bearer ${jwt}`
            };

            const response = createMock<Response>();
            response.statusCode = 200;

            passport.authenticate('jwt-refresh')(request, response);
            expect(response.statusCode).toEqual(200);
            done();
        });

        it('should fail with status code 401 unauthorized when the jwt token is invalid', async (done) => {

            const request = createMock<Request>();
            request.headers = {
                authorization: `Bearer ${jwt}INVALID`
            };
            const response = createMock<Response>();
            response.statusCode = 200;

            passport.authenticate('jwt-refresh')(request, response);

            expect(response.statusCode).toEqual(401);
            done();
        });

        it('should throw an invalid refresh exception when the jwt token has NOT expired yet', async (done) => {

            jest.spyOn(authServiceMock, 'hasJwtExpired').mockImplementation(() => false);

            const request = createMock<Request>();
            request.headers = {
                authorization: `Bearer ${jwt}`
            };
            const response = createMock<Response>();

            passport.authenticate('jwt-refresh', (error) => {
                expect(error).toBeInstanceOf(JwtInvalidRefreshException);
                done();
            })(request, response);
        });

        it('should throw an Unauthorized exception when the jwt is more than one month old', async (done) => {

            const oneMonthOldJwt = await jwtService.signAsync({
                sub: 10,
                // 32 days old so definitely older than one month
                exp: Math.floor((Date.now() - 60000 * 60 * 24 * 32) / 1000),
                jti: "some_uuid",
                context: {
                    user: {},
                    roles: []
                }
            });

            const request = createMock<Request>();
            request.headers = {
                authorization: `Bearer ${oneMonthOldJwt}`
            };
            const response = createMock<Response>();
            response.statusCode = 200;

            passport.authenticate('jwt-refresh',  (error) => {
                expect(error).toBeInstanceOf(UnauthorizedException);
                done();
            })(request, response);

        });
    });
});