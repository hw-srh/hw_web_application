import {Test} from '@nestjs/testing';
import {AuthService} from "../auth.service";
import {User} from "../../entities";
import {LocalStrategy} from "./local.strategy";
import {LoginDto} from "../../common/shared_interfaces/dto/login/login.dto";
import {createMock} from "@golevelup/ts-jest";
import {Request, Response} from 'express';
import * as passport from "passport";


describe('LocalStrategy', () => {

    let authServiceMock: AuthService;
    let strategy: LocalStrategy;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: AuthService,
                    useValue: {
                        validateUser: jest.fn(async (loginDto: LoginDto) => {
                            if (!loginDto.userNameOrEmail || !loginDto.password) return undefined;
                            return Object.assign(new User(), {email: loginDto.userNameOrEmail})
                        })
                    }
                },
                LocalStrategy,
            ]
        }).compile();

        authServiceMock = moduleRef.get<AuthService>(AuthService);
        strategy = moduleRef.get<LocalStrategy>(LocalStrategy);
    });

    it('should be defined', () => {
        expect(strategy).toBeDefined();
    });

    describe('validate', () => {
        it('should return status code 200', async (done) => {

            const request = createMock<Request>();
            request.body = {
                userNameOrEmail: 'test@test.de',
                password: 'test_password'
            };

            const response = createMock<Response>();
            response.statusCode = 200;

            passport.authenticate('local')(request, response);

            expect(response.statusCode).toEqual(200);
            done();
        });

        it('should return status code 400 because the password field is missing from the request body', async (done) => {

            const request = createMock<Request>();
            request.body = {
                userNameOrEmail: 'test@test.de'
            };

            const response = createMock<Response>();
            response.statusCode = 200;

            passport.authenticate('local')(request, response);

            expect(response.statusCode).toEqual(400);
            done();
        });
    });
});