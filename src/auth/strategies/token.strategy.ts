import {PassportStrategy} from "@nestjs/passport";
import {AuthService} from "../auth.service";
import {Injectable, UnauthorizedException} from "@nestjs/common";
import {Strategy} from "passport-local";

@Injectable()
export class TokenStrategy extends PassportStrategy(Strategy, 'token') {
    constructor(private authService: AuthService) {
        super({
            usernameField: 'email',
            passwordField: 'token',
            session: false
        });
    }

    async validate(email: string, token: string): Promise<object> {
        const user = await this.authService.validateUserByEmailAndPasswordResetToken(email.trim(), token.trim());
        if (!user) {
            throw new UnauthorizedException();
        }
        return user;
    }
}