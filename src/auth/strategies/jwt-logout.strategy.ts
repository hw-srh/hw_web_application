import {ExtractJwt, Strategy} from 'passport-jwt';
import {PassportStrategy} from '@nestjs/passport';
import {Injectable} from '@nestjs/common';
import {AuthService} from "../auth.service";
import {JwtPayload} from "../../common/shared_interfaces/jwt-payload.interface";
import {JwtInvalidLogoutRequestException} from "../exceptions/jwt-invalid-logout-request.exception";
import {ConfigService} from "@nestjs/config";
import {JwtModuleConfig} from "../../config/interfaces/jwt-config.interface";

@Injectable()
export class JwtLogoutStrategy extends PassportStrategy(Strategy, 'jwt-logout') {
    constructor(
        private authService: AuthService,
        private configService: ConfigService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: true, // ignore the build in expiration check
            secretOrKey: configService.get<JwtModuleConfig>('jwt').secret,
        });
    }

    async validate(payload: JwtPayload): Promise<{}> {

        if (this.authService.hasJwtExpired(payload)) {
            throw new JwtInvalidLogoutRequestException();
        }

        await this.authService.removeJti(payload);

        return {};
    }
}