import {ExtractJwt, Strategy} from 'passport-jwt';
import {PassportStrategy} from '@nestjs/passport';
import {Injectable} from '@nestjs/common';
import {JwtPayload} from "../../common/shared_interfaces/jwt-payload.interface";
import {AuthService} from "../auth.service";
import {User} from "../../entities";
import {JwtExpiredException} from "../exceptions/jwt-expired.exception";
import {ConfigService} from "@nestjs/config";
import {JwtModuleConfig} from "../../config/interfaces/jwt-config.interface";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        private authService: AuthService,
        private configService: ConfigService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: true, // ignore the build in expiration check
            secretOrKey: configService.get<JwtModuleConfig>('jwt').secret,
        });
    }

    async validate(payload: JwtPayload): Promise<User> {

        if (this.authService.hasJwtExpired(payload)) {
            throw new JwtExpiredException();
        }

        await this.authService.validateJwt(payload);

        return this.authService.constructUserFromJwtPayload(payload);
    }
}