import {Test} from '@nestjs/testing';
import {AuthService} from "../auth.service";
import {User} from "../../entities";
import {TokenStrategy} from "./token.strategy";
import {createMock} from "@golevelup/ts-jest";
import {Request, Response} from 'express';
import * as passport from "passport";

describe('TokenStrategy', () => {

    let authServiceMock: AuthService;
    let strategy: TokenStrategy;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: AuthService,
                    useValue: {
                        validateUserByEmailAndPasswordResetToken: jest.fn(async (token?: string) => {
                            if (typeof token !== "string") return undefined;
                            return new User();
                        })
                    }
                },
                TokenStrategy,
            ]
        }).compile();

        authServiceMock = moduleRef.get<AuthService>(AuthService);
        strategy = moduleRef.get<TokenStrategy>(TokenStrategy);
    });

    it('should be defined', () => {
        expect(strategy).toBeDefined();
    });

    describe('validate', () => {
        it('should return status code 200', async (done) => {

            const request = createMock<Request>();
            request.body = {
                token: 'some_random_token_id...',
                email: 'some_users_email@test.test'
            };

            const response = createMock<Response>();
            response.statusCode = 200;

            passport.authenticate('token')(request, response);

            expect(response.statusCode).toEqual(200);
            done();
        });

        it('should return status code 400 because the email field is missing from the request body', async (done) => {

            const request = createMock<Request>();
            request.body = {
                token: 'some_random_token_id...',
            };
            const response = createMock<Response>();
            response.statusCode = 200;

            try {
                passport.authenticate('token')(request, response);
            } catch (e) {
            }

            expect(response.statusCode).toEqual(400);
            done();
        });

        it('should return status code 400 because the token field is missing from the request body', async (done) => {

            const request = createMock<Request>();
            request.body = {};

            const response = createMock<Response>();
            response.statusCode = 200;

            try {
                passport.authenticate('token')(request, response);
            } catch (e) {
            }

            expect(response.statusCode).toEqual(400);
            done();
        });
    });
});