import {Strategy} from 'passport-local';
import {PassportStrategy} from '@nestjs/passport';
import {Injectable, UnauthorizedException} from '@nestjs/common';
import {AuthService} from "../auth.service";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private authService: AuthService) {
        super({
            usernameField: 'userNameOrEmail',
            passwordField: 'password',
            session: false,
        });
    }

    async validate(userNameOrEmail: string, password: string): Promise<object> {
        const user = await this.authService.validateUser({
            userNameOrEmail: userNameOrEmail.trim(),
            password: password.trim()
        });
        if (!user) {
            throw new UnauthorizedException();
        }
        return user;
    }
}