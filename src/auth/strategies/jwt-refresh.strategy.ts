import {ExtractJwt, Strategy} from 'passport-jwt';
import {PassportStrategy} from '@nestjs/passport';
import {Injectable, UnauthorizedException} from '@nestjs/common';
import {AuthService} from "../auth.service";
import {JwtPayload} from "../../common/shared_interfaces/jwt-payload.interface";
import {User} from "../../entities";
import {JwtInvalidRefreshException} from "../exceptions/jwt-invalid-refresh.exception";
import {ConfigService} from "@nestjs/config";
import {JwtModuleConfig} from "../../config/interfaces/jwt-config.interface";


@Injectable()
export class JwtRefreshStrategy extends PassportStrategy(Strategy, 'jwt-refresh') {
    constructor(
        private authService: AuthService,
        private configService: ConfigService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: true, // ignore the build in expiration check
            secretOrKey: configService.get<JwtModuleConfig>('jwt').secret,
        });
    }

    private static isMoreThanOneMonthOld(payload: JwtPayload) {
        const differenceInSeconds = Math.floor((Date.now() / 1000)) - payload.exp;
        const days = differenceInSeconds / 3600 / 24;
        return days > 31;
    }

    async validate(payload: JwtPayload): Promise<User | undefined> {

        if (!this.authService.hasJwtExpired(payload)) {
            throw new JwtInvalidRefreshException();
        }

        if (JwtRefreshStrategy.isMoreThanOneMonthOld(payload)) {
            await this.authService.removeJti(payload);
            throw new UnauthorizedException("This token has not been used within the last month, authentication is required!");
        }

        await this.authService.validateJwt(payload);
        await this.authService.removeJti(payload);
        return this.authService.constructUserFromJwtPayload(payload);
    }
}