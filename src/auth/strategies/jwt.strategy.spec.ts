import {Test} from '@nestjs/testing';
import {AuthService} from "../auth.service";
import {User} from "../../entities";
import {JwtStrategy} from "./jwt.strategy";
import {createMock} from "@golevelup/ts-jest";
import {Request, Response} from 'express';
import * as passport from "passport";
import {PassportModule} from "@nestjs/passport";
import {JwtModule, JwtService} from "@nestjs/jwt";
import {ConfigService} from "@nestjs/config";
import {JwtExpiredException} from "../exceptions/jwt-expired.exception";


describe('JwtStrategy', () => {

    let authServiceMock: AuthService;
    let strategy: JwtStrategy;
    let jwt: string;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: AuthService,
                    useValue: {
                        hasJwtExpired: jest.fn(() => false),
                        validateJwt: jest.fn(() => true),
                        constructUserFromJwtPayload: jest.fn(() => new User()),
                    }
                },
                {
                    provide: ConfigService,
                    useValue: {
                        get: jest.fn(() => ({secret: "some_random_key"})),
                    }
                },

                JwtStrategy,
            ],
            imports: [
                PassportModule.register({defaultStrategy: 'jwt'}),
                JwtModule.register({secret: "some_random_key"}),
            ]
        }).compile();

        authServiceMock = moduleRef.get<AuthService>(AuthService);
        strategy = moduleRef.get<JwtStrategy>(JwtStrategy);

        const jwtService = moduleRef.get<JwtService>(JwtService);

        jwt = await jwtService.signAsync({
            sub: 10,
            exp: Math.floor((Date.now() + 30 * 60000) / 1000),
            jti: "some_uuid",
            context: {
                user: {},
                roles: []
            }
        });
    });

    it('should be defined', () => {
        expect(strategy).toBeDefined();
    });

    describe('validate', () => {
        it('should return status code 200 if the jwt token is valid', async (done) => {

            const request = createMock<Request>();
            request.headers = {
                authorization: `Bearer ${jwt}`
            };

            const response = createMock<Response>();
            response.statusCode = 200;

            passport.authenticate('jwt')(request, response);
            expect(response.statusCode).toEqual(200);
            done();
        });

        it('should fail with status code 401 unauthorized when the jwt token is invalid', async (done) => {

            const request = createMock<Request>();
            request.headers = {
                authorization: `Bearer ${jwt}INVALID`
            };
            const response = createMock<Response>();
            response.statusCode = 200;

            passport.authenticate('jwt')(request, response);

            expect(response.statusCode).toEqual(401);
            done();
        });

        it('should throw an jwt expired exception when the jwt token has expired', async (done) => {

            jest.spyOn(authServiceMock, 'hasJwtExpired').mockImplementation(() => true);

            const request = createMock<Request>();
            request.headers = {
                authorization: `Bearer ${jwt}`
            };
            const response = createMock<Response>();

            passport.authenticate('jwt', (error) => {
                expect(error).toBeInstanceOf(JwtExpiredException);
                done();
            })(request, response);

        });
    });
});