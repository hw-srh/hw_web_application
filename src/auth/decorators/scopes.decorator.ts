import {Scope} from "../interfaces/scope.interface";
import {SetMetadata} from "@nestjs/common";

export const RestrictToScopes = (...scopes: Scope[]) => SetMetadata('scopes', scopes);

