import {Injectable, UnauthorizedException} from '@nestjs/common';
import {UsersService} from '../users/users.service';
import * as bcrypt from 'bcrypt';
import {JwtService} from "@nestjs/jwt";
import {Role, User} from "../entities";
import {JwtPayload} from "../common/shared_interfaces/jwt-payload.interface";
import {v4 as uuidv4} from 'uuid';
import {RedisService} from "../redis/redis.service";
import {LoginDto} from "../common/shared_interfaces/dto/login/login.dto";
import {PasswordResetTokensService} from "../password-reset-tokens/password-reset-tokens.service";
import {PasswordResetTokenExpiredException} from "./exceptions/password-reset-token-expired-exception";


@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService,
        private jwtService: JwtService,
        private redisService: RedisService,
        private passwordResetTokenService: PasswordResetTokensService
    ) {
    }

    async validateUser(loginDto: LoginDto): Promise<User | null> {
        const user = await this.usersService.findOneByUserNameOrEmail(loginDto.userNameOrEmail);

        if (!user) {
            return null;
        }
        const match = await bcrypt.compare(loginDto.password, user.password);
        return match ? user : null;
    }

    private static constructRedisKeyForUsersJtis(entity: User): string {
        return `jtis_user_${entity?.id}`;
    }

    private static constructJwtPayloadFromUserObject(user: User): JwtPayload {
        return {
            sub: user.id,
            // after 30 minutes the token has to be refreshed,
            // also the JWT standard requires the time in seconds and not milliseconds
            exp: Math.floor((Date.now() + 30 * 60000) / 1000),
            jti: uuidv4(),
            context: {
                user: {
                    email: user.email,
                    username: user.username
                },
                roles: user.roles ? user.roles.map((role: Role) => role.id) : []
            }
        };
    }

    async constructUserFromJwtPayload(payload: JwtPayload): Promise<User> {
        const user = new User();
        user.id = payload.sub;
        user.username = payload.context.user.username;
        user.email = payload.context.user.email;
        user.roles = payload.context.roles.map(
            (role: string) => {
                return {id: role, users: null}
            }
        );
        return user;
    }

    async login(user: User): Promise<{ access_token: string }> {
        const payload: JwtPayload = AuthService.constructJwtPayloadFromUserObject(user);

        const access_token = await this.jwtService.signAsync(payload);

        await this.saveJti(payload);

        return {access_token};
    }

    hasJwtExpired(payload: JwtPayload): boolean {
        return Math.floor((Date.now() / 1000)) > payload.exp;
    }

    async validateJwt(payload: JwtPayload): Promise<void> {

        const user = Object.assign(new User, {id: payload.sub});

        if (!await this.redisService.doesValueExistInSet(AuthService.constructRedisKeyForUsersJtis(user), payload.jti)) {
            throw new UnauthorizedException();
        }
    }

    async validateUserByEmailAndPasswordResetToken(email: string, token: string): Promise<User | null> {
        const passwordResetToken = await this.passwordResetTokenService.findPasswordResetToken(token);

        if (!passwordResetToken?.user) {
            return null;
        }

        await this.passwordResetTokenService.deleteAllTokensForUser(passwordResetToken?.user);

        if ((passwordResetToken?.created.getTime() + 30 * 60000) < (Date.now())) {
            throw new PasswordResetTokenExpiredException();
        }

        return passwordResetToken?.user?.email === email ? passwordResetToken?.user : null;
    }

    async removeJti(payload: JwtPayload): Promise<void> {
        const user = Object.assign(new User, {id: payload.sub});
        return await this.redisService.removeValueFromSet(AuthService.constructRedisKeyForUsersJtis(user), payload.jti);
    }

    async invalidateJwtsForUser(entity: User): Promise<void> {
        await this.redisService.removeSet(AuthService.constructRedisKeyForUsersJtis(entity));
    }

    private async saveJti(payload: JwtPayload): Promise<void> {
        const user = Object.assign(new User, {id: payload.sub});
        return await this.redisService.addValueToSet(AuthService.constructRedisKeyForUsersJtis(user), payload.jti);
    }
}