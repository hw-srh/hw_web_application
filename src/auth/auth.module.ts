import {forwardRef, Module} from '@nestjs/common';
import {AuthService} from './auth.service';
import {LocalStrategy} from "./strategies/local.strategy";
import {UsersModule} from "../users/users.module";
import {PassportModule} from "@nestjs/passport";
import {AuthController} from "./auth.controller";
import {JwtModule} from "@nestjs/jwt";
import {JwtStrategy} from "./strategies/jwt.strategy";
import {JwtRefreshStrategy} from "./strategies/jwt-refresh.strategy";
import {APP_GUARD} from "@nestjs/core";
import {PublicAuthGuard} from "./guards/public.guard";
import {RedisModule} from "../redis/redis.module";
import {JwtLogoutStrategy} from "./strategies/jwt-logout.strategy";
import {ConfigService} from "@nestjs/config";
import {JwtModuleConfig} from "../config/interfaces/jwt-config.interface";
import {TokenStrategy} from "./strategies/token.strategy";
import {PasswordResetTokensModule} from "../password-reset-tokens/password-reset-tokens.module";

@Module({
    imports: [
        PasswordResetTokensModule,
        RedisModule,
        forwardRef(() => UsersModule),
        PassportModule.register({defaultStrategy: 'jwt'}),
        JwtModule.registerAsync({
            useFactory: (configService: ConfigService) => configService.get<JwtModuleConfig>('jwt'),
            inject: [ConfigService]
        }),
    ],
    providers: [
        AuthService,
        LocalStrategy,
        TokenStrategy,
        JwtStrategy,
        JwtRefreshStrategy,
        JwtLogoutStrategy,
        {
            provide: APP_GUARD, // makes this guard active globally
            useClass: PublicAuthGuard,
        },
    ],
    exports: [
        AuthService
    ],
    controllers: [AuthController],
})
export class AuthModule {
}
