import {Test, TestingModule} from '@nestjs/testing';
import {AuthService} from './auth.service';
import {UsersService} from "../users/users.service";
import {JwtService} from "@nestjs/jwt";
import {RedisService} from "../redis/redis.service";
import {PasswordResetToken, Role, User} from "../entities";
import * as bcrypt from "bcrypt";
import {LoginDto} from "../common/shared_interfaces/dto/login/login.dto";
import {JwtPayload} from "../common/shared_interfaces/jwt-payload.interface";
import {UnauthorizedException} from "@nestjs/common";
import {PasswordResetTokensService} from "../password-reset-tokens/password-reset-tokens.service";
import {PasswordResetTokenExpiredException} from "./exceptions/password-reset-token-expired-exception";

describe('AuthService', () => {
    let authService: AuthService;

    let usersService: UsersService;
    let jwtService: JwtService;
    let redisService: RedisService;
    let passwordRestTokenService: PasswordResetTokensService;

    let user: User;
    let jwtPayload: JwtPayload;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                {
                    provide: PasswordResetTokensService,
                    useValue: {
                        findPasswordResetToken: jest.fn(),
                        deleteAllTokensForUser: jest.fn(async () => {
                        })
                    }
                },
                {
                    provide: UsersService,
                    useValue: {
                        findOneByUserNameOrEmail: jest.fn(),
                    }
                },
                {
                    provide: JwtService,
                    useValue: {
                        signAsync: jest.fn()
                    }
                },
                {
                    provide: RedisService,
                    useValue: {
                        addValueToSet: jest.fn(),
                        doesValueExistInSet: jest.fn(),
                        removeValueFromSet: jest.fn(),
                        removeSet: jest.fn(),
                    }
                },
                AuthService
            ],
        }).compile();

        user = {
            id: 42,
            email: "test",
            password: "test",
            firstname: "test",
            lastname: "test",
            username: "test",
            roles: [{id: "admin", users: null}, {id: "something_else", users: null}]
        } as User;

        jwtPayload = {
            sub: user.id,
            exp: Math.floor((Date.now() + 30 * 60000) / 1000),
            jti: 'some_random_token_id',
            context: {
                user: {
                    email: user.email,
                    username: user.username,
                },
                roles: user.roles.map((role: Role) => role.id),
            }
        } as JwtPayload;

        authService = module.get<AuthService>(AuthService);

        usersService = module.get<UsersService>(UsersService);
        jwtService = module.get<JwtService>(JwtService);
        redisService = module.get<RedisService>(RedisService);
        passwordRestTokenService = module.get<PasswordResetTokensService>(PasswordResetTokensService);
    });

    describe('construct', () => {
        it('should be defined', async () => {
            expect(authService).toBeDefined();
        });
    });

    describe('validateUser', () => {

        let loginDto: LoginDto;

        beforeEach(async () => {
            loginDto = {
                userNameOrEmail: user.email,
                password: user.password
            };
        });

        it('should return a user object if the user is valid', async () => {
            const expected = {
                ...user,
                password: await bcrypt.hash(user.password, 5),
            };

            jest.spyOn(usersService, 'findOneByUserNameOrEmail').mockImplementation(async () => expected);

            const actual = await authService.validateUser(loginDto);

            expect(actual).toStrictEqual(expected);
        });

        it('should return null if the user password is invalid', async () => {
            const expected = {
                ...user,
                password: 'wrong_password_hash',
            };

            jest.spyOn(usersService, 'findOneByUserNameOrEmail').mockImplementation(async () => expected);

            const actual = await authService.validateUser(loginDto);

            expect(actual).toBeNull();
        });
    });

    describe('login', () => {
        it('should create a new jwt token, save the jwt id to redis and return the token', async (done) => {
            let expected = null;

            jest.spyOn(jwtService, 'signAsync').mockImplementation(async (payload: JwtPayload) => {
                expected = payload;
                return JSON.stringify(payload);
            });

            jest.spyOn(redisService, 'addValueToSet').mockImplementation(async (key, jti) => {
                expect(jti).toStrictEqual(expected.jti);
                done();
            });

            const actual = (await authService.login(user)).access_token;

            expect(actual).toStrictEqual(JSON.stringify(expected));
        });
    });

    describe('validateJwt', () => {
        it('should not throw if the jti exists in the redis jtis set of the current user', async () => {
            jest.spyOn(redisService, 'doesValueExistInSet').mockImplementation(async () => true);
            await expect(authService.validateJwt({sub: user.id} as JwtPayload)).resolves.not.toThrow();
        });

        it('should throw if the jti does not exists in the redis jtis set of the current user', async () => {
            jest.spyOn(redisService, 'doesValueExistInSet').mockImplementation(async () => false);
            await expect(authService.validateJwt({sub: user.id} as JwtPayload)).rejects.toThrow(UnauthorizedException);
        });
    });

    describe('constructUserFromJwtPayload', () => {
        it('should create a user object from the jwt content', async () => {

            const actual = await authService.constructUserFromJwtPayload(jwtPayload);

            expect(actual).toStrictEqual(Object.assign(new User(), {
                email: user.email,
                username: user.username,
                id: user.id,
                roles: user.roles
            }));
        });
    });


    describe('removeJti', () => {
        it('should remove the jti from the users jtis set in redis', async (done) => {
            jest.spyOn(redisService, 'removeValueFromSet').mockImplementation(async (set_key, jti) => {
                expect(jti).toEqual(jwtPayload.jti);
                done();
            });

            await authService.removeJti(jwtPayload);
        });
    });

    describe('hasJwtExpired', () => {
        it('should return true if the current timestamp exceeds the timestamp in the jwt', () => {
            expect(authService.hasJwtExpired({
                ...jwtPayload,
                exp: Math.floor((Date.now() - 60000) / 1000)
            })).toBeTruthy();
        });

        it('should return false if the current timestamp does not exceed the timestamp in the jwt', () => {
            expect(authService.hasJwtExpired(jwtPayload)).toBeFalsy();
        });
    });

    describe('invalidateJwtsForUser', () => {
        it('should remove the users jtis set from redis', async (done) => {
            jest.spyOn(redisService, 'removeSet').mockImplementation(async (key) => {
                expect(redisService.removeSet).toBeCalled();
                done();
            });
            await authService.invalidateJwtsForUser(user);
        });
    });

    describe('validateUserByEmailAndPasswordResetToken', () => {

        let passwordResetToken: PasswordResetToken;

        beforeEach(async () => {
            passwordResetToken = {
                id: 1,
                userId: user.id,
                tokenHash: '',
                created: new Date(),
                user
            };
        });

        it('should return null if the passwordResetToken was not found', async () => {
            jest.spyOn(passwordRestTokenService, 'findPasswordResetToken').mockImplementation(async () => {
                return null;
            });
            await expect(authService.validateUserByEmailAndPasswordResetToken(user.email, 'test_token')).resolves.toBe(null);
        });

        it('should return null if the users email does not match the expected email', async () => {
            jest.spyOn(passwordRestTokenService, 'findPasswordResetToken').mockImplementation(async () => passwordResetToken);
            await expect(authService.validateUserByEmailAndPasswordResetToken(user.email + 'new', 'test_token')).resolves.toBe(null);
        });

        it('should return the user if the email matches the given email and the token can be matched to a user', async () => {
            jest.spyOn(passwordRestTokenService, 'findPasswordResetToken').mockImplementation(async () => passwordResetToken);
            await expect(authService.validateUserByEmailAndPasswordResetToken(user.email, 'test_token')).resolves.toBe(user);
        });

        it('should return null if the user object is missing from the passwordResetToken object', async () => {
            jest.spyOn(passwordRestTokenService, 'findPasswordResetToken').mockImplementation(async () => ({
                ...passwordResetToken,
                user: null
            }));
            await expect(authService.validateUserByEmailAndPasswordResetToken(user.email, 'test_token')).resolves.toBe(null);
        });

        it(`should throw ${PasswordResetTokenExpiredException.name} when the token is older than 30 minutes`, async () => {
            jest.spyOn(passwordRestTokenService, 'findPasswordResetToken').mockImplementation(async () => {
                return {
                    ...passwordResetToken,
                    created: new Date(Date.now() - 31 * 60000)
                };
            });
            await expect(authService.validateUserByEmailAndPasswordResetToken(user.email, 'test_token')).rejects.toThrow(PasswordResetTokenExpiredException);
        });
    });
});
