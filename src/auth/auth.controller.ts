import {Controller, Delete, Post, Request, Res, UseGuards} from '@nestjs/common';
import {AuthService} from "./auth.service";
import {JwtRefreshGuard} from "./guards/jwt-refresh.guard";
import {LocalAuthGuard} from "./guards/local-auth.guard";
import {LogoutGuard} from "./guards/logout.guard";

@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) {
    }

    // @TODO protect against brute force
    @UseGuards(LocalAuthGuard)
    @Post('/login')
    async login(@Request() req) {
        return this.authService.login(req.user);
    }

    @UseGuards(JwtRefreshGuard)
    @Post('/refresh')
    async refreshToken(@Request() req) {
        return this.authService.login(req.user);
    }

    @UseGuards(LogoutGuard)
    @Delete('/logout')
    async logout(@Res() response) {
        return response.status(200).send();
    }
}
