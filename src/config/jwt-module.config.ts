import {JwtModuleConfig} from "./interfaces/jwt-config.interface";

export const jwtModuleConfig = (): { jwt: JwtModuleConfig } =>
    ({
        jwt: {
            secret: process.env.APP_SECRET,
        }
    });