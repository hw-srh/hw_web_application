import {dataBaseConfig} from './database.config';
import {jwtModuleConfig} from "./jwt-module.config";
import {redisConfig} from "./redis.config";
import {smtpConfig} from "./smtp.config";
import {bcryptConfig} from "./bcrypt.config";
import {appEnvironment} from "./app-environment.config";

export const configs = [
    dataBaseConfig,
    jwtModuleConfig,
    redisConfig,
    smtpConfig,
    bcryptConfig,
    appEnvironment
]