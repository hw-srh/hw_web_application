export type AppEnvironmentType = 'dev' | 'prod' | 'development' | 'production';

export const appEnvironment = (): { appEnv: AppEnvironmentType } =>
    ({
        appEnv: process.env.NODE_ENV as AppEnvironmentType ?? 'production'
    });