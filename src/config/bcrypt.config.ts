import {BcryptConfig} from "./interfaces/bcrypt-config.interface";

export const bcryptConfig = (): { bcrypt: BcryptConfig } =>
    ({
        bcrypt: {
            defaultSaltRounds: parseInt(process.env.DEFAULT_SALT_ROUNDS),
        }
    });