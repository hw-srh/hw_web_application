import {DatabaseConfig} from "./interfaces/database-config.interface";

export const dataBaseConfig = (): { database: DatabaseConfig } =>
    ({
        database: {
            type: process.env.DB_SERVER_TYPE,
            host: process.env.DB_HOST,
            port: parseInt(process.env.DB_PORT),
            username: process.env.DB_USERNAME,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_DEFAULT_DATABASE_NAME,
            // if set to true this will sync model changes with the database, DONT USE it in production!
            synchronize: false,
            autoLoadEntities: true
        }
    });