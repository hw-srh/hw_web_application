export interface SmtpConfig {
    host: string,
    port: number,
    secure: boolean // upgrade later with STARTTLS
    auth: {
        user: string,
        pass: string,
    },
}