type IpType = 4 | 6;

export interface RedisConfig {
    port: number;
    host: string;
    family: IpType;
    password: string;
    db: number;
    showFriendlyErrorStack: boolean;
}