export interface BcryptConfig {
    defaultSaltRounds: number
}