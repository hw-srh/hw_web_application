export interface DatabaseConfig {
    type: any;
    host: string;
    port: number;
    username: string;
    password: string;
    database: string;
    // if set to true this will sync model changes with the database, DONT USE it in production!
    synchronize: boolean;
    autoLoadEntities: boolean;
}