import {RedisConfig} from "./interfaces/redis-config.interface";

export const redisConfig = (): { redis: RedisConfig } =>
    ({
        redis: {
            port: 6379,
            host: process.env.REDIS_HOST,
            family: 4,
            password: undefined,
            db: 0,
            showFriendlyErrorStack: true
        }
    });