import {SmtpConfig} from "./interfaces/smtp-config.interface";

export const smtpConfig = (): { smtp: SmtpConfig } =>
    ({
        smtp: {
            host: process.env.SMTP_HOST,
            port: parseInt(process.env.SMTP_PORT),
            secure: process.env.SMTP_PORT === "465", // STARTTLS encryption with port 587 otherwise use TLS
            auth: {
                user: process.env.SMTP_USERNAME,
                pass: process.env.SMTP_PASSWORD,
            },
        }
    });