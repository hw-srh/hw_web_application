import {Logger, Module} from '@nestjs/common';
import {ServeStaticModule} from "@nestjs/serve-static";
import {join} from 'path';
import {TypeOrmModule} from "@nestjs/typeorm";
import {UsersModule} from "./users/users.module";
import {AuthModule} from './auth/auth.module';
import {RedisModule} from "./redis/redis.module";
import {entities} from "./entities";
import {ConfigModule, ConfigService} from "@nestjs/config";
import {DatabaseConfig} from "./config/interfaces/database-config.interface";
import {configs} from "./config";
import {PasswordResetTokensModule} from './password-reset-tokens/password-reset-tokens.module';
import {MailerModule} from "@nestjs-modules/mailer";
import {SmtpConfig} from "./config/interfaces/smtp-config.interface";
import {PasswordResetModule} from './password-reset/password-reset.module';
import {TasksModule} from './tasks/tasks.module';
import {TimeTrackingModule} from './time-tracking/time-tracking.module';
import {UsersTasksModule} from './users-tasks/users-tasks.module';
import {APP_FILTER} from "@nestjs/core";
import {AppExceptionsFilter} from "./app-exceptions.filter";
import {AppService} from "./app.service";

@Module({
    imports: [
        ServeStaticModule.forRoot({
            rootPath: join(__dirname, '..', 'client/build'),
            renderPath: '/*',
            exclude: ['/auth*', '/users*', '/password-reset/*', '/time-tracking/*']
        }),
        ConfigModule.forRoot(
            {
                isGlobal: true,
                cache: true,
                envFilePath: ['.env', '.development.env'], // existing env vars will not be overwritten, .env is loaded before .development.env
                load: configs
            }
        ),
        MailerModule.forRootAsync({
            useFactory: async (configService: ConfigService) => ({
                transport: configService.get<SmtpConfig>('smtp'),
                defaults: {
                    textEncoding: 'base64',
                    from: 'SRH password reset service',
                },
            }),
            inject: [ConfigService]
        }),
        TypeOrmModule.forFeature(entities),
        TypeOrmModule.forRootAsync({
            useFactory: async (configService: ConfigService) => configService.get<DatabaseConfig>('database'),
            inject: [ConfigService],
        }),
        UsersModule,
        AuthModule,
        RedisModule,
        PasswordResetTokensModule,
        PasswordResetModule,
        TasksModule,
        TimeTrackingModule,
        UsersTasksModule
    ],
    exports: [ConfigModule],
    controllers: [],
    providers: [
        {
            provide: APP_FILTER,
            useClass: AppExceptionsFilter,
        },
        Logger,
        AppService
    ]
})
export class AppModule {
}
