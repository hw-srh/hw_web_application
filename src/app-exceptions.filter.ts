import {ArgumentsHost, Catch, HttpException, Injectable, Logger} from '@nestjs/common';
import {BaseExceptionFilter} from '@nestjs/core';
import {Exception} from "./common/exceptions/exception";
import {Request} from 'express';
import {ConfigService} from "@nestjs/config";
import {AppEnvironmentType} from "./config/app-environment.config";

@Injectable()
@Catch()
export class AppExceptionsFilter extends BaseExceptionFilter {

    private readonly appEnv: string;

    constructor(private logger: Logger, private configService: ConfigService) {
        super();
        this.appEnv = configService.get<AppEnvironmentType>('appEnv');
    }

    private static isUnexpectedError(exception: Error) {
        return !(exception instanceof HttpException) && !(exception instanceof Exception);
    }

    catch(exception: Error, host: ArgumentsHost) {

        if (!AppExceptionsFilter.isUnexpectedError(exception) &&
            ['dev', 'development'].includes(this.appEnv)
        ) {
            this.logExpectedErrors(exception, host);
        }

        super.catch(exception, host);
    }

    private logExpectedErrors(exception: Error, host: ArgumentsHost) {
        const request: Request = host.switchToHttp().getRequest();
        const errorMessage = [
            `\n${exception.message} ${request.method} Request`,
            `url: ${request.url}`,
            `body: ${JSON.stringify(request.body)}`
        ]
        this.logger.error(errorMessage.join('\n'));
    }
}