import {Module} from '@nestjs/common';
import {RedisService} from "./redis.service";
import IORedis from "ioredis";
import {ConfigService} from "@nestjs/config";
import {RedisConfig} from "../config/interfaces/redis-config.interface";

@Module({
    providers: [
        {
            useFactory: async (configService: ConfigService): Promise<IORedis.Redis> => {
                return new IORedis(configService.get<RedisConfig>('redis'));
            },
            provide: 'RedisClient',
            inject: [ConfigService]
        },
        RedisService
    ],
    exports: [RedisService],
})
export class RedisModule {
}
