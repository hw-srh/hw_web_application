import {Inject, Injectable, OnApplicationShutdown} from '@nestjs/common';
import {Redis as RedisClient} from "ioredis";

@Injectable()
export class RedisService implements OnApplicationShutdown {

    constructor(@Inject('RedisClient') private redisClient: RedisClient) {
    }

    async addValueToSet(key: string, value: string): Promise<void> {
        await this.redisClient.sadd(key, value);
    }

    async getNumberOfElementsInSet(key: string): Promise<number> {
        return await this.redisClient.scard(key);
    }

    async getSet(key: string): Promise<string[]> {
        return await this.redisClient.smembers(key);
    }

    async doesValueExistInSet(key: string, value: string): Promise<boolean> {
        return await this.redisClient.sismember(key, value) === 1;
    }

    async removeSet(key: string): Promise<void> {
        await this.redisClient.del(key);
    }

    async removeValueFromSet(key: string, value: string): Promise<void> {
        await this.redisClient.srem(key, value);
    }

    async onApplicationShutdown(signal?: string): Promise<void> {
        this.redisClient.disconnect();
    }
}
