import {Test, TestingModule} from '@nestjs/testing';
import {RedisService} from "./redis.service";
import {Redis, Redis as RedisClient} from 'ioredis';

describe('RedisService', () => {

    let redisService: RedisService;
    let redisClient: Partial<Redis>;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                {
                    provide: 'RedisClient',
                    useValue: {
                        sadd: jest.fn(),
                        sismember: jest.fn(),
                        smembers: jest.fn(),
                        scard: jest.fn(),
                        del: jest.fn(),
                        srem: jest.fn()
                    }
                },
                RedisService
            ],
        }).compile();

        redisClient = module.get('RedisClient');
        redisService = module.get<RedisService>(RedisService);
    });

    it('should be defined', () => {
        expect(redisService).toBeDefined();
    });

    describe('addValueToSet', () => {
        it('should add the specified value to the redis set', (done) => {

            const expected = 'test_value';

            jest.spyOn(redisClient, 'sadd').mockImplementation(async (key, value) => {
                expect(value).toEqual(expected);
                done();
                return 1;
            });

            redisService.addValueToSet('some_key', expected);

        });
    });


    describe('doesValueExistInSet', () => {
        it('should return true if the value exists in the set', async () => {

            jest.spyOn(redisClient, 'sismember').mockImplementation(async (key, value) => {
                return 1;
            });

            expect(await redisService.doesValueExistInSet('some_key', 'test_value')).toBeTruthy();

        });
    });

    describe('removeSet', () => {
        it('should remove the entire set', (done) => {

            const expected = 'some_key';
            jest.spyOn(redisClient, 'del').mockImplementation(async (key) => {
                expect(key).toEqual(expected);
                done()
                return 1;
            });

            redisService.removeSet(expected);
        });
    });


    describe('removeValueFromSet', () => {
        it('should remove the specified value from the set', (done) => {

            const expected = 'test_value';
            jest.spyOn(redisClient, 'srem').mockImplementation(async (key, value) => {
                expect(value).toEqual(expected);
                done();
                return 1;
            });

            redisService.removeValueFromSet('some_key', expected);

        });
    });

    describe('getSet', () => {
        it('should return an array with all elements in the set', async () => {

            const expected = ['test_value'];

            jest.spyOn(redisClient, 'smembers').mockImplementation(async (key) => {
                return expected;
            });

            const actual = await redisService.getSet('some_key');
            expect(actual).toStrictEqual(expected);

        });
    });

    describe('getNumberOfElementsInSet', () => {
        it('should return the number of elements in the set', async () => {

            const expected = 3;

            jest.spyOn(redisClient, 'scard').mockImplementation(async (key) => {
                return 3;
            });

            const actual = await redisService.getNumberOfElementsInSet('some_key');
            expect(actual).toStrictEqual(expected);

        });
    });
});
