export function createDateObjWithoutMilliseconds(timestampOffset: number = 0): Date {
    const date = new Date(Date.now() + timestampOffset);
    date.setMilliseconds(0);
    return date;
}