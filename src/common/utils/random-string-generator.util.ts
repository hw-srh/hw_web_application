import {randomBytes} from "crypto";

export class RandomStringGenerator {
    static async hex(length: number = 32): Promise<string> {
        return new Promise((resolve: (token: string) => void, reject: (error: Error | null) => void) => {
            randomBytes(Math.floor(length / 2), (err: Error | null, buffer: Buffer) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(buffer.toString('hex'));
                }
            });
        });
    }
}