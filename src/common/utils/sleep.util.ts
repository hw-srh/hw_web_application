import {promisify} from "util";

export const sleep: <T>(ms: number) => Promise<T> = promisify(setTimeout);
