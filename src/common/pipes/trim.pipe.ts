import {
    ArgumentMetadata,
    Injectable,
    InternalServerErrorException,
    NotImplementedException,
    PipeTransform
} from "@nestjs/common";

@Injectable()
export class TrimPipe<T, R> implements PipeTransform {

    async transform(value: T, {type}: ArgumentMetadata): Promise<R> {

        switch (type) {
            case "body":
                return this.trimObjectStringProperties(value);
            case "query":
            case "param":
            case "custom":
                throw new NotImplementedException();
            default:
                throw new InternalServerErrorException();
        }
    }

    private async trimObjectStringProperties(object: {[key: string]: any}): Promise<R> {

        const resultObj = {};

        for(const [key,value] of Object.entries(object)){
            if(typeof value === 'string'){
                resultObj[key] = value.trim();
                continue;
            }

            resultObj[key] = value;
        }

        return resultObj as R;
    }
}