import {TrimPipe} from "./trim.pipe";

describe('TrimPipe', () => {
    it('should be defined', () => {
        expect(new TrimPipe()).toBeDefined();
    });

    describe("BODY type", () => {
        it('should remove preceding and leading spaces from all object properties', async () => {

            const receivedObj = {
                stringProp: "should not remove anything from this string",
                anotherStringProp: "    lots of leading and preceding spaces       ",
                intProp: 2134534,
                objProp: {
                    a: 10,
                    b: "hey  "
                },
            }

            const expected = {
                ...receivedObj,
                anotherStringProp: receivedObj.anotherStringProp.trim()
            }

            const actual = await (new TrimPipe()).transform(receivedObj, {type: "body"});
            expect(actual).toEqual(expected);
        });
    });
});
