import {IsEmail} from "class-validator";

export class EmailParamValidation {
    @IsEmail()
    email: string;
}