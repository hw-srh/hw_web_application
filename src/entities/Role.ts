import {Column, Entity, ManyToMany} from "typeorm";
import {User} from "./User";

@Entity("role", {schema: "system"})
export class Role {
  @Column("varchar", {primary: true, name: "id", length: 50})
  id: string;

  @ManyToMany(() => User, (user) => user.roles)
  users: User[];
}
