import {Column, Entity, Index, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn,} from "typeorm";
import {UserTask} from "./UserTask";
import {Role} from "./Role";
import {PasswordResetToken} from "./PasswordResetToken";

@Index("email", ["email"], {unique: true})
@Index("username", ["username"], {unique: true})
@Entity("user", {schema: "system"})
export class User {
  @PrimaryGeneratedColumn({type: "int", name: "id", unsigned: true})
  id: number;

  @Column("varchar", {name: "email", unique: true, length: 255})
  email: string;

  @Column("varchar", {name: "username", unique: true, length: 255})
  username: string;

  @Column("varchar", {name: "password", length: 255})
  password: string;

  @Column("varchar", {name: "firstname", length: 255})
  firstname: string;

  @Column("varchar", {name: "lastname", length: 255})
  lastname: string;

  @Column("tinyint", {
    name: "work_hours_per_week",
    unsigned: true,
    default: () => "'40'",
  })
  workHoursPerWeek: number;

  @OneToMany(() => UserTask, (userTask) => userTask.user)
  userTasks: UserTask[];

  @ManyToMany(() => Role, (role) => role.users)
  @JoinTable({
    name: "user_role",
    joinColumns: [{name: "user_id", referencedColumnName: "id"}],
    inverseJoinColumns: [{name: "role_id", referencedColumnName: "id"}],
    schema: "system",
  })
  roles: Role[];

  @OneToMany(
      () => PasswordResetToken,
      (passwordResetToken) => passwordResetToken.user
  )
  passwordResetTokens: PasswordResetToken[];
}
