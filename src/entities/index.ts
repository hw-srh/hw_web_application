import {User} from "./User";
import {PasswordResetToken} from "./PasswordResetToken";
import {Task} from "./Task";
import {UserTask} from "./UserTask";

export * from "./PasswordResetToken";
export * from "./Role";
export * from "./User";
export * from "./Task";
export * from "./UserTask";


export const entities = [
    User,
    PasswordResetToken,
    Task,
    UserTask
]