import {Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn,} from "typeorm";
import {User} from "./User";

@Index("token_hash", ["tokenHash"], {unique: true})
@Index("user_id", ["userId"], {})
@Entity("password_reset_token", {schema: "system"})
export class PasswordResetToken {
  @PrimaryGeneratedColumn({type: "int", name: "id", unsigned: true})
  id: number;

  @Column("int", {name: "user_id", unsigned: true})
  userId: number;

  @Column("char", {name: "token_hash", unique: true, length: 64})
  tokenHash: string;

  @Column("datetime", {
    name: "created",
    nullable: true,
    default: () => "CURRENT_TIMESTAMP",
  })
  created: Date | null;

  @ManyToOne(() => User, (user) => user.passwordResetTokens, {
    onDelete: "CASCADE",
    onUpdate: "RESTRICT",
  })
  @JoinColumn([{name: "user_id", referencedColumnName: "id"}])
  user: User;
}
