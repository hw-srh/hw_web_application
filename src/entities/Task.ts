import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {UserTask} from "./UserTask";

@Entity("task", {schema: "system"})
export class Task {
  @PrimaryGeneratedColumn({type: "int", name: "id", unsigned: true})
  id: number;

  @Column("varchar", {name: "name", length: 255})
  name: string;

  @Column("datetime", {
    name: "scheduled_start",
    default: () => "CURRENT_TIMESTAMP",
  })
  scheduledStart: Date;

  @Column("datetime", {name: "scheduled_end"})
  scheduledEnd: Date;

  @Column("time", {name: "scheduled_break_time", default: () => "'33:00:00'"})
  scheduledBreakTime: string;

  @Column("varchar", {name: "customer_name", length: 255})
  customerName: string;

  @Column("mediumtext", {name: "customer_description"})
  customerDescription: string;

  @Column("mediumtext", {name: "task_description"})
  taskDescription: string;

  @OneToMany(
      () => UserTask,
      (userTask) => userTask.task,
      {cascade: true}
  )
  userTasks: UserTask[];
}
