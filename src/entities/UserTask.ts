import {Column, Entity, Index, JoinColumn, ManyToOne} from "typeorm";
import {User} from "./User";
import {Task} from "./Task";

@Index(["userId", "taskId"], {unique: true})
@Entity("user_task", {schema: "system"})
export class UserTask {
  @Column("int", {primary: true, name: "user_id", unsigned: true})
  userId: number;

  @Column("int", {primary: true, name: "task_id", unsigned: true})
  taskId: number;

  @Column("datetime", {
    name: "actual_start",
    nullable: true,
    default: () => "CURRENT_TIMESTAMP",
  })
  actualStart: Date | null;

  @Column("datetime", {name: "actual_end", nullable: true})
  actualEnd: Date | null;

  @Column("time", {
    name: "actual_break_time",
    nullable: true,
    default: () => "'01:00:00'",
  })
  actualBreakTime: string | null;

  @Column("mediumtext", {name: "work_protocol", nullable: true})
  workProtocol: string | null;

  @ManyToOne(() => User, (user) => user.userTasks, {
    onDelete: "CASCADE",
    onUpdate: "RESTRICT",
  })
  @JoinColumn([{name: "user_id", referencedColumnName: "id"}])
  user: User;

  @ManyToOne(() => Task, (task) => task.userTasks, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{name: "task_id", referencedColumnName: "id"}])
  task: Task;
}