import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {LogLevel} from "@nestjs/common";

async function bootstrap() {

  let loggingLevels: LogLevel[] = ['error', 'warn', 'log'];

  if (['dev', 'development'].includes(process.env.APP_ENV)) {
    loggingLevels = [...loggingLevels, 'debug', 'verbose'];
  }

  const app = await NestFactory.create(AppModule, {
    logger: loggingLevels
  });

  app.enableShutdownHooks();


  if (process.env.APP_ENV === 'dev') {
    app.enableCors();
  }

  await app.listen(8000);
}

bootstrap();
