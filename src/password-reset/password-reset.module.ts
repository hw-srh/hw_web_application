import {forwardRef, Logger, Module} from '@nestjs/common';
import {PasswordResetService} from './password-reset.service';
import {PasswordResetController} from './password-reset.controller';
import {UsersModule} from "../users/users.module";
import {PasswordResetTokensModule} from "../password-reset-tokens/password-reset-tokens.module";

@Module({
    imports: [
        PasswordResetTokensModule,
        forwardRef(() => UsersModule)
    ],
    providers: [
        PasswordResetService,
        Logger
    ],
    exports: [
        PasswordResetService
    ],
    controllers: [PasswordResetController]
})
export class PasswordResetModule {
}
