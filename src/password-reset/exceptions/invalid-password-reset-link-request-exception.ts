import {Exception} from "../../common/exceptions/exception";

export class InvalidPasswordResetLinkRequestException extends Exception {
    constructor(email: string) {
        super(`Could not send the password reset link, no user exists with email: ${email}`);
    }
}