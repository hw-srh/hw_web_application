import {Test, TestingModule} from '@nestjs/testing';
import {PasswordResetService} from './password-reset.service';
import {ConfigService} from "@nestjs/config";
import {MailerService} from "@nestjs-modules/mailer";
import {ISendMailOptions} from "@nestjs-modules/mailer/dist/interfaces/send-mail-options.interface";
import {PasswordResetTokensService} from "../password-reset-tokens/password-reset-tokens.service";
import {PasswordResetToken, User} from "../entities";
import {CreatePasswordResetTokenDto} from "../password-reset-tokens/dto/create-password-reset-token.dto";
import {UsersService} from "../users/users.service";
import {InvalidPasswordResetLinkRequestException} from "./exceptions/invalid-password-reset-link-request-exception";

describe('PasswordResetService', () => {
    let mailService: MailerService;

    let passwordResetTokenService: PasswordResetTokensService;
    let passwordResetService: PasswordResetService;

    let usersService: UsersService;

    let user: User;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                {
                    provide: ConfigService,
                    useValue: {
                        get: jest.fn(() => ({
                            auth: {
                                user: "test@email.test"
                            }
                        })),
                    }
                },
                {
                    provide: MailerService,
                    useValue: {
                        sendMail: jest.fn()
                    }
                },
                {
                    provide: PasswordResetTokensService,
                    useValue: {
                        save: jest.fn(),
                    }
                },
                {
                    provide: UsersService,
                    useValue: {
                        findOneByUserNameOrEmail: jest.fn(),
                        setNewPassword: jest.fn()
                    }
                },
                PasswordResetService
            ],
        }).compile();


        passwordResetService = module.get<PasswordResetService>(PasswordResetService);
        passwordResetTokenService = module.get<PasswordResetTokensService>(PasswordResetTokensService);
        usersService = module.get<UsersService>(UsersService);

        mailService = module.get<MailerService>(MailerService);

        user = {
            id: 42,
            email: "test",
            password: "test",
            firstname: "test",
            lastname: "test",
            username: "test",
            roles: [{id: "admin",}, {id: "something_else"}]
        } as User;
    });

    it('should be defined', () => {
        expect(passwordResetService).toBeDefined();
    });

    describe('sendLink', () => {

        it('should create a new password reset token, save it to the database and send an email with a link containing the token', async () => {

            let expectedEmailBody: string = 'expected';
            let actualEmailBody: string = 'actual';

            jest.spyOn(usersService, 'findOneByUserNameOrEmail').mockImplementation(async () => user);

            jest.spyOn(passwordResetTokenService, 'save').mockImplementation(async (createPasswordResetTokenDto: CreatePasswordResetTokenDto) => {
                expectedEmailBody = `<a href="https://localhost/app/password-reset/${createPasswordResetTokenDto.token}">Click to reset your password</a>`;
                return new PasswordResetToken();
            });

            jest.spyOn(mailService, 'sendMail').mockImplementation(async (sendMailOptions: ISendMailOptions) => {
                actualEmailBody = sendMailOptions.html as string;
            });

            await passwordResetService.sendLink('test@email.test', 'localhost');

            expect(actualEmailBody).toStrictEqual(expectedEmailBody);
        });

        it(`should throw an ${InvalidPasswordResetLinkRequestException.name} if no user matching the email address is found`, async () => {

            jest.spyOn(usersService, 'findOneByUserNameOrEmail').mockImplementation(async () => null);
            await expect(passwordResetService.sendLink('test@email.test', 'localhost')).rejects.toThrow(InvalidPasswordResetLinkRequestException);
        });
    });

    describe('resetPassword', () => {
        it('should set the provided password as the new password for the user', async () => {

            const newPassword = 'new_password';

            let actual: User = new User();

            jest.spyOn(usersService, 'setNewPassword').mockImplementation(async (user, password) => {
                actual = {
                    ...user,
                    password
                };

                return actual;
            });

            await passwordResetService.resetPassword(user, newPassword);

            const expected = {...user, password: newPassword}
            expect(actual).toStrictEqual(expected);
        });
    });
});
