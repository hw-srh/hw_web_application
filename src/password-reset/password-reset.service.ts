import {BadRequestException, forwardRef, Inject, Injectable} from '@nestjs/common';
import {ConfigService} from "@nestjs/config";
import {MailerService} from "@nestjs-modules/mailer";
import {PasswordResetTokensService} from "../password-reset-tokens/password-reset-tokens.service";
import {SmtpConfig} from "../config/interfaces/smtp-config.interface";
import {UsersService} from "../users/users.service";
import {InvalidPasswordResetLinkRequestException} from "./exceptions/invalid-password-reset-link-request-exception";
import {User} from "../entities";
import {RandomStringGenerator} from "../common/utils/random-string-generator.util";

@Injectable()
export class PasswordResetService {

    private readonly RESET_TOKEN_LENGTH = 64;

    constructor(
        private readonly configService: ConfigService,
        private readonly mailerService: MailerService,
        private readonly passwordResetTokenService: PasswordResetTokensService,
        @Inject(forwardRef(() => UsersService))
        private readonly userService: UsersService
    ) {
    }


    async sendLink(email: string, currentServerDomainName: string): Promise<void> {
        const token = await RandomStringGenerator.hex(this.RESET_TOKEN_LENGTH);

        const user = await this.userService.findOneByUserNameOrEmail(email);

        if (!user) {
            throw new InvalidPasswordResetLinkRequestException(email);
        }

        await this.passwordResetTokenService.save({
            userId: user.id,
            token,
        });

        try {
            await this.sendEmail({
                email,
                token,
                currentServerDomainName
            });
        } catch (e) {
            throw new BadRequestException(`Failed to send password reset link to: ${email}`);
        }
    }

    async resetPassword(user: User, newPassword: string): Promise<void> {
        await this.userService.setNewPassword(user, newPassword);
    }

    private async sendEmail(linkData: { email: string, token: string, currentServerDomainName: string }): Promise<void> {
        await this.mailerService.sendMail({
            to: linkData.email,
            from: this.configService.get<SmtpConfig>('smtp').auth.user,
            subject: 'SRH Password reset',
            html: `<a href="https://${linkData.currentServerDomainName}/app/password-reset/${linkData.token}">Click to reset your password</a>`,
        });
    }
}
