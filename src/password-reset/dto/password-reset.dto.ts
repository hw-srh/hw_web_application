import {PasswordResetDto as PasswordResetDtoInterface} from "../../common/shared_interfaces/dto/password-reset/password-reset.dto";
import {IsEmail, IsHexadecimal, IsString, Length, MaxLength, MinLength} from "class-validator";

export class PasswordResetDto implements PasswordResetDtoInterface {
    @IsEmail()
    email: string;

    @IsString()
    @MinLength(4)
    @MaxLength(200)
    password: string;

    @IsHexadecimal()
    @Length(64, 64)
    token: string;
}