import {Test, TestingModule} from '@nestjs/testing';
import {PasswordResetController} from './password-reset.controller';
import {PasswordResetService} from "./password-reset.service";
import {Logger} from "@nestjs/common";

describe('PasswordResetController', () => {
    let controller: PasswordResetController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                {
                    provide: PasswordResetService,
                    useValue: {},
                },
                {
                    provide: Logger,
                    useClass: Logger
                }
            ],
            controllers: [PasswordResetController],
        }).compile();

        controller = module.get<PasswordResetController>(PasswordResetController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
