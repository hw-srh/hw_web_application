import {Body, Controller, Logger, Param, Post, Req, Res, UseGuards, UsePipes, ValidationPipe} from '@nestjs/common';
import {PasswordResetService} from "./password-reset.service";
import {TokenAuthGuard} from "../auth/guards/token-auth.guard";
import {Public} from "../auth/decorators/public.decorator";
import {sleep} from "../common/utils/sleep.util";
import {EmailParamValidation} from "../common/validation/email-param.validation";
import {PasswordResetDto} from "./dto/password-reset.dto";
import {TrimPipe} from "../common/pipes/trim.pipe";

@UsePipes(ValidationPipe)
@Controller('password-reset')
export class PasswordResetController {

    constructor(
        private readonly passwordResetService: PasswordResetService,
        private readonly logger: Logger
    ) {
    }

    @Public()
    @Post(':email')
    async sendPasswordResetLink(@Param() params: EmailParamValidation, @Req() request, @Res() response): Promise<void> {
        const randomOffset = Math.floor(Math.random() * 200);

        await sleep(randomOffset);

        try {
            await this.passwordResetService.sendLink(params.email, request.headers.host);
        } catch (e) {
            this.logger.error('\n' + e.message);
        }

        response.send();
    }

    @UseGuards(TokenAuthGuard)
    @UsePipes(TrimPipe)
    @Post()
    async resetPassword(@Req() request, @Body() passwordResetDto: PasswordResetDto): Promise<void> {
        await this.passwordResetService.resetPassword(request.user, passwordResetDto.password);
    }
}
