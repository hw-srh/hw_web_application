### Copyright

#### This project is based on [NestJS](https://github.com/nestjs/nest)

(The MIT License)

Copyright (c) 2017-2021 Kamil Mysliwiec <https://kamilmysliwiec.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the
'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

### How to get started

This project is meant to be used in combination with the docker
based [development-runtime](https://bitbucket.org/hw-srh/development_runtime/src/master/)
take a look at the instructions there.

### Conventions:

* [Twelve-Factor App Method](https://12factor.net/)

* SOLID

### Naming conventions

#### Files

* NestJS module core files (controller,service, module) should start with the module name (e.g. app.controller.ts
  app.module.ts)
* Files in module sub folders should end with the folder name (singular e.g. guards/jwt-auth.guard.ts)
* Multiple words should be separated with hyphens

##### Examples

* jwt-payload.interface.ts = class JwtPayload (do not include the word Interface)
* local-auth.guard.ts = LocalAuthGuard
* jwt-refresh.guard.ts = JwtRefreshGuard